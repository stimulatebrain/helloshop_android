package com.agcd.helloshop;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.agcd.helloshop.model.ModelGuest;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jspark on 2017. 6. 2..
 */

public class UserCreateActivity extends Activity {

    @BindView(R.id.userSearch_et) EditText mEtSearch;
    @BindView(R.id.userSearch_lv) ListView lvSearch;
    @BindView(R.id.userSearch_tvTitle) TextView tvTitle;

    public final static int SEARCH_TYPE_NAME = 0;
    public final static int SEARCH_TYPE_PHONE = 1;

    private int mSearchType = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_search);
        ButterKnife.bind(this);

        mSearchType = getIntent().getIntExtra("type", 0);

        if( mSearchType == SEARCH_TYPE_NAME){
            tvTitle.setText("이름 입력");
            mEtSearch.setInputType(InputType.TYPE_CLASS_TEXT);
        }else {
            tvTitle.setText("전화번호 입력");
            mEtSearch.setInputType(InputType.TYPE_CLASS_PHONE);
        }

        mEtSearch.post(new Runnable() {
            public void run() {
                mEtSearch.requestFocusFromTouch();
                InputMethodManager lManager = (InputMethodManager)UserCreateActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                lManager.showSoftInput(mEtSearch, 0);
            }
        });


        ArrayList<ModelGuest> list = new ArrayList<>();
        list.add(new ModelGuest("김효섭", "VIP", "010-8204-4551"));
        list.add(new ModelGuest("김효섭", "VIP", "010-8204-4551"));
        list.add(new ModelGuest("김효섭", "VIP", "010-8204-4551"));
        list.add(new ModelGuest("김효섭", "VIP", "010-8204-4551"));
        list.add(new ModelGuest("김효섭", "VIP", "010-8204-4551"));
        list.add(new ModelGuest("김효섭", "VIP", "010-8204-4551"));
        list.add(new ModelGuest("김효섭", "VIP", "010-8204-4551"));
        list.add(new ModelGuest("박종성", "Normal", "010-8204-4551"));
        list.add(new ModelGuest("박종성", "Normal", "010-8204-4551"));
        list.add(new ModelGuest("박종성", "Normal", "010-8204-4551"));
        SearchAdapter adapter = new SearchAdapter(this, list);
        lvSearch.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @OnClick(R.id.userSearch_btnBack)
    public void clickBackButton(){
        finish();
    }

    public class SearchAdapter extends BaseAdapter {

        private ArrayList<ModelGuest> contentsList;
        private LayoutInflater inflater;

        public SearchAdapter(Context context, ArrayList<ModelGuest> contents) {
            inflater = LayoutInflater.from(context);
            contentsList = contents;
        }

        @Override
        public int getCount() {
            return contentsList.size();
        }

        @Override
        public Object getItem(int position) {
            return contentsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            SearchAdapter.ViewHolder holder;
            if (convertView == null) {
                holder = new SearchAdapter.ViewHolder();
                convertView = inflater.inflate(R.layout.cell_user_search, parent, false);
                holder.tvFirst = (TextView) convertView.findViewById(R.id.cell_userSearch_tvFirst);
                holder.tvSecond = (TextView) convertView.findViewById(R.id.cell_userSearch_tvSecond);
                convertView.setTag(holder);
            } else {
                holder = (SearchAdapter.ViewHolder) convertView.getTag();
            }
            if( mSearchType == SEARCH_TYPE_NAME){
                holder.tvFirst.setText(contentsList.get(position).getName());
                holder.tvSecond.setText(contentsList.get(position).getPhone());
            }else {
                holder.tvFirst.setText(contentsList.get(position).getPhone());
                holder.tvSecond.setText(contentsList.get(position).getName());
            }
            return convertView;
        }

        class ViewHolder {
            TextView tvFirst;
            TextView tvSecond;
        }

    }

}
