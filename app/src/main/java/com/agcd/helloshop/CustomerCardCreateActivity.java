package com.agcd.helloshop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.agcd.helloshop.model.ModelCreate;
import com.agcd.helloshop.model.ModelCustomerCreate;
import com.agcd.helloshop.model.ModelUser;
import com.agcd.helloshop.popup.PopupBottomDialog;
import com.agcd.helloshop.util.AnimationUtility;
import com.agcd.helloshop.util.OnPopupStringResultListener;
import com.agcd.helloshop.util.Utility;
import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import se.emilsjolander.stickylistheaders.ExpandableStickyListHeadersListView;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by jspark on 2017. 5. 30..
 */

public class CustomerCardCreateActivity extends Activity implements AdapterView.OnItemClickListener{
    @BindView(R.id.customerCard_create_lv) ExpandableStickyListHeadersListView mListview;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_create);
        ButterKnife.bind(this);

        ArrayList<ModelCustomerCreate> lists = new ArrayList<>();
        lists.add(new ModelCustomerCreate("이름입력", ModelCustomerCreate.CustomerCellType.TYPE_DEFAULT));
        lists.add(new ModelCustomerCreate("전화번호입력", ModelCustomerCreate.CustomerCellType.TYPE_DEFAULT));
        lists.add(new ModelCustomerCreate("등급 선택", ModelCustomerCreate.CustomerCellType.TYPE_INFORMATION));
        lists.add(new ModelCustomerCreate("성별 선택", ModelCustomerCreate.CustomerCellType.TYPE_INFORMATION));
        lists.add(new ModelCustomerCreate("사진 변경", ModelCustomerCreate.CustomerCellType.TYPE_INFORMATION));
        lists.add(new ModelCustomerCreate("Kakaotalk", ModelCustomerCreate.CustomerCellType.TYPE_CONTACT));
        lists.add(new ModelCustomerCreate("Line", ModelCustomerCreate.CustomerCellType.TYPE_CONTACT));

        CustomerCardCreateAdapter mAdapter = new CustomerCardCreateAdapter(this, lists);
        mListview.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        mListview.setOnHeaderClickListener(new StickyListHeadersListView.OnHeaderClickListener() {
            @Override
            public void onHeaderClick(StickyListHeadersListView l, View header, int itemPosition, long headerId, boolean currentlySticky) {
                if(mListview.isHeaderCollapsed(headerId)){
                    mListview.expand(headerId);
                    ((ViewGroup)header).getChildAt(1).setSelected(false);
                }else {
                    ((ViewGroup)header).getChildAt(1).setSelected(true);
                    mListview.collapse(headerId);
                }
            }
        });
        mListview.setOnItemClickListener(this);
    }


    @OnClick(R.id.customerCard_create_btnClose)
    public void onCloseClick(View v){
        finish();
    }

    @OnClick(R.id.customerCard_create_btnSave)
    public void onClickSave(View v){

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if( position == 0){

        }else if( position == 1){

        }else {

        }
    }


    public class CustomerCardCreateAdapter extends BaseAdapter implements StickyListHeadersAdapter {
        private LayoutInflater inflater;
        private ArrayList<ModelCustomerCreate> createLists;

        public CustomerCardCreateAdapter(Context context, ArrayList<ModelCustomerCreate> createLists) {
            inflater = LayoutInflater.from(context);
            this.createLists = createLists;
        }

        @Override
        public int getCount() {
            return createLists.size();
        }

        @Override
        public Object getItem(int position) {
            return createLists.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return 4;
        }

        @Override
        public int getItemViewType(int position) {
            return createLists.get(position).type.getValue();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            Object holder;
            ModelCustomerCreate item = (ModelCustomerCreate) getItem(position);
            if (convertView == null) {
                switch(item.type){
                    case TYPE_DEFAULT:
                    default:
                        convertView = inflater.inflate(R.layout.cell_customer_card_create_et, parent, false);
                        holder = new DefaultViewHolder();
                        ((DefaultViewHolder)holder).etName = (EditText) convertView.findViewById(R.id.cell_customerCardCreateDefault_tvName);
                        break;
                    case TYPE_INFORMATION:
                        convertView = inflater.inflate(R.layout.cell_customer_card_create_info, parent, false);
                        holder = new InfoViewHolder();
                        ((InfoViewHolder)holder).tvName = (TextView) convertView.findViewById(R.id.cell_customerCardCreateInfo_tvName);
                        ((InfoViewHolder)holder).tvSelect = (TextView) convertView.findViewById(R.id.cell_customerCardCreateInfo_tvButton);
                        ((InfoViewHolder)holder).ivPhoto = (ImageView) convertView.findViewById(R.id.cell_customerCardCreateInfo_iv);
                        ((InfoViewHolder)holder).ivWrapper = (View) convertView.findViewById(R.id.cell_customerCardCreateInfo_ivWrapper);
                        break;
                    case TYPE_CONTACT:
                        convertView = inflater.inflate(R.layout.cell_customer_card_create_contact, parent, false);
                        holder = new ContactViewHolder();
                        ((ContactViewHolder)holder).etName = (EditText) convertView.findViewById(R.id.cell_customerCardCreateContact_tvName);
                        ((ContactViewHolder)holder).ivMessenger = (ImageView) convertView.findViewById(R.id.cell_customerCardCreateContact_iv);
                        break;
                }
                convertView.setTag(holder);
            }

            switch (item.type) {
                case TYPE_DEFAULT:
                    DefaultViewHolder holder1 = (DefaultViewHolder) convertView.getTag();
                    holder1.etName.setHint(item.name);
                    break;
                case TYPE_INFORMATION:
                    InfoViewHolder holder2 = (InfoViewHolder) convertView.getTag();
                    if (item.name.equals("등급 선택")) {
                        holder2.tvName.setText("NEW");
                        holder2.tvName.setTextColor(Color.parseColor("#313131"));
                        holder2.tvSelect.setText("등급 선택");
                        holder2.ivWrapper.setVisibility(View.GONE);

                        holder2.tvSelect.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ArrayList<String> tests = new ArrayList<>();
                                tests.add("VIP");
                                tests.add("Normal");
                                tests.add("NEW");
                                tests.add("BAD");
                                PopupBottomDialog dialog = new PopupBottomDialog(CustomerCardCreateActivity.this, tests, new OnPopupStringResultListener() {
                                    @Override
                                    public void onComplete(String result) {

                                    }
                                });
                                dialog.show();

                            }
                        });

                    } else if (item.name.equals("성별 선택")) {
                        holder2.tvName.setText("성별");
                        holder2.tvName.setTextColor(Color.parseColor("#7a7a7a"));
                        holder2.tvSelect.setText("성별 선택");
                        holder2.ivWrapper.setVisibility(View.GONE);

                        holder2.tvSelect.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ArrayList<String> tests = new ArrayList<>();
                                tests.add("남성");
                                tests.add("여성");
                                PopupBottomDialog dialog = new PopupBottomDialog(CustomerCardCreateActivity.this, tests, new OnPopupStringResultListener() {
                                    @Override
                                    public void onComplete(String result) {

                                    }
                                });
                                dialog.show();
                            }
                        });

                    } else {
                        holder2.tvName.setVisibility(View.GONE);
                        holder2.tvSelect.setText("사진 변경");
                        holder2.ivWrapper.setVisibility(View.VISIBLE);
                    }
                    break;
                case TYPE_CONTACT:
                    ContactViewHolder holder3 = (ContactViewHolder) convertView.getTag();
                    if (item.name.equals("Kakaotalk")) {
                        holder3.etName.setHint("Kakaotalk ID");
                        holder3.ivMessenger.setBackgroundResource(R.drawable.ic_kakao);
                    } else {
                        holder3.etName.setHint("Line ID");
                        holder3.ivMessenger.setBackgroundResource(R.drawable.ic_line);
                    }
                    break;
            }
            return convertView;
        }

        @Override
        public View getHeaderView(int position, View convertView, ViewGroup parent) {
            HeaderViewHolder holder;
            if (convertView == null) {
                holder = new HeaderViewHolder();
                convertView = inflater.inflate(R.layout.cell_card_create_header, parent, false);
                holder.tvName = (TextView) convertView.findViewById(R.id.cell_cardCreateHeader_tvName);
                convertView.setTag(holder);
            } else {
                holder = (HeaderViewHolder) convertView.getTag();
            }
            ModelCustomerCreate model = createLists.get(position);
            switch (model.type){
                case TYPE_DEFAULT:
                    convertView.setBackgroundResource(R.drawable.card_create_header_background);
                    holder.tvName.setText("기본 정보");
                    break;
                case TYPE_INFORMATION:
                    convertView.setBackgroundColor(Color.parseColor("#e6e6e6"));
                    holder.tvName.setText("기타 정보");
                    break;
                case TYPE_CONTACT:
                    convertView.setBackgroundColor(Color.parseColor("#e6e6e6"));
                    holder.tvName.setText("기타 연락처");
                    break;
            }
            return convertView;
        }

        @Override
        public long getHeaderId(int position) {
            //return the first character of the country as ID because this is what headers are based upon
            return createLists.get(position).type.getValue();
        }

        class HeaderViewHolder {
            TextView tvName;
        }

        class DefaultViewHolder {
            EditText etName;
        }
        class InfoViewHolder {
            TextView tvName;
            ImageView ivPhoto;
            TextView tvSelect;
            View ivWrapper;
        }

        class ContactViewHolder{
            ImageView ivMessenger;
            EditText etName;
        }

    }
}
