package com.agcd.helloshop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.agcd.helloshop.util.OnPopupPriceResultListener;
import com.agcd.helloshop.popup.PopupBottomDialog;
import com.agcd.helloshop.popup.PopupPriceDialog;
import com.agcd.helloshop.util.OnPopupStringResultListener;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jspark on 2017. 5. 29..
 */

public class CardModifyActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_modify);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.card_modify_btnClose)
    public void clickClose(View view){
        finish();
    }

    @OnClick(R.id.card_modify_btnSelectGender)
    public void selectGender(View view){
        ArrayList<String> tests = new ArrayList<>();
        tests.add("남성");
        tests.add("여성");

        PopupBottomDialog dialog = new PopupBottomDialog(this, tests, new OnPopupStringResultListener() {
            @Override
            public void onComplete(String result) {

            }
        });
        dialog.show();
    }


    @OnClick(R.id.card_modify_btnSelectService)
    public void selectService(View view){
        Intent i = new Intent(this, CardModifyListActivity.class);
        startActivity(i);
    }


    @OnClick(R.id.card_modify_btnSelectPrice)
    public void selectPrice(View view){
        PopupPriceDialog dialog = new PopupPriceDialog(this, 1000, new OnPopupPriceResultListener() {
            @Override
            public void onComplete(int newPrice) {

            }
        });
        dialog.show();

    }


    @OnClick(R.id.card_modify_btnSelectExpert)
    public void selectExpert(View view){
        Intent i = new Intent(this, CardModifyListActivity.class);
        startActivity(i);
    }





}
