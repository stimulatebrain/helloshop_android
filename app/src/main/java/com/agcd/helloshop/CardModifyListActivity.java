package com.agcd.helloshop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.agcd.helloshop.model.ModelGuest;
import com.agcd.helloshop.model.ModelService;
import com.agcd.helloshop.model.ModelStaff;
import com.agcd.helloshop.popup.PopupBottomDialog;
import com.agcd.helloshop.rest.APIHandler;
import com.agcd.helloshop.rest.ModelError;
import com.agcd.helloshop.rest.RestClient;
import com.agcd.helloshop.rest.ServerAPI;
import com.agcd.helloshop.util.Constants;
import com.agcd.helloshop.util.LocalData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

/**
 * Created by jspark on 2017. 5. 30..
 */

public class CardModifyListActivity extends Activity {
    @BindView(R.id.card_modifyList_lv) ListView mListview;
    public final static int TYPE_SERVICE = 0;
    public final static int TYPE_STAFF = 1;

    private int mType;

    @BindView(R.id.card_modifyList_tvTitle) TextView mTvTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_modify_list);
        ButterKnife.bind(this);

        mType = getIntent().getIntExtra("type", 0);
        int sex = getIntent().getIntExtra("sex", 0);

        if(mType == TYPE_SERVICE){
            mTvTitle.setText("서비스 선택");
            ArrayList<ModelService> array = new ArrayList<>();
            final ModifyListServiceAdapter adapter = new ModifyListServiceAdapter(this, array);
            mListview.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            RestClient.getApi().loadService(LocalData.getShopId(this),
                                            sex,
                                 ServerAPI.TEST_AUTH_TOKEN).enqueue (new APIHandler<List<ModelService>>(this) {
                                    @Override
                                    public void onSuccess(List<ModelService> dataObject) {
                                        adapter.contentsList = (ArrayList<ModelService>) dataObject;
                                        adapter.notifyDataSetChanged();
                                    }
                                    @Override
                                    public void onFailureOrLogicError(ModelError error) {

                                    }
                                });

        }else {
            mTvTitle.setText("EXPERT 선택");
            ArrayList<ModelStaff> array = new ArrayList<>();
            final ModifyListStaffAdapter adapter = new ModifyListStaffAdapter(this, array);
            mListview.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            RestClient.getApi().loadStaff(LocalData.getShopId(this),
                    ServerAPI.TEST_AUTH_TOKEN).enqueue (new APIHandler<List<ModelStaff>>(this) {
                @Override
                public void onSuccess(List<ModelStaff> dataObject) {
                    adapter.contentsList = (ArrayList<ModelStaff>) dataObject;
                    adapter.notifyDataSetChanged();
                }
                @Override
                public void onFailureOrLogicError(ModelError error) {

                }
            });
        }

    }

    @OnItemClick(R.id.card_modifyList_lv)
    public void onItemClick(AdapterView<?> parent, View view, int position, long id){
        Intent i = new Intent();
        if(mType == TYPE_SERVICE){
            i.putExtra("service", ((ModifyListServiceAdapter)mListview.getAdapter()).contentsList.get(position));
            setResult(CardCreateActivity.RESULT_CODE_SERVICE, i);
            finish();
        }else {
            i.putExtra("staff", ((ModifyListStaffAdapter)mListview.getAdapter()).contentsList.get(position));
            setResult(CardCreateActivity.RESULT_CODE_STAFF, i);
            finish();
        }
    }

    @OnClick(R.id.card_modifyList_btnClose)
    public void onClickClose(View view){
        finish();
    }


    public class ModifyListServiceAdapter extends BaseAdapter {

        private ArrayList<ModelService> contentsList;
        private LayoutInflater inflater;

        public ModifyListServiceAdapter(Context context, ArrayList<ModelService> contents) {
            inflater = LayoutInflater.from(context);
            contentsList = contents;
        }

        @Override
        public int getCount() {
            return contentsList.size();
        }

        @Override
        public Object getItem(int position) {
            return contentsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
           ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.cell_card_modify_service_list, parent, false);
                holder.tvName = (TextView) convertView.findViewById(R.id.cell_cardModifyServiceList_tvName);
                holder.tvTime = (TextView) convertView.findViewById(R.id.cell_cardModifyServiceList_tvTime);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.tvName.setText(contentsList.get(position).getName());
            holder.tvName.setTextColor(Color.parseColor(contentsList.get(position).getColor_code()));
            holder.tvTime.setText(contentsList.get(position).getTime() + "분");

            return convertView;
        }

        class ViewHolder {
            TextView tvName;
            TextView tvTime;
        }

    }

    public class ModifyListStaffAdapter extends BaseAdapter {

        private ArrayList<ModelStaff> contentsList;
        private LayoutInflater inflater;

        public ModifyListStaffAdapter(Context context, ArrayList<ModelStaff> contents) {
            inflater = LayoutInflater.from(context);
            contentsList = contents;
        }

        @Override
        public int getCount() {
            return contentsList.size();
        }

        @Override
        public Object getItem(int position) {
            return contentsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.cell_card_modify_list, parent, false);
                holder.tvName = (TextView) convertView.findViewById(R.id.cell_cardModifyList_tvName);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.tvName.setText(contentsList.get(position).getNickname());

            return convertView;
        }

        class ViewHolder {
            TextView tvName;
        }

    }
}
