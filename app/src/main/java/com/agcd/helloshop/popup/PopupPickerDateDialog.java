package com.agcd.helloshop.popup;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.NumberPicker;

import com.agcd.helloshop.R;
import com.agcd.helloshop.util.OnPopupDateResultListener;
import com.agcd.helloshop.util.OnPopupPriceResultListener;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by jspark on 2016. 12. 17..
 */

public class PopupPickerDateDialog extends Dialog {

    @BindView(R.id.popup_picker_1) NumberPicker mPicker1;
    @BindView(R.id.popup_picker_2) NumberPicker mPicker2;
    @BindView(R.id.popup_picker_3) NumberPicker mPicker3;

    private OnPopupDateResultListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 다이얼로그 외부 화면 흐리게 표현
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.4f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.popup_datepicker);

        ButterKnife.bind(this);

        setDividerColor(mPicker1, Color.TRANSPARENT);
        setDividerColor(mPicker2, Color.TRANSPARENT);
        setDividerColor(mPicker3, Color.TRANSPARENT);

        mPicker1.setMinValue(2015);
        mPicker1.setMaxValue(2017);

        mPicker2.setMinValue(1);
        mPicker2.setMaxValue(12);

        mPicker3.setMinValue(1);
        mPicker3.setMaxValue(31);
    }

    private void setDividerColor(NumberPicker picker, int color) {
        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    @OnClick(R.id.popup_picker_btnCancel)
    public void onClickCancel(View view){
        this.dismiss();
    }

    @OnClick(R.id.popup_picker_btnComplete)
    public void onClickComplete(View view){
        this.dismiss();
        listener.onComplete(new Date());
    }


    public PopupPickerDateDialog(Context context, OnPopupDateResultListener listener) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.listener = listener;
    }

}
