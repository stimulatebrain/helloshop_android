package com.agcd.helloshop.popup;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.agcd.helloshop.R;
import com.agcd.helloshop.util.OnPopupPriceResultListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by jspark on 2016. 12. 17..
 */

public class PopupPriceDialog extends Dialog {

    @BindView(R.id.popup_price_btnComplete) TextView btnComplete;
    private OnPopupPriceResultListener listener;
    private int price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 다이얼로그 외부 화면 흐리게 표현
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.6f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.popup_price);

        ButterKnife.bind(this);

    }

    @OnClick(R.id.popup_price_btnComplete)
    public void onClickComplete(View view){
        this.dismiss();
        listener.onComplete(1000);
    }


    public PopupPriceDialog(Context context, int price, OnPopupPriceResultListener listener) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.price = price;
        this.listener = listener;
    }

}
