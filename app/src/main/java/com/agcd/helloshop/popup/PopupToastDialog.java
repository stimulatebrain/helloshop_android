package com.agcd.helloshop.popup;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.agcd.helloshop.R;
import com.agcd.helloshop.util.Utility;

import org.w3c.dom.Text;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by jspark on 2016. 12. 17..
 */

public class PopupToastDialog extends Dialog implements AdapterView.OnItemClickListener {

    @BindView(R.id.popup_toast_ll)
    LinearLayout llToast;

    @BindView(R.id.popup_toast_tv)
    TextView tvToast;

    private SpannableString mTitleString;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 다이얼로그 외부 화면 흐리게 표현
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.0f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.popup_toast);
        ButterKnife.bind(this);


        ((ViewGroup.MarginLayoutParams)llToast.getLayoutParams()).bottomMargin = Utility.dpToPx(-60);
        llToast.requestLayout();
        tvToast.setText(mTitleString);
        startToastAnim();
    }

    private void startToastAnim(){

        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                ((ViewGroup.MarginLayoutParams)llToast.getLayoutParams()).bottomMargin = (int) (Utility.dpToPx(20) * interpolatedTime);
                llToast.requestLayout();
                llToast.setAlpha(interpolatedTime);
            }
        };
        anim.setDuration(500);
        anim.setInterpolator(new OvershootInterpolator());
        llToast.startAnimation(anim);

        llToast.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopToastAnim();
            }
        }, 2000);
    }

    private void stopToastAnim() {
        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                ((ViewGroup.MarginLayoutParams)llToast.getLayoutParams()).bottomMargin = (int) (Utility.dpToPx(-60) * interpolatedTime);
                llToast.requestLayout();
                llToast.setAlpha(1 - interpolatedTime);
            }
        };
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                PopupToastDialog.this.dismiss();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        anim.setDuration(500);
        anim.setInterpolator(new OvershootInterpolator());
        llToast.startAnimation(anim);
    }

    public PopupToastDialog(Context context, SpannableString text) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        mTitleString = text;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        this.dismiss();
    }


}
