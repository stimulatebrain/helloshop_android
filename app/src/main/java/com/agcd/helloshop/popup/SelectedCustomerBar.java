package com.agcd.helloshop.popup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.agcd.helloshop.CustomerMessageActivity;
import com.agcd.helloshop.R;
import com.agcd.helloshop.model.ModelGuest;
import com.agcd.helloshop.util.OnPopupStringResultListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectedCustomerBar extends RelativeLayout {

    Context mContext;
    OnClickActionListener mOnClickAction;
    public void setOnClickAction(OnClickActionListener onClickActionListener) {
        mOnClickAction = onClickActionListener;
    }

    public interface OnClickActionListener {
        void onClickChangeGrade();
        void onClickExport();
        void onClickDelete();
    }

    @BindView(R.id.selectedCustomerBar_tvSelectedCountInfo)
    TextView mTvSelectedCountInfo;

    @OnClick(R.id.selectedCustomerBar_llBtnNext)
    void clickNext() {
        ArrayList<String> list = new ArrayList<>();
        list.add("쪽지 전송");
        list.add("문자메시지 전송");
        list.add("등급변경");
        list.add("내보내기");
        list.add("고객카드삭제");
        PopupBottomDialog dialog = new PopupBottomDialog(mContext, list, new OnPopupStringResultListener() {
            @Override
            public void onComplete(String result) {
                if (result.equals("쪽지 전송")) {
                    Intent i = new Intent(mContext, CustomerMessageActivity.class);
                    mContext.startActivity(i);
                } else if (result.equals("문자메시지 전송")) {

                } else if (result.equals("등급변경")) {
                } else if (result.equals("내보내기")) {
                } else if (result.equals("고객카드삭제")) {
                }
            }
        });
        dialog.show();

//        if (mOnClickAction!=null) {
//            mOnClickAction.onClickNext();
//        }
    }

    public SelectedCustomerBar(Context context) {
        this(context, null, 0);
    }
    public SelectedCustomerBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
    public SelectedCustomerBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        initWith(context);
    }
    void initWith(Context context) {
        mContext = context;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.view_selected_customer_bar, this, true);
        ButterKnife.bind(this, view);
    }


    public String selectedInfoTitleWithList(List<ModelGuest> selectedList) {
        String title;
        if (selectedList.size()==1) {
            title = selectedList.get(0).guest_name;
        } else {
            title = selectedList.get(0).guest_name + " 외 " + selectedList.size() + "명 선택";
        }
        return title;
    }
    public void setSelectedInfoTitle(String title) {
        mTvSelectedCountInfo.setText(title);
    }
    public void setSelectedInfoTitle(List<ModelGuest> selectedList) {
        String title = selectedInfoTitleWithList(selectedList);
        setSelectedInfoTitle(title);
    }


}
