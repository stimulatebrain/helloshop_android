package com.agcd.helloshop.popup;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.agcd.helloshop.R;
import com.agcd.helloshop.util.OnPopupStringResultListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by jspark on 2016. 12. 17..
 */

public class PopupBottomDialog extends Dialog implements AdapterView.OnItemClickListener {


    @BindView(R.id.popup_bottom_lv) ListView mListview;

    private PopupAdapter mAdapter;
    private OnPopupStringResultListener mListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 다이얼로그 외부 화면 흐리게 표현
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.4f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.popup_bottom_list);
        ButterKnife.bind(this);

        mListview.setAdapter(mAdapter);
        mListview.setOnItemClickListener(this);
        mAdapter.notifyDataSetChanged();
    }

    public PopupBottomDialog(Context context, ArrayList<String> contents, OnPopupStringResultListener listener) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        mAdapter = new PopupAdapter(context, contents);
        mListener = listener;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(mListener != null){
            mListener.onComplete(mAdapter.contentsList.get(position));
        }
        this.dismiss();
    }


    public class PopupAdapter extends BaseAdapter {

        private ArrayList<String> contentsList;
        private LayoutInflater inflater;

        public PopupAdapter(Context context, ArrayList<String> contents) {
            inflater = LayoutInflater.from(context);
            contentsList = contents;
        }

        @Override
        public int getCount() {
            return contentsList.size();
        }

        @Override
        public Object getItem(int position) {
            return contentsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.cell_popup_bottom, parent, false);
                holder.tvName = (TextView) convertView.findViewById(R.id.cell_popupBottom_tvName);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.tvName.setText(contentsList.get(position));
            convertView.setBackgroundColor(Color.WHITE);
            return convertView;
        }

        class ViewHolder {
            TextView tvName;
        }

    }

}
