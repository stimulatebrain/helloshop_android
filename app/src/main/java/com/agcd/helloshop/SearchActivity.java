package com.agcd.helloshop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import com.agcd.helloshop.adapter.CustomerListAdapter;
import com.agcd.helloshop.model.ModelGuest;
import com.agcd.helloshop.model.ModelMemo;
import com.agcd.helloshop.model.ModelParent;
import com.agcd.helloshop.model.ModelReserve;
import com.agcd.helloshop.popup.PopupBottomDialog;
import com.agcd.helloshop.popup.PopupPickerDateDialog;
import com.agcd.helloshop.popup.SelectedCustomerBar;
import com.agcd.helloshop.rest.APIHandler;
import com.agcd.helloshop.rest.ModelError;
import com.agcd.helloshop.rest.RestClient;
import com.agcd.helloshop.util.LocalData;
import com.agcd.helloshop.util.OnPopupDateResultListener;
import com.agcd.helloshop.util.OnPopupStringResultListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.hoang8f.android.segmented.SegmentedGroup;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by jspark on 2017. 5. 30..
 */

public class SearchActivity extends Activity{

    SearchAdapter mSearchAllAdapter;
    CustomerListAdapter mCustomerAdapter;

    @BindView(R.id.search_lv) StickyListHeadersListView mListView;
    @BindView(R.id.actSearch_llCustomerSubTab) ViewGroup mCustomerSubTab;

    @BindView(R.id.actSearch_pbLoading) ProgressBar mPbLoading;
    @BindView(R.id.actSearch_etSearch) EditText mEtSearch;


    @BindView(R.id.actSearch_selectedCustomerBar)
    SelectedCustomerBar mCustomerSelectedBar;

    @BindView(R.id.actSearch_rlSearchResultHeader) ViewGroup mSearchResultHeader;
    @BindView(R.id.actSearch_tvResultCount) TextView mTvResultCount;
    @BindView(R.id.actSearch_tvBtnSelect) TextView mTvBtnSelect;
    @OnClick(R.id.actSearch_tvBtnSelect)
    public void onClickSelectUser() {

        if (mCustomerAdapter.isSelectMode()==true) {
            mCustomerAdapter.setSelectModeOff();

            mTvBtnSelect.setText("선택");
//            mBottomSelectedView.setVisibility(View.GONE);
            mCustomerSelectedBar.setVisibility(View.GONE);
        } else {
            mCustomerAdapter.setSelectModeOn(new CustomerListAdapter.OnChangeSelectedListener() {
                @Override
                public void onChange(List<ModelGuest> selectedList) {
                    if (selectedList.size()>0) {
                        mCustomerSelectedBar.setVisibility(View.VISIBLE);
                        mCustomerSelectedBar.setSelectedInfoTitle(selectedList);
                    } else {
                        mCustomerSelectedBar.setVisibility(View.GONE);
                    }
                }
            });

            mTvBtnSelect.setText("취소");
            //mBottomSelectedView.setVisibility(View.VISIBLE);
            mCustomerSelectedBar.setVisibility(View.VISIBLE);
        }
    }

    @BindView(R.id.actSearch_sgSearchTypeTab) SegmentedGroup mSgSearchTypeTab;
    @OnClick({R.id.search_segmentedGroup_1, R.id.search_segmentedGroup_2})
    public void onCheckedChanged(RadioButton button) {
        boolean isChecked = button.isChecked();

        switch (button.getId()){
            case R.id.search_segmentedGroup_1:
                mCustomerSubTab.setVisibility(View.GONE);
                mListView.setAdapter(mSearchAllAdapter);
                break;
            case R.id.search_segmentedGroup_2:
                mCustomerSubTab.setVisibility(View.VISIBLE);
                mListView.setAdapter(mCustomerAdapter);
                mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (mCustomerAdapter.isSelectMode()) {
                            mCustomerAdapter.clickCheck(position);
                        } else {
                            Intent i = new Intent(SearchActivity.this, CustomerCardActivity.class);
                            startActivity(i);
                        }
                    }
                });
                break;
        }
    }
    private boolean isAllSearchTabSelected() {
        if (mCustomerSubTab.getVisibility()==View.VISIBLE) {
            return false;
        } else {
            return true;
        }
    }

    @OnClick(R.id.search_btnClose)
    public void onCloseClick(View v){
        finish();
    }



    @OnClick(R.id.search_btnGrade)
    public void onClickGrade(View v){
        ArrayList<String> list = new ArrayList<>();
        list.add("상관없음");
        list.add("VIP");
        list.add("New");
        list.add("Normal");
        list.add("Bad");

        PopupBottomDialog dialog = new PopupBottomDialog(this, list, new OnPopupStringResultListener() {
            @Override
            public void onComplete(String result) {

            }
        });
        dialog.show();
    }

    @OnClick(R.id.search_btnState)
    public void onClickState(View v){
        ArrayList<String> list = new ArrayList<>();
        list.add("상관없음");
        list.add("시술대기");
        list.add("시술완료");
        PopupBottomDialog dialog = new PopupBottomDialog(this, list, new OnPopupStringResultListener() {
            @Override
            public void onComplete(String result) {

            }
        });
        dialog.show();
    }

    @OnClick(R.id.search_btnCount)
    public void onClickCount(View v){
        ArrayList<String> list = new ArrayList<>();
        list.add("상관없음");
        list.add("1회 방문");
        list.add("2회 방문");
        list.add("3회 이상");
        list.add("5회 이상");
        list.add("10회 이상");

        PopupBottomDialog dialog = new PopupBottomDialog(this, list, new OnPopupStringResultListener() {
            @Override
            public void onComplete(String result) {

            }
        });
        dialog.show();
    }

    @OnClick({R.id.search_date1, R.id.search_date2})
    public void onClickDate(View v){
        PopupPickerDateDialog dialog = new PopupPickerDateDialog(this, new OnPopupDateResultListener() {

            @Override
            public void onComplete(Date date) {

            }
        });
        dialog.show();

    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String searchText = textView.getText().toString();
                    if (isAllSearchTabSelected()) {
                        searchAll(searchText);
                    } else {
                        int shopId = LocalData.getShopId(SearchActivity.this);
                        mPbLoading.setVisibility(View.VISIBLE);
                        searchCustomer(searchText, ModelGuest.Grade.ALL, shopId, new CustomerHandler() {
                            @Override
                            public void onSuccess(List<ModelGuest> list) {
                                mPbLoading.setVisibility(View.INVISIBLE);

                                mTvResultCount.setText( "전체 "+list.size()+"건" );
                                mCustomerAdapter.setCustomerList(list);
                                mListView.setAdapter(mCustomerAdapter);
                                mCustomerAdapter.notifyDataSetChanged();
                            }
                            @Override
                            public void onFailure() {
                                mPbLoading.setVisibility(View.INVISIBLE);
                            }
                        });
                    }
                    return true;
                }
                return false;
            }
        });

        mSearchAllAdapter = new SearchAdapter(this);
//
        ArrayList<ModelParent> lists = new ArrayList<>();
//        lists.add(new ModelGuest(0, 1));
//        lists.add(new ModelGuest(0, 2));
//        lists.add(new ModelGuest(0, 3));
//        lists.add(new ModelGuest(0, 4));
//        lists.add(new ModelGuest(0, 4));
//        lists.add(new ModelParent(0, ModelParent.Type.TYPE_CELL));
//
//        lists.add(new ModelGuest(1, 1));
//        lists.add(new ModelGuest(1, 2));
//        lists.add(new ModelGuest(1, 3));
//        lists.add(new ModelGuest(1, 4));
//        lists.add(new ModelParent(1, ModelParent.Type.TYPE_CELL));
//
////        lists.add(new ModelReserve(2, "1"));
////        lists.add(new ModelReserve(2, "2"));
////        lists.add(new ModelReserve(2, "3"));
////        lists.add(new ModelReserve(2, "4"));
//        lists.add(new ModelParent(2, ModelParent.Type.TYPE_CELL));
//
//
////        lists.add(new ModelReserve(3, "1"));
////        lists.add(new ModelReserve(3, "2"));
////        lists.add(new ModelReserve(3, "3"));
////        lists.add(new ModelReserve(3, "4"));
//        lists.add(new ModelParent(3, ModelParent.Type.TYPE_CELL));

        mSearchAllAdapter.mList = lists;
        mListView.setAdapter(mSearchAllAdapter);
        mSearchAllAdapter.notifyDataSetChanged();

        mCustomerAdapter = new CustomerListAdapter(this);
    }


    public interface CustomerHandler {
        void onSuccess(List<ModelGuest> list);
        void onFailure();
    }
    void searchCustomer(String searchText, ModelGuest.Grade grade, int shopId, final CustomerHandler customerHandler) {
        RestClient.getApi().searchGuestList(shopId, searchText,
                LocalData.getTokenKey(this)).enqueue(new APIHandler<List<ModelGuest>>(this) {
            @Override
            public void onSuccess(List<ModelGuest> dataObject) {
                customerHandler.onSuccess(dataObject);
            }

            @Override
            public void onFailureOrLogicError(ModelError error) {
                customerHandler.onFailure();
            }
        });
    }


    final ArrayList<ModelParent> mSearchResultList = new ArrayList<>();
    void searchAll(final String searchText) {
        final int shopId = LocalData.getShopId(this);

        final ArrayList<ModelParent> lists = new ArrayList<>();

        // start loading
        mPbLoading.setVisibility(View.VISIBLE);
        searchSchedule(searchText, shopId, new ScheduleHandler() {
            @Override
            public void onSuccess(List<ModelReserve> list) {
                for (int i = 0; i < list.size(); i++) {
                    ModelReserve schedule = list.get(i);
                    schedule.headerId = 0;
                    schedule.sectionCount = list.size();
                    schedule.type = ModelParent.Type.TYPE_RESERVE;
                    lists.add(schedule);
                }
                //if (list.size()>0) lists.add(new ModelParent(0, ModelParent.Type.TYPE_CELL));

                searchCustomer(searchText, ModelGuest.Grade.ALL, shopId, new CustomerHandler() {
                    @Override
                    public void onSuccess(List<ModelGuest> list) {
                        for (int i = 0; i < list.size(); i++) {
                            ModelGuest schedule = list.get(i);
                            schedule.headerId = 1;
                            schedule.sectionCount = list.size();
                            schedule.type = ModelParent.Type.TYPE_CUSTOMER;
                            lists.add(schedule);
                        }
                        //if (list.size()>0) lists.add(new ModelParent(1, ModelParent.Type.TYPE_CELL));

                        searchMemo(searchText, shopId, new MemoHandler() {
                            @Override
                            public void onSuccess(List<ModelMemo> list) {
                                mPbLoading.setVisibility(View.INVISIBLE);

                                for (int i = 0; i < list.size(); i++) {
                                    ModelMemo schedule = list.get(i);
                                    schedule.headerId = 2;
                                    schedule.sectionCount = list.size();
                                    schedule.type = ModelParent.Type.TYPE_MEMO;
                                    lists.add(schedule);
                                }
                                //if (list.size()>0) lists.add(new ModelParent(2, ModelParent.Type.TYPE_CELL));

                                mSearchAllAdapter.mList = lists;
                                mListView.setAdapter(mSearchAllAdapter);
                                mSearchAllAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                    @Override
                    public void onFailure() { searchFailed(); }
                });
            }
        });
    }
    void searchFailed() {
        // end loading
        mPbLoading.setVisibility(View.INVISIBLE);
    }

    public interface ScheduleHandler {
        void onSuccess(List<ModelReserve> list);
    }
    void searchSchedule(String searchText, int shopId, final ScheduleHandler scheduleHandler) {
        String statusValue = "";
        RestClient.getApi().searchScheduleList(shopId, searchText, statusValue,
                LocalData.getTokenKey(this)).enqueue(new APIHandler<List<ModelReserve>>(this) {
            @Override
            public void onSuccess(List<ModelReserve> dataObject) {
                scheduleHandler.onSuccess(dataObject);
            }

            @Override
            public void onFailureOrLogicError(ModelError error) {
                searchFailed();
            }
        });
    }
    public interface MemoHandler {
        void onSuccess(List<ModelMemo> list);
    }
    void searchMemo(String searchText, int shopId, final MemoHandler memoHandler) {
        String statusValue = "";
        RestClient.getApi().searchMemoList(shopId, searchText,
                LocalData.getTokenKey(this)).enqueue(new APIHandler<List<ModelMemo>>(this) {
            @Override
            public void onSuccess(List<ModelMemo> dataObject) {
                memoHandler.onSuccess(dataObject);
            }

            @Override
            public void onFailureOrLogicError(ModelError error) {
                searchFailed();
            }
        });
    }




    public class SearchAdapter extends BaseAdapter implements StickyListHeadersAdapter {
        private LayoutInflater mInflater;
        private ArrayList<ModelParent> mList;

        public SearchAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public Object getItem(int position) {
            return mList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return 5; // temp - 왜 4면 에러?? 원래 4였는데 case TYPE_MEMO: 추가 후 5로 해야 에러 안남
        }

        @Override
        public int getItemViewType(int position) {
            return ((ModelParent)mList.get(position)).type.getValue();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            //ViewHolder holder;

            ModelParent model = mList.get(position);

            switch (model.type){
                case TYPE_CELL:
//                    return ReservationCell.configure(SearchActivity.this, mInflater, convertView, parent, (ModelReserve) model);
                    convertView = mInflater.inflate(R.layout.cell_search_more, parent, false);
                    if (convertView == null) {
                        ViewHolder holder = new ViewHolder();
                        convertView.setTag(holder);
                    } else {
                        ViewHolder holder = (ViewHolder)convertView.getTag();
                    }
                    break;
                case TYPE_CUSTOMER:
                    return CustomerListCell.configure(SearchActivity.this, mInflater, convertView, parent, (ModelGuest) model);

                case TYPE_RESERVE:
                    return ReservationCell.configure(SearchActivity.this, mInflater, convertView, parent, (ModelReserve) model);
//                    convertView = mInflater.inflate(R.layout.cell_card_reserve, parent, false);
//                    break;
                case TYPE_MEMO:
                    convertView = mInflater.inflate(R.layout.cell_customer, parent, false);
                    break;
            }

            return convertView;
        }

        @Override
        public View getHeaderView(int position, View convertView, ViewGroup parent) {
            HeaderViewHolder holder;
            if (convertView == null) {
                holder = new HeaderViewHolder();
                convertView = mInflater.inflate(R.layout.cell_search_header, parent, false);
                holder.tvName = (TextView) convertView.findViewById(R.id.searchCellHeader_tvName);
                holder.tvCount = (TextView) convertView.findViewById(R.id.searchCellHeader_tvCount);
                convertView.setTag(holder);
            } else {
                holder = (HeaderViewHolder) convertView.getTag();
            }
            ModelParent model = mList.get(position);
            switch (model.headerId){
                case 0:
                    holder.tvName.setText("예약카드");
                    holder.tvCount.setText("전체 " + model.sectionCount + "건");
                    break;
                case 1:
                    holder.tvName.setText("고객카드");
                    holder.tvCount.setText("전체 " + model.sectionCount + "건");
                    break;
                case 2:
                    holder.tvName.setText("고객메모");
                    holder.tvCount.setText("전체 " + model.sectionCount + "건");
                    break;
                case 3:
                    holder.tvName.setText("예약카드");
                    break;
            }
            return convertView;
        }

        @Override
        public long getHeaderId(int position) {
            //return the first character of the country as ID because this is what headers are based upon
            return mList.get(position).headerId;
        }

        class HeaderViewHolder {
            TextView tvName;
            TextView tvCount;
        }

        class ViewHolder {
            TextView tvName;
        }

    }
}
