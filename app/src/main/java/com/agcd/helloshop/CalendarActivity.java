package com.agcd.helloshop;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.andexert.calendarlistview.library.DatePickerController;
import com.andexert.calendarlistview.library.DayPickerView;
import com.andexert.calendarlistview.library.SimpleMonthAdapter;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jspark on 2017. 5. 22..
 */

public class CalendarActivity extends Activity implements DatePickerController {

    @BindView(R.id.calendar_pickerView) DayPickerView mPickerView;


    @OnClick(R.id.calendar_btnX)
    public void onClickBtnX(View v){
        this.finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        ButterKnife.bind(this);
        mPickerView.setController(this);
        Calendar now = Calendar.getInstance();

        mPickerView.scrollToMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH));
    }

    @Override
    public int getMaxYear() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.YEAR, 1);
        return now.get(Calendar.YEAR);
    }

    @Override
    public int getMinYear() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.YEAR, -1);
        return now.get(Calendar.YEAR);
    }

    @Override
    public int getCurrentYear() {
        Calendar now = Calendar.getInstance();
        return now.get(Calendar.YEAR);
    }

    @Override
    public void onDayOfMonthSelected(int year, int month, int day) {
        Log.d("Calendar", "day is" + day);
    }

    @Override
    public void onDateRangeSelected(SimpleMonthAdapter.SelectedDays<SimpleMonthAdapter.CalendarDay> selectedDays) {

    }
}
