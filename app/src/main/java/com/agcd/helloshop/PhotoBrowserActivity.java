package com.agcd.helloshop;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.agcd.helloshop.util.HackyViewPager;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.PicassoEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.lang.reflect.Array;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jspark on 2017. 7. 8..
 */

public class PhotoBrowserActivity extends Activity implements ViewPager.OnPageChangeListener {
    private final static int REQUEST_CODE_CHOOSE = 0x0001;
    @BindView(R.id.photo_browser_viewPager) HackyViewPager viewPager;


    @OnClick(R.id.photo_browser_btnClose)
    public void onClickClose(View v){
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_browser);
        ButterKnife.bind(this);

        ArrayList<String> photoList = new ArrayList<>();
        photoList.add("http://uhd.img.topstarnews.net/wys3/file_attach/2016/05/20/1463718710-99-org.jpg") ;
        photoList.add("http://uhd.img.topstarnews.net/wys3/file_attach/2016/05/13/1463135733-20-org.jpg") ;
        photoList.add("http://img.gqkorea.co.kr/gq/2016/03/style_56f0c74bca8eb.jpg") ;
        ViewPagerAdapter adapter = new ViewPagerAdapter();
        adapter.setPhotoList(photoList);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    class ViewPagerAdapter extends PagerAdapter {
        private ArrayList<String> mPhotoList;

        public void setPhotoList(ArrayList<String> photoList) {
            this.mPhotoList = photoList;
        }

        @Override
        public int getCount() {
            return mPhotoList.size();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(PhotoBrowserActivity.this).inflate(R.layout.view_photo_browser, null);
            PhotoView iv = (PhotoView) view.findViewById(R.id.photoView_iv);
            Picasso.with(PhotoBrowserActivity.this).load(mPhotoList.get(position)).into(iv);
            ImageButton ivDelete = (ImageButton)view.findViewById(R.id.photoView_btnDelete);
            ImageButton ivEdit = (ImageButton)view.findViewById(R.id.photoView_btnEdit);

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Matisse.from(PhotoBrowserActivity.this)
                            .choose(MimeType.ofImage())
                            .showSingleMediaType(true)
                            .countable(true)
                            .maxSelectable(1)
                            .capture(true)
                            .captureStrategy(new CaptureStrategy(true, "com.agcd.helloshop.fileprovider"))
                            .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                            .thumbnailScale(0.5f)
                            .theme(R.style.Matisse_Dracula)
                            .imageEngine(new PicassoEngine())
                            .forResult(REQUEST_CODE_CHOOSE);
                }
            });

            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CHOOSE
                && resultCode == RESULT_OK) {
           // mSelected = Matisse.obtainResult(data);
            //Log.d("Matisse", "mSelected: " + mSelected);
            //Picasso.with(this).load(mSelected.get(0)).fit().into(iv1);
            //iv1.setTag(ALREADY_FILL);
        }
    }
}
