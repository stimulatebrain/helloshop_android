package com.agcd.helloshop;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.agcd.helloshop.fragment.CustomerFragment;
import com.agcd.helloshop.fragment.DashboardFragment;
import com.agcd.helloshop.fragment.ReserveFragment;
import com.agcd.helloshop.fragment.SettingFragment;
import com.agcd.helloshop.model.ModelDashboardExpert;
import com.agcd.helloshop.model.ModelShop;
import com.agcd.helloshop.rest.APIHandler;
import com.agcd.helloshop.rest.ModelError;
import com.agcd.helloshop.rest.RestClient;
import com.agcd.helloshop.rest.ServerAPI;
import com.agcd.helloshop.util.Constants;
import com.agcd.helloshop.util.JFragmentManager;
import com.agcd.helloshop.util.LocalData;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jspark on 2017. 1. 20..
 */

public class MainTabActivity extends Activity {
    @BindView(R.id.maintab_tab_dashboard) View mViewDashboard;
    @BindView(R.id.maintab_tab_reserve) View mViewReserve;
    @BindView(R.id.maintab_tab_add) View mViewAdd;
    @BindView(R.id.maintab_tab_customer) View mViewCustomer;
    @BindView(R.id.maintab_tab_setting) View mViewSetting;

    @BindView(R.id.maintab_btn_noti) ImageButton mButtonNoti;
    @BindView(R.id.maintab_tab) View mBottomView;



    private JFragmentManager mFragmentManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintab);
        ButterKnife.bind(this);

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                ).withListener(new MultiplePermissionsListener() {
            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {}
            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
        }).check();

        
        // Shop Info 가져오기. 추후에 로그인할때로 변경.
        RestClient.getApi().getShopInfo(
                Constants.TEST_SHOP_ID,
                ServerAPI.TEST_AUTH_TOKEN).enqueue
                (new APIHandler<ModelShop>(this) {
                    @Override
                    public void onSuccess(ModelShop dataObject) {
                        LocalData.setShopModel(dataObject);
                    }
                    @Override
                    public void onFailureOrLogicError(ModelError error) {

                    }
                });


        HashMap<String, Fragment> fragmentMap = new HashMap<>();
        // 프래그먼트를 생성한다.
        fragmentMap.put("dashboard", new DashboardFragment());
        fragmentMap.put("reserve", new ReserveFragment());
        fragmentMap.put("customer", new CustomerFragment());
        fragmentMap.put("setting", new SettingFragment());
        mFragmentManager = new JFragmentManager(fragmentMap);

        onTabBtnClick(mViewDashboard);
    }

    @OnClick(R.id.maintab_btn_noti)
    public void onNotiClick(View view){
        Intent i = new Intent(this, NotificationActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.maintab_btn_search)
    public void onSearchCLick(View view){
        Intent i = new Intent(this, SearchActivity.class);
        startActivity(i);
    }

    @OnClick({R.id.maintab_tab_dashboard, R.id.maintab_tab_reserve, R.id.maintab_tab_customer, R.id.maintab_tab_setting})
    public void onTabBtnClick(View view){
        mViewDashboard.setSelected(false);
        mViewCustomer.setSelected(false);
        mViewReserve.setSelected(false);
        mViewSetting.setSelected(false);

        view.setSelected(true);
        if( view == mViewDashboard){
            mFragmentManager.changeFragment(this, "dashboard");
        }else if( view == mViewReserve){
            mFragmentManager.changeFragment(this, "reserve");
        }else if ( view == mViewCustomer){
            mFragmentManager.changeFragment(this, "customer");
        }else {// mViewSetting
            mFragmentManager.changeFragment(this, "setting");
        }
    }

    @OnClick(R.id.maintab_tab_add)
    public void onTabAddBtnClick(View view){
        Intent i = new Intent(this, CardCreateActivity.class);
        startActivity(i);

    }

    public void setToggleBottomView(){
        if( mBottomView.getVisibility() == View.GONE) {
            mBottomView.setVisibility(View.VISIBLE);
        }else {
            mBottomView.setVisibility(View.GONE);
        }
    }



}

