package com.agcd.helloshop.rest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public abstract class APICallback<T> implements Callback<T> {

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        T retrunObject = response.body();
        if (retrunObject instanceof ModelError) {
            onLogicError( (ModelError)retrunObject );
        } else {
            onSuccess( retrunObject );
        }
    }

    //@Override
    //public void onFailure(Throwable t) {
        //onFailure(t);
    //}

    abstract public void onSuccess(T dataObject);
    abstract public void onLogicError(ModelError returnObject);
    //abstract public void onFailure(Throwable t);

}
