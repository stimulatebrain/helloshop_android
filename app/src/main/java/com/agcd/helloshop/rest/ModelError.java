package com.agcd.helloshop.rest;

import java.util.Objects;

public class ModelError {
    public int status;
    public boolean success;
    public String message;
    public ModelErrorDetail errorDetail;

    public ModelError(boolean success, int status, String message, ModelErrorDetail error) {
        this.success = success;
        this.status = status;
        this.message = message;
        this.errorDetail = error;
    }
}
