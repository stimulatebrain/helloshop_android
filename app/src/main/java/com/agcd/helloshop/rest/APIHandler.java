package com.agcd.helloshop.rest;

import android.content.Context;
import android.widget.Toast;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public abstract class APIHandler<T> implements Callback<T> {

    public Context mContext;
    public APIHandler (Context context) {
        this.mContext = context;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        T retrunObject = response.body();
        if (response != null && !response.isSuccessful() && response.errorBody() != null) {
            try {
                ModelError myError = (ModelError) RestClient.getRetrofit().responseBodyConverter(
                        ModelError.class, ModelError.class.getAnnotations()).convert(response.errorBody());
                Toast.makeText(mContext, myError.message, Toast.LENGTH_LONG).show();
                onFailureOrLogicError(myError);
            } catch (IOException e) {
                e.printStackTrace();
                onFailureOrLogicError(new ModelError(false, 500, "Internal Server Error", new ModelErrorDetail()) );
            }
            //DO ERROR HANDLING HERE
            return;
        }

        onSuccess( retrunObject );
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        String message = t.getMessage();
        if (message==null || message.length()==0) {
            message = "ERROR!";
        }
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
        onFailureOrLogicError(new ModelError(false, 400, "", null));
    }

    abstract public void onSuccess(T dataObject);
    abstract public void onFailureOrLogicError(ModelError error);

}
