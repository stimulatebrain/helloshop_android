package com.agcd.helloshop.rest;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class ItemTypeAdapterFactory implements TypeAdapterFactory {

    public <T> TypeAdapter<T> create(Gson gson, final TypeToken<T> type) {

        final TypeAdapter<T> delegate = gson.getDelegateAdapter(this, type);
        final TypeAdapter<JsonElement> elementAdapter = gson.getAdapter(JsonElement.class);

        return new TypeAdapter<T>() {

            public void write(JsonWriter out, T value) throws IOException {
                delegate.write(out, value);
            }

            public T read(JsonReader in) throws IOException {

                JsonElement jsonElement = elementAdapter.read(in);
                if (jsonElement.isJsonObject()) {
                    JsonObject jsonObject = jsonElement.getAsJsonObject();
                    if (jsonObject.has("success") && !jsonObject.get("success").getAsBoolean())
                    {
                        boolean success = jsonObject.get("success").isJsonNull()? false : jsonObject.get("success").getAsBoolean();
                        int status = jsonObject.get("status").isJsonNull()? 0 : jsonObject.get("status").getAsInt();
                        String message = jsonObject.has("message") ? jsonObject.get("message").getAsString() : null;

//                        String returnData = jsonObject.get("_returnData").isJsonNull()? null : jsonObject.get("_returnData").getAsString();
                        ModelErrorDetail returnData = null;
//                        if (jsonObject.has("error") && !jsonObject.get("error").isJsonNull()) {
//                            returnData = (ModelErrorDetail) delegate.fromJsonTree( jsonObject.get("error") );
//                        }
                        return delegate.fromJsonTree(jsonObject);
                    }
                    //if (jsonObject.has("_returnData") && (jsonObject.get("_returnData").isJsonObject() || jsonObject.get("_returnData").isJsonArray()))
                    if (jsonObject.has("data") && !jsonObject.get("data").isJsonNull())
                    {
//                        if (jsonObject.has("status") && jsonObject.get("status").getAsInt() != 200) {
//                            final String returnMessage = jsonObject.get("_returnMessage").isJsonNull()? null : jsonObject.get("_returnMessage").getAsString();
//                            if (returnMessage!=null && returnMessage.length()>0) {
//                                Handler mHandler = new Handler(Looper.getMainLooper());
//                                mHandler.postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        //Toast.makeText(AnalyticsApplication.context, returnMessage, Toast.LENGTH_LONG).show();
//
//                                    }
//                                }, 0);
//                            }
//                        }
                        // data 중첩시 이슈 해결. success 있는거로 바꿈.
                        jsonElement = jsonObject.get("data");
                    }
                }

                return delegate.fromJsonTree(jsonElement);
            }
        }.nullSafe();
    }
}
