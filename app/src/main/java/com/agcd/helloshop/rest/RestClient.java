package com.agcd.helloshop.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.Retrofit;


public class RestClient {

//    public static final String BASE_URL = "http://192.168.0.2:8080/";
    public static final String BASE_URL = "http://helloshop-api-jws.azurewebsites.net/api/v1/";
    private static ServerAPI serverApi;
    private static Retrofit retrofit;

    public static ServerAPI getApi()
    {
        if (serverApi==null) {
            Gson gson = new GsonBuilder()
                    .registerTypeAdapterFactory(new ItemTypeAdapterFactory()) // This is the important line ;)
                    .create();

            // for log
//            OkHttpClient client = new OkHttpClient();
//            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//            client.interceptors().add(interceptor);

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
//                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            serverApi = retrofit.create(ServerAPI.class);

        }
        return serverApi;
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }
}
