package com.agcd.helloshop.rest;

import com.agcd.helloshop.model.ModelDashboard;
import com.agcd.helloshop.model.ModelDashboardExpert;
import com.agcd.helloshop.model.ModelGuest;
import com.agcd.helloshop.model.ModelMemo;
import com.agcd.helloshop.model.ModelNotification;
import com.agcd.helloshop.model.ModelNotificationWrapper;
import com.agcd.helloshop.model.ModelReserve;
import com.agcd.helloshop.model.ModelService;
import com.agcd.helloshop.model.ModelShop;
import com.agcd.helloshop.model.ModelStaff;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ServerAPI {
    static int PAGE_SIZE = 30;
    public final static String TEST_AUTH_TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjBjZDk4MDYwMTFlYzAxMGJkYjA2MzFkYzQxMDk2YzZiOWEzNzBlMGFmYTRhN2NjNGQ2ZmRjNTVjMjg3OTI3ZDI3MGEyM2UzMjEyMGYyOTNiIn0.eyJhdWQiOiIxIiwianRpIjoiMGNkOTgwNjAxMWVjMDEwYmRiMDYzMWRjNDEwOTZjNmI5YTM3MGUwYWZhNGE3Y2M0ZDZmZGM1NWMyODc5MjdkMjcwYTIzZTMyMTIwZjI5M2IiLCJpYXQiOjE0OTU2OTMyMzMsIm5iZiI6MTQ5NTY5MzIzMywiZXhwIjoxODExMjI2MDMzLCJzdWIiOiI0NjEiLCJzY29wZXMiOltdfQ.l4e44tOWuzIKh7bHxcCngxw1VyK-Xki0_lSNcxPmUS_uc7A49Z8WpC9_Rc4mWYliotBjiyI5qi5LbDkGHIEw2q2NiYzMR0HKSvNdCtmYJA4ep3rgXqvFpWVN1_b48kCee8RODvgQTPQfWEWLGnaHSLC8ZtbTPzxXjTAYTzCmV4_QCNXcBYv8wkL2FxnWe5fyTIfVsu1RrVYm6AwdBmpazfBbuNb7DkN1qq4ct1cSSg_Skk38NdYKiM8ZB_ikeEbSXuQnY3w6MZvTGe0vEhXm2Ayunh-fKbmyJY0ytHL2MMOMfwVbVrzowlEauBbvsDtok5InmReIbaSRJAvEoTZj4_7oTlc_NTyLLwk2Gg8dxaEmKrjhY3iL3nEHHQQMQPkZFFfadgkOvde3cWx5GWiuvST2UTlzoIKok8TYWEm6xXUSdeEsKM2ST_HojC1QsN4dOhJunoOib5cuyJc4e2Cz_eYlZujLUT38A1hd0FjASNf92wiEt4pJ4P1ZkISADe6gKGhstZJnnlexJhhESsDO8mv3VfWA-unJeTOL49EdDSosEd5ENefJ83npBpNnJjUqPHTaUEdc6HqRErOCerpkHdF8mcV-h3kt9j9gMgGdB-IKORDb1AzoORIOHTG70wzq6-SlyrXPVqcSCDSXOxM0oi7s5IkP6FjJXSZqDBIUkiw";

    @POST("shops/{shop_id}/scheduleListInfo/mobile-reserve")
    Call<List<ModelReserve>> loadScheduleListInfo(
            @Path("shop_id") int shopId,
            @Query("start_date") String startDate,
            @Query("end_date") String endDate,
            @Query("staff_id") int staffId,
            @Header("Authorization")String auth
    );

    // Dashboard Fragment
    @POST("shops/{shop_id}/dashboard/total-reservation-count")
    Call<ModelDashboard> getDashboardReservationCount(@Path("shop_id") int shopId,
                                             @Query("start_date") String startDate,
                                             @Query("end_date") String endDate, @Header("Authorization")String auth);


    @POST("shops/7/dashboard/expert-schedule-count")
    Call<List<ModelDashboardExpert>> getDashboardReservationExpertCount(
            @Path("shop_id") int shopId,
            @Query("start_date") String startDate,
            @Query("end_date") String endDate,
            @Header("Authorization")String auth);

    @GET("shops/{shop_id}/notifications")
    Call<List<ModelNotification>> loadNotificationsFromDashboard(
            @Path("shop_id") int shopId,
            @Header("Authorization")String auth);

    @PUT("shops/{shop_id}/schedules/{id}")
    Call<ModelReserve> getSchedule(
            @Path("shop_id") int shopId,
            @Path("id") int scheduleId,
            @Query("include") String include,
            @Header("Authorization")String auth);

    @GET("shops/{shop_id}/guests/{id}/reserve-list")
    Call<List<ModelReserve>> loadGuestReserveList(
            @Path("shop_id") int shopId,
            @Path("id") int guestId,
            @Query("include") String include,
            @Header("Authorization")String auth
    );


    @GET("shops/{shop_id}/guests")
    Call<List<ModelGuest>> loadGuestList(
            @Path("shop_id") int shopId,
            @Query("guest_class") String guestGradeValue,
            @Header("Authorization")String auth
    );

    @GET("shops/{shop_id}/guests")
    Call<List<ModelGuest>> loadGuestListByName(
            @Path("shop_id") int shopId,
            @Query("guest_name") String guestName,
            @Header("Authorization")String auth
    );

    @GET("shops/{shop_id}/guests")
    Call<List<ModelGuest>> loadGuestListByPhone(
            @Path("shop_id") int shopId,
            @Query("guest_mobile") String guestMobile,
            @Header("Authorization")String auth
    );

    @POST("shops/{shop_id}/search/guest-card")
    Call<List<ModelGuest>> searchGuestList(
            @Path("shop_id") int shopId,
            @Query("keyword") String keyword,
            @Header("Authorization")String auth
    );

    @POST("shops/{shop_id}/search/schedule-card")
    Call<List<ModelReserve>> searchScheduleList(
            @Path("shop_id") int shopId,
            @Query("keyword") String keyword,
            @Query("status") String statusValue,
            @Header("Authorization")String auth
    );


    @POST("shops/{shop_id}/search/guest-memo")
    Call<List<ModelMemo>> searchMemoList(
            @Path("shop_id") int shopId,
            @Query("keyword") String keyword,
            @Header("Authorization")String auth
    );

    @PUT("shops/{shop_id}")
    Call<ModelShop> getShopInfo(
            @Path("shop_id") int shopId,
            @Header("Authorization")String auth
    );

    @GET("shops/{shop_id}/staffs")
    Call<List<ModelStaff>> loadStaff(
            @Path("shop_id") int shopId,
            @Header("Authorization")String auth
    );

    @GET("shops/{shop_id}/services")
    Call<List<ModelService>> loadService(
            @Path("shop_id") int shopId,
            @Query("sex") int sex,
            @Header("Authorization")String auth
    );

    @POST("shops/{shop_id}/guests")
    Call<ModelGuest> uploadGuest(
            @Path("shop_id") int shopId,
            @Query("guest_class") String guestClass,
            @Query("guest_name") String guestName,
            @Query("guest_mobile") String guestMobile,
            @Header("Authorization")String auth
    );

    @POST("shops/{shop_id}/scheduleListInfo/mobile-reserve-possible")
    Call<List<ModelReserve>> loadPossibleTimes(
            @Path("shop_id") int shopId,
            @Query("start_date") String startDate,
            @Query("end_date") String endDate,
            @Query("staff_id") int staffId,
            @Header("Authorization")String auth
    );



//    @POST("shops/{shop_id}/schedules")
//    Call<>

//    @POST("data/privacyPolicy")
//    Call<String> loadPrivacyPolicy();
//
//
//    @POST("user/getAllPhoto")
//    Call<List<ModelPhoto>> getAllUsersPhoto(
//            @Query("userIndex") String userIndex,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//
//    @POST("user/getUserInfo")
//    Call<ModelUser> getUserInfo(
//            @Query("userIndex") String userIndex
//    );
//    @POST("user/autoSignIn")
//    Call<ModelUser> autoSignIn(
//            @Query("userIndex") String userIndex,
//            @Query("deviceType") String deviceType,
//            @Query("deviceToken") String deviceToken,
//            @Query("version") String version
//    );
//
//    @POST("user/changePassword")
//    Call<String> changePassword(
//            @Query("userIndex") String userIndex,
//            @Query("currentPassword") String currentPassword,
//            @Query("newPassword") String newPassword
//    );
//    @POST("user/checkPassword")
//    Call<String> checkPassword(
//            @Query("userIndex") String userIndex
//    );
//
//    @POST("user/updateProfile")
//    Call<ModelUser> updateProfile(
//            @Query("index") String index,
//            @Query("id") String id,
//            @Query("email") String email,
//            @Query("thumbnailUrl") String thumbnailUrl,
//            @Query("isMale") int isMale,
//            @Query("birthYear") String birthYear,
//            @Query("isPrivate") int isPrivate,
//            @Query(value="description", encoded=true) String description,
//            @Query("accessToken") String accessToken
//    );
//    @POST("user/updateThumbnail")
//    Call<String> updateThumbnail(
//            @Query("userIndex") String userIndex,
//            @Query("thumbnailUrl") String thumbnailUrl,
//            @Query("accessToken") String accessToken
//    );
//    @POST("user/signIn")
//    Call<ModelUser> signIn(
//            @Query("id") String userId,
//            @Query("email") String email,
//            @Query("password") String password,
//            @Query("deviceType") String deviceType,
//            @Query("deviceToken") String deviceToken
//            );
//    @POST("user/checkEmail")
//    Call<ModelUser> checkUserEmail(
//            @Query("email") String email
//    );
//    @POST("user/signUp")
//    Call<ModelUser> signUp(
//            @Query("id") String id,
//            @Query("password") String password,
//            @Query("thumbnailUrl") String thumbnailUrl,
//            @Query("isMale") String isMale,
//            @Query("birthYear") String birthYear,
//            @Query("isPrivate") String isPrivate,
//            @Query("deviceType") String deviceType,
//            @Query("deviceToken") String deviceToken,
//            @Query(value="name", encoded=true) String name,
//            @Query(value="description", encoded=true) String description,
//            @Query("email") String email,
//            @Query("countryCode") String countryCode,
//            @Query("integrateId") String integrateId,
//            @Query("integrateType") String integrateType,
//            @Query("version") String version
//    );
//
//    @POST("user/integrateSignIn")
//    Call<ModelUser> integrateSignIn(
//            @Query("integrateId") String integrateId,
//            @Query("integrateType") String integrateType,
//            @Query("deviceType") String deviceType,
//            @Query("deviceToken") String deviceToken
//    );
//
//    @POST("user/follow")
//    Call<ModelUser> follow(
//            @Query("followerIndex") String followerIndex,
//            @Query("followingIndex") String followingIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("user/unfollow")
//    Call<ModelUser> unfollow(
//            @Query("followerIndex") String followerIndex,
//            @Query("followingIndex") String followingIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("user/block")
//    Call<Integer> blockUser(
//            @Query("userIndex") String userIndex,
//            @Query("targetUserIndex") String targetUserIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("user/unblock")
//    Call<Integer> unblockUser(
//            @Query("userIndex") String userIndex,
//            @Query("targetUserIndex") String targetUserIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("user/report")
//    Call<String> reportUser(
//            @Query("reporterIndex") String reporterIndex,
//            @Query("targetUserIndex") String targetUserIndex,
//            @Query("accessToken") String accessToken
//    );
//
//
//    @POST("posts/recentList")
//    Call<List<ModelPosts>> recentPostsList(
//            @Query("requesterIndex") String requesterIndex,
//            @Query("loadRowCount") String loadRowCount,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//    @POST("posts/featuredList")
//    Call<List<ModelPosts>> featuredPostsList(
//            @Query("requesterIndex") String requesterIndex,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//    @POST("posts/followingList")
//    Call<List<ModelPosts>> followingPostsList(
//            @Query("requesterIndex") String requesterIndex,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//    @POST("posts/voteBackList")
//    Call<List<ModelPosts>> voteBackList(
//            @Query("requesterIndex") String requesterIndex,
//            @Query("loadRowCount") String loadRowCount,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//    @POST("posts/requestedList")
//    Call<List<ModelPosts>> receivedRequestList(
//            @Query("requesterIndex") String requesterIndex,
//            @Query("loadRowCount") String loadRowCount,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//
//    @POST("posts/voteList")
//    Call<List<ModelPosts>> votePostsList(
//            @Query("voterIndex") String voterIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//    @POST("posts/myList")
//    Call<List<ModelPosts>> myPostsList(
//            @Query("writerIndex") String writerIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("lastRowIndex") String lastRowIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("posts/followerList")
//    Call<List<ModelPosts>> privatePostsList(
//            @Query("requesterIndex") String requesterIndex,
//            @Query("lastRowIndex") String lastRowIndex,
//            @Query("accessToken") String accessToken
//    );
//
//    @POST("posts/write")
//    Call<ModelPosts> write(
//            @Query(value="title", encoded=true) String title,
//            @Query("writerIndex") String writerIndex,
//            @Query(value="photoArrayAsJson", encoded=true) String photoArrayAsJson,
//            @Query("categoryIndex") String categoryIndex,
//            @Query("voteRequired") String voteRequired,
//            @Query("security") String security,
//            @Query("userIndexesForRequest") String userIndexesForRequest,
//            @Query("accessToken") String accessToken
//    );
//    @POST("posts/loadPhotosInMergedPosts")
//    Call<List<ModelPhoto>> getPhotosInMergedPosts(
//            @Query("writerIndex") String writerIndex,
//            @Query("categoryIndex") String categoryIndex,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//
//    @POST("posts/mergedList")
//    Call<List<ModelPosts>> mergedPostsList(
//            @Query("userIndex") String userIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("lastRowIndex") String lastRowIndex,
//            @Query("thumbnailCount") String thumbnailCount
//    );
//
//    @POST("posts/loadPosts")
//    Call<ModelPosts> loadPosts(
//            @Query("postsIndex") String postsIndex,
//            @Query("requesterIndex") String requesterIndex
//    );
//    @POST("posts/loadMergedPosts")
//    Call<ModelPosts> loadMergedPosts(
//            @Query("postsIndex") String postsIndex,
//            @Query("requesterIndex") String requesterIndex
//    );
//    @POST("posts/delete")
//    Call<String> deletePosts(
//            @Query("postsIndex") String postsIndex,
//            @Query("writerIndex") String writerIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("posts/setPrivacy")
//    Call<ModelPosts> setPostsPrivacy(
//            @Query("postsIndex") String postsIndex,
//            @Query("postsPrivacy") String postsPrivacy,
//            @Query("writerIndex") String writerIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("posts/skip")
//    Call<String> skipVote(
//            @Query("userIndex") String userIndex,
//            @Query("postsIndex") String postsIndex,
//            @Query("accessToken") String accessToken
//    );
//
//
//    @POST("vote/getVoter")
//    Call<List<ModelVote>> getVoter(
//            @Query("postsIndex") String postsIndex,
//            @Query("isMale") String isMale,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//    @POST("vote/getMergedPostsVoter")
//    Call<List<ModelVote>> getMergedPostsVoter(
//            @Query("mergedPostsIndex") String mergedPostsIndex,
//            @Query("isMale") String isMale,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//
//    @POST("user/getProfilePageInfo")
//    Call<ModelUser> getProfilePageInfo(
//            @Query("userIndex") String userIndex,
//            @Query("requesterIndex") String requesterIndex
//    );
//
//    @POST("user/suggestList")
//    Call<List<ModelSuggestUserList>> suggestUserList(
//            @Query("lastRowIndex") String lastRowIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("appVersion") String appVersion,
//            @Query("serverVersion") int serverVersion,
//            @Query("deviceType") String deviceType,
//            @Query("countryCode") String countryCode
//    );
//
//    @POST("user/getFollower")
//    Call<List<ModelUser>> getFollowerList(
//            @Query("userIndex") String userIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//    @POST("user/getFollowing")
//    Call<List<ModelUser>> getFollowingList(
//            @Query("userIndex") String userIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//
//    @POST("user/getPointHistory")
//    Call<List<ModelPointHistory>> getPointHistory(
//            @Query("userIndex") String userIndex,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//    @POST("user/getTotalPoint")
//    Call<String> getTotalPoint(
//            @Query("userIndex") String userIndex
//    );
//
//
//    @POST("user/search")
//    Call<List<ModelUser>> searchUser(
//            @Query(value="id", encoded=true) String id,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//
//
//    @POST("activity/alertList")
//    Call<List<ModelActivityAlert>> recentActivity(
//            @Query("userIndex") String userIndex,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//
//
//    @POST("notice/recentList")
//    Call<List<ModelNotice>> recentNotceList(
//            @Query("lastRowIndex") String lastRowIndex
//    );
//
//
//    @POST("category/popularList")
//    Call<List<ModelCategory>> popularCategoryList(
//            @Query("lastRowIndex") String lastRowIndex
//    );
//    @POST("category/search")
//    Call<List<ModelCategory>> searchCategoryList(
//            @Query(value="searchText", encoded=true) String searchText,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//    @POST("category/register")
//    Call<ModelCategory> registerCategory(
//            @Query(value="name", encoded=true) String name,
//            @Query("writerIndex") String writerIndex,
//            @Query("accessToken") String accessToken
//    );
//
//
//    @POST("comment/getComment")
//    Call<DataSetComment> getCommentOfPosts(
//            @Query("postsIndex") String postsIndex,
//            @Query("postsWriterIndex") String postsWriterIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("loadRowCount") String loadRowCount,
//            @Query("lastRowIndex") String lastRowIndex,
//            @Query("useDeprecatedVersion") boolean useDeprecatedVersion
//    );
//    @POST("comment/getCommentInMergedPosts")
//    Call<DataSetComment> getCommentOfMergedPosts(
//            @Query("postsIndex") String postsIndex,
//            @Query("postsWriterIndex") String postsWriterIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("loadRowCount") String loadRowCount,
//            @Query("lastRowIndex") String lastRowIndex,
//            @Query("useDeprecatedVersion") boolean useDeprecatedVersion
//    );
//
//    @POST("comment/getChildCommentInPosts")
//    Call<DataSetComment> getChildCommentInPosts(
//            @Query("parentIndex") String parentIndex,
//            @Query("postsIndex") String postsIndex,
//            @Query("postsWriterIndex") String postsWriterIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("loadRowCount") String loadRowCount,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//    @POST("comment/getChildCommentInMergedPosts")
//    Call<DataSetComment> getChildCommentInMergedPosts(
//            @Query("parentIndex") String parentIndex,
//            @Query("postsIndex") String postsIndex,
//            @Query("postsWriterIndex") String postsWriterIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("loadRowCount") String loadRowCount,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//
//    @POST("comment/delete")
//    Call<DataSetComment> deleteCommentInPosts(
//            @Query("postsIndex") String postsIndex,
//            @Query("postsWriterIndex") String postsWriterIndex,
//            @Query("commentIndex") String commentIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("parentIndex") String parentIndex,
//            @Query("loadRowCount") String loadRowCount,
//            @Query("accessToken") String accessToken,
//            @Query("useDeprecatedVersion") boolean useDeprecatedVersion
//    );
//    @POST("comment/deleteInMergedPosts")
//    Call<DataSetComment> deleteCommentInMergedPosts(
//            @Query("postsIndex") String postsIndex,
//            @Query("postsWriterIndex") String postsWriterIndex,
//            @Query("commentIndex") String commentIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("parentIndex") String parentIndex,
//            @Query("loadRowCount") String loadRowCount,
//            @Query("accessToken") String accessToken,
//            @Query("useDeprecatedVersion") boolean useDeprecatedVersion
//    );
//    @POST("comment/write")
//    Call<DataSetComment> writeCommentToPosts(
//            @Query("writerIndex") String writerIndex,
//            @Query("postsIndex") String postsIndex,
//            //@Query("postsWriterIndex") String postsWriterIndex,
//            @Query(value="content", encoded=true) String content,
//            @Query("security") String security,
//            @Query("loadRowCount") String loadRowCount,
//            @Query("parentIndex") String parentIndex,
//            @Query("accessToken") String accessToken,
//            @Query("useDeprecatedVersion") boolean useDeprecatedVersion
//    );
//    @POST("comment/writeToMergedPosts")
//    Call<DataSetComment> writeCommentToMergedPosts(
//            @Query("writerIndex") String writerIndex,
//            @Query("postsIndex") String postsIndex,
//            //@Query("postsWriterIndex") String postsWriterIndex,
//            @Query(value="content", encoded=true) String content,
//            @Query("security") String security,
//            @Query("loadRowCount") String loadRowCount,
//            @Query("parentIndex") String parentIndex,
//            @Query("accessToken") String accessToken,
//            @Query("useDeprecatedVersion") boolean useDeprecatedVersion
//    );
//
//    @POST("vote/getVoter")
//    Call<List<ModelVoter>> getVoterInPosts(
//            @Query("postsIndex") String postsIndex,
//            @Query("isMale") String isMale,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//    @POST("vote/getMergedPostsVoter")
//    Call<List<ModelVoter>> getVoterInMergedPosts(
//            @Query("mergedPostsIndex") String mergedPostsIndex,
//            @Query("isMale") String isMale,
//            @Query("lastRowIndex") String lastRowIndex
//    );
//    @POST("vote/getMergedVoteResultByVoter")
//    Call<List<ModelVoteResult>> getMergedVoteResultByVoter(
//            @Query("postsIndex") String postsIndex,
//            @Query("voterIndex") String voterIndex
//    );
//    @POST("vote/getVoteResultByVoter")
//    Call<List<ModelVoteResult>> getVoteResultByVoter(
//            @Query("postsIndex") String postsIndex,
//            @Query("voterIndex") String voterIndex
//    );
//    @POST("vote/voteMergedPosts")
//    Call<List<ModelVoteResult>> voteToMergedPosts(
//            @Query("postsIndex") String postsIndex,
//            @Query(value="postsTitle", encoded=true) String postsTitle,
//            @Query("postsWriterIndex") String postsWriterIndex,
//            @Query(value="voteArrayAsJson", encoded=true) String voteArrayAsJson,
//            @Query("voterIndex") String voterIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("vote/votePosts")
//    Call<List<ModelVoteResult>> voteToPosts(
//            @Query("postsIndex") String postsIndex,
//            @Query(value="postsTitle", encoded=true) String postsTitle,
//            @Query("postsWriterIndex") String postsWriterIndex,
//            @Query(value="voteArrayAsJson", encoded=true) String voteArrayAsJson,
//            @Query("voterIndex") String voterIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("vote/getVoteResult")
//    Call<List<ModelVoteResult>> getVoteResult(
//            @Query("postsIndex") String postsIndex,
//            @Query("isMale") String isMale,
//            @Query("lastRowPhotoIndex") String lastRowPhotoIndex
//    );
//    @POST("vote/getMergedVoteResult")
//    Call<DataSetVoteResult> getMergedVoteResult(
//            @Query("postsIndex") String postsIndex,
//            @Query("isMale") String isMale,
//            @Query("lastRowPhotoIndex") String lastRowPhotoIndex
//    );
//
//    @POST("request/vote")
//    Call<String> requestVote(
//            @Query("postsIndex") String postsIndex,
//            @Query("postsPrivacy") int postsPrivacy,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("receiverIndex") String receiverIndex,
//            @Query("message") String message,
//            @Query("point") String point,
//            @Query("accessToken") String accessToken
//            );
//    @POST("request/list")
//    Call<List<ModelRequest>> loadRequestedList(
//            @Query("requesterIndex") String requesterIndex,
//            @Query("lastRowIndex") String lastRowIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("request/list")
//    Call<List<ModelRequest>> loadRequestedList(
//            @Query("requesterIndex") String requesterIndex,
//            @Query("postsIndex") String postsIndex,
//            @Query("lastRowIndex") String lastRowIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("request/suggestedUserList")
//    Call<List<ModelUser>> loadSuggestedUserListForRequest(
//            @Query("requesterIndex") String requesterIndex,
//            @Query("lastRowIndex") String lastRowIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("request/list")
//    Call<List<ModelRequest>> loadReceivedRequestedList(
//            @Query("receiverIndex") String receiverIndex,
//            @Query("lastRowIndex") String lastRowIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("message/send")
//    Call<List<ModelMessage>> sendMessage(
//            @Query("senderIndex") String senderIndex,
//            @Query("requestIndex") String requestIndex,
//            @Query("receiverIndex") String receiverIndex,
//            @Query("message") String message,
//            @Query("accessToken") String accessToken
//    );
//    @POST("message/list")
//    Call<List<ModelMessage>> loadMessageInRequest(
//            @Query("requestIndex") String requestIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("lastRowIndex") String lastRowIndex,
//            @Query("loadRowCount") int loadRowCount,
//            @Query("accessToken") String accessToken
//    );
//    @POST("request/delete")
//    Call<List<ModelRequest>> deleteRequest(
//            @Query("requestIndex") String requestIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("request/markAsDeleted")
//    Call<List<ModelRequest>> markAsDeletedRequest(
//            @Query("requestIndex") String requestIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("request/object")
//    Call<ModelRequest> loadRequestByIndex(
//            @Query("requestIndex") String requestIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("accessToken") String accessToken
//    );
//    @POST("request/updateList")
//    Call<Integer> updateRequestList(
//            @Query("userIndexesString") String userIndexesString,
//            @Query("postsIndex") String postsIndex,
//            @Query("requesterIndex") String requesterIndex,
//            @Query("accessToken") String accessToken
//    );

}
