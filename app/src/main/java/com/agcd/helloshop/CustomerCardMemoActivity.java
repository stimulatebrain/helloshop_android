package com.agcd.helloshop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.agcd.helloshop.util.Utility;
import com.squareup.picasso.Picasso;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.PicassoEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jspark on 2017. 7. 8..
 */

public class CustomerCardMemoActivity extends Activity {
    private final static int REQUEST_CODE_CHOOSE = 0x0001;
    private final static int ALREADY_FILL  = 0x0002;

    @BindView(R.id.customer_cardMemo_iv1)
    ImageView iv1;

    @BindView(R.id.customer_cardMemo_rlMenuKey)
    RelativeLayout rlMenuKey;

    @BindView(R.id.customer_cardMemo_rlMenu)
    RelativeLayout rlMenu;

    @BindView(R.id.customer_cardMemo_ll)
    LinearLayout llRoot;

    @BindView(R.id.customer_cardMemo_scroll)
    ScrollView mScrollView;

    @OnClick(R.id.customer_cardMemo_btnClose)
    public void onClickClose(View v){
        finish();
    }

    @OnClick(R.id.customer_cardMemo_btnKeyDown)
    public void onClickKeyDown(View v){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_card_memo);
        ButterKnife.bind(this);

        llRoot.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = llRoot.getRootView().getHeight() - llRoot.getHeight();
                if (heightDiff > Utility.dpToPx(200)) {
                    rlMenu.setVisibility(View.GONE);
                    rlMenuKey.setVisibility(View.VISIBLE);
                    ((ViewGroup.MarginLayoutParams)mScrollView.getLayoutParams()).bottomMargin = Utility.dpToPx(41);

                }else {
                    rlMenu.setVisibility(View.VISIBLE);
                    rlMenuKey.setVisibility(View.GONE);
                    ((ViewGroup.MarginLayoutParams)mScrollView.getLayoutParams()).bottomMargin = Utility.dpToPx(0);
                }
            }
        });
    }

    @OnClick({R.id.customer_cardMemo_view1, R.id.customer_cardMemo_view2, R.id.customer_cardMemo_view3})
    public void onClickPhoto(View v){
        CardView view = (CardView) v;
        ImageView iv = (ImageView) view.getChildAt(0);
        if (iv.getTag() == null){
            Matisse.from(CustomerCardMemoActivity.this)
                    .choose(MimeType.ofImage())
                    .showSingleMediaType(true)
                    .countable(true)
                    .maxSelectable(1)
                    .capture(true)
                    .captureStrategy(new CaptureStrategy(true, "com.agcd.helloshop.fileprovider"))
                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                    .thumbnailScale(0.5f)
                    .theme(R.style.Matisse_Dracula)
                    .imageEngine(new PicassoEngine())
                    .forResult(REQUEST_CODE_CHOOSE);
        }else {
            Intent i = new Intent(this, PhotoBrowserActivity.class);
            startActivity(i);
        }
    }
    List<Uri> mSelected;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CHOOSE
                && resultCode == RESULT_OK) {
            mSelected = Matisse.obtainResult(data);
            Log.d("Matisse", "mSelected: " + mSelected);
            Picasso.with(this).load(mSelected.get(0)).fit().into(iv1);
            iv1.setTag(ALREADY_FILL);
        }
    }

}
