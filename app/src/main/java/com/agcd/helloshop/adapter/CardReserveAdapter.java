package com.agcd.helloshop.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agcd.helloshop.CardActivity;
import com.agcd.helloshop.R;
import com.agcd.helloshop.model.ModelMemo;
import com.agcd.helloshop.model.ModelParent;
import com.agcd.helloshop.model.ModelReserve;
import com.alamkanak.weekview.WeekViewEvent;

import org.w3c.dom.Text;

import java.util.ArrayList;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by jspark on 2017. 7. 24..
 */

public class CardReserveAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private ArrayList<ModelReserve> reserveList = new ArrayList<>();
    private ArrayList<ModelMemo> memoList = new ArrayList<>();

    private LayoutInflater inflater;
    public ModelParent.Type currentViewType = ModelParent.Type.TYPE_RESERVE;

    public void setViewType(ModelParent.Type type){
        this.currentViewType = type;
    }

    public void setReserveList(ArrayList<ModelReserve> reserveList) {
        this.reserveList = reserveList;
    }

    public void setMemoList(ArrayList<ModelMemo> memoList) {
        this.memoList = memoList;
    }

    public CardReserveAdapter(Context context) {
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if( currentViewType == ModelParent.Type.TYPE_RESERVE) {
            return reserveList.size();
        }else {
            return memoList.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if( currentViewType == ModelParent.Type.TYPE_RESERVE) {
            return reserveList.get(position);
        }else {
            return memoList.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 5;
    }

    @Override
    public int getItemViewType(int position) {
        return ((ModelParent)getItem(position)).type.getValue();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Object holder;
        ModelParent model = (ModelParent) getItem(position);
        if (convertView == null) {
            if( model.type == ModelParent.Type.TYPE_RESERVE){
                convertView = inflater.inflate(R.layout.cell_card_reserve, parent, false);
                holder = new ReserveViewHolder();

                ((ReserveViewHolder)holder).tvName = (TextView) convertView.findViewById(R.id.cell_cardReserve_tvName);
                ((ReserveViewHolder)holder).tvDesc = (TextView) convertView.findViewById(R.id.cell_cardReserve_tvDesc);
                ((ReserveViewHolder)holder).tvStatus = (TextView) convertView.findViewById(R.id.cell_cardReserve_tvStatus);
                ((ReserveViewHolder)holder).tvType = (TextView) convertView.findViewById(R.id.cell_cardReserve_tvType);
                ((ReserveViewHolder)holder).tvRelativeDate = (TextView) convertView.findViewById(R.id.cell_cardReserve_tvRelativeDate);

            }else {
                convertView = inflater.inflate(R.layout.cell_customer_memo, parent, false);
                holder = new MemoViewHolder();
            }
            convertView.setTag(holder);
        } else {
            holder = convertView.getTag();
        }
        if(model.type == ModelParent.Type.TYPE_RESERVE){
            ModelReserve reserve = (ModelReserve)model;
            ReserveViewHolder holderReserve = (ReserveViewHolder)holder;
            holderReserve.tvName.setText(reserve.getService_name());
            holderReserve.tvStatus.setText(WeekViewEvent.statusForCode(reserve.getStatus()).getDescription());
            if(reserve.getService() != null && reserve.getService().is_deposit()){
                holderReserve.tvType.setVisibility(View.VISIBLE);
            }else {
                holderReserve.tvType.setVisibility(View.GONE);
            }

            // TODO EDIT!
            holderReserve.tvRelativeDate.setVisibility(View.GONE);
            holderReserve.tvDesc.setText(reserve.getGuest_memo());

        }else {

        }
        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        final CardReserveAdapter.HeaderViewHolder holder;
        if (convertView == null) {
            holder = new CardReserveAdapter.HeaderViewHolder();
            convertView = inflater.inflate(R.layout.cell_card_reserve_header, parent, false);

            holder.llReserve = (LinearLayout) convertView.findViewById(R.id.cell_cardHeader_llReserve);
            holder.llMemo = (LinearLayout) convertView.findViewById(R.id.cell_cardHeader_llMemo);
            holder.tvMemo = (TextView) convertView.findViewById(R.id.cell_cardHeader_tvMemo);
            holder.tvReserve = (TextView) convertView.findViewById(R.id.cell_cardHeader_tvReserve);
            holder.tvReserveCount = (TextView) convertView.findViewById(R.id.cell_cardHeader_tvReserveCount);

            if( currentViewType == ModelParent.Type.TYPE_RESERVE){
                holder.tvReserve.setTextColor(Color.parseColor("#191919"));
                holder.tvReserve.setTypeface(Typeface.DEFAULT_BOLD);
                holder.tvReserveCount.setTextColor(Color.parseColor("#E60B25"));
                holder.tvReserveCount.setTypeface(Typeface.DEFAULT_BOLD);

                holder.tvMemo.setTextColor(Color.parseColor("#a3a3a3"));
                holder.tvMemo.setTypeface(Typeface.DEFAULT);
            }else {
                holder.tvReserve.setTextColor(Color.parseColor("#a3a3a3"));
                holder.tvReserve.setTypeface(Typeface.DEFAULT);
                holder.tvReserveCount.setTextColor(Color.parseColor("#a3a3a3"));
                holder.tvReserveCount.setTypeface(Typeface.DEFAULT);

                holder.tvMemo.setTextColor(Color.parseColor("#191919"));
                holder.tvMemo.setTypeface(Typeface.DEFAULT_BOLD);

            }
            holder.llReserve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setViewType(ModelParent.Type.TYPE_RESERVE);
                    CardReserveAdapter.this.notifyDataSetChanged();
                }
            });
            holder.llMemo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setViewType(ModelParent.Type.TYPE_MEMO);
                    CardReserveAdapter.this.notifyDataSetChanged();
                }
            });
            convertView.setTag(holder);
        } else {
            // convertView.
        }
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        //return the first character of the country as ID because this is what headers are based upon
        return 0;
    }

    class HeaderViewHolder {
        LinearLayout llReserve;
        LinearLayout llMemo;
        TextView tvReserveCount;
        TextView tvReserve;
        TextView tvMemo;
    }

    class ReserveViewHolder {
        TextView tvName;
        TextView tvType;
        TextView tvStatus;
        TextView tvDesc;
        TextView tvRelativeDate;

    }

    class MemoViewHolder {

    }

}