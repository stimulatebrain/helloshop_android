package com.agcd.helloshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agcd.helloshop.CustomerMessageActivity;
import com.agcd.helloshop.R;
import com.agcd.helloshop.fragment.CustomerFragment;
import com.agcd.helloshop.model.ModelGuest;
import com.agcd.helloshop.model.ModelMemo;
import com.agcd.helloshop.model.ModelParent;
import com.agcd.helloshop.model.ModelReserve;
import com.agcd.helloshop.popup.PopupBottomDialog;
import com.agcd.helloshop.util.OnPopupStringResultListener;
import com.alamkanak.weekview.WeekViewEvent;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import butterknife.OnClick;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by uxight on 2017. 7. 26.
 */

public class CustomerListAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    public CustomerListAdapter(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    private List<ModelGuest> mCustomerList = new ArrayList<ModelGuest>();
    private List<ModelGuest> mSelectedList = new ArrayList<ModelGuest>();


    private OnChangeSelectedListener mOnChangeSelected;
    public interface OnChangeSelectedListener {
        void onChange(List<ModelGuest> selectedList);
    }
    private boolean mIsSelectMode = false;
    public boolean isSelectMode() {
        return mIsSelectMode;
    }
    public void setSelectModeOn(OnChangeSelectedListener onChangeSelected) {
        mIsSelectMode = true;
        mOnChangeSelected = onChangeSelected;
        notifyDataSetChanged();
    }
    public void setSelectModeOff() {
        mSelectedList.clear();
        mIsSelectMode = false;
        notifyDataSetChanged();
    }


    public void clickCheck(int position) {
        ModelGuest guest = mCustomerList.get(position);
        if (mSelectedList.contains(guest)) {
            mSelectedList.remove(guest);
        } else {
            mSelectedList.add(guest);
        }
        notifyDataSetChanged();

        if (mOnChangeSelected != null) {
            mOnChangeSelected.onChange(mSelectedList);
        }
    }

//    public class CustomerAdapter extends BaseAdapter implements StickyListHeadersAdapter {

//        private boolean mIsSelectMode = false;

        public void setCustomerList(List<ModelGuest> customerList) {
            this.mCustomerList = customerList;
        }

//        public boolean toggleShowCheckBtn(){
//            mIsSelectMode = !mIsSelectMode;
//            return mIsSelectMode;
//        }


        @Override
        public int getCount() {
            return mCustomerList.size();
        }

        @Override
        public Object getItem(int position) {
            return mCustomerList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.cell_customer, parent, false);
                holder.tvName = (TextView) convertView.findViewById(R.id.customercell_tv_name);
                holder.tvPhone = (TextView) convertView.findViewById(R.id.customercell_tv_phone);
                holder.btnCheck = (ImageButton) convertView.findViewById(R.id.customercell_btn_check);
                holder.btnCall = (ImageButton) convertView.findViewById(R.id.customercell_btn_phone);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.btnCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickCheck(position);
                }
            });
            holder.btnCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("TAG", "CALL");
                }
            });

            ModelGuest guest = mCustomerList.get(position);

            holder.tvName.setText(guest.guest_name);
            holder.tvPhone.setText(guest.guest_mobile);

            if (mIsSelectMode)
            {
                holder.btnCheck.setVisibility(View.VISIBLE);
                if (mSelectedList.contains(guest)) {
                    holder.btnCheck.setSelected(true);
                } else {
                    holder.btnCheck.setSelected(false);
                }
            }
            else
            {
                holder.btnCheck.setVisibility(View.GONE);
            }

            return convertView;
        }

        @Override
        public View getHeaderView(int position, View convertView, ViewGroup parent) {
            HeaderViewHolder holder;
            if (convertView == null) {
                holder = new HeaderViewHolder();
                convertView = mInflater.inflate(R.layout.cell_customer_header, parent, false);
                holder.text = (TextView) convertView.findViewById(R.id.customercell_tv_header);
                convertView.setTag(holder);
            } else {
                holder = (HeaderViewHolder) convertView.getTag();
            }
            //set header text as first char in name
            String headerText = mCustomerList.get(position).getGrade();
            holder.text.setText(headerText);
            return convertView;
        }

        @Override
        public long getHeaderId(int position) {
            return 0;
            //return the first character of the country as ID because this is what headers are based upon
            //return mCustomerList.get(position).getGrade().charAt(0);
        }

        class HeaderViewHolder {
            TextView text;
        }

        public class ViewHolder {
            TextView tvName;
            TextView tvPhone;
            ImageButton btnCheck;
            ImageButton btnCall;
        }

//    }



}