package com.agcd.helloshop.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.agcd.helloshop.CardActivity;
import com.agcd.helloshop.R;
import com.agcd.helloshop.model.ModelDashboardExpert;
import com.agcd.helloshop.model.ModelReserve;
import com.agcd.helloshop.rest.APIHandler;
import com.agcd.helloshop.rest.ModelError;
import com.agcd.helloshop.rest.RestClient;
import com.agcd.helloshop.rest.ServerAPI;
import com.agcd.helloshop.util.Constants;
import com.agcd.helloshop.util.CustomViewPager;
import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewDataDelegate;
import com.alamkanak.weekview.WeekViewEvent;
import com.astuetz.PagerSlidingTabStrip;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jspark on 2017. 1. 20..
 */

public class ReserveFragment extends Fragment implements ViewPager.OnPageChangeListener {
    @BindView(R.id.reserve_pager) CustomViewPager mPager;
    @BindView(R.id.reserve_tabs) PagerSlidingTabStrip mTabs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reserve, container, false);
        ButterKnife.bind(this, rootView);

        final ReservePagerAdapter adapter = new ReservePagerAdapter(this);
        mPager.setAdapter(adapter);
        mPager.setPagingDisabled();
        mPager.addOnPageChangeListener(this);
        mTabs.setViewPager(mPager);

        RestClient.getApi().getDashboardReservationExpertCount(
                Constants.TEST_SHOP_ID,
                new DateTime().toString("yyyy-MM-dd"),
                new DateTime().toString("yyyy-MM-dd"),
                ServerAPI.TEST_AUTH_TOKEN).enqueue
                (new APIHandler<List<ModelDashboardExpert>>(getActivity()) {
            @Override
            public void onSuccess(List<ModelDashboardExpert> dataObject) {
                adapter.experts = dataObject;
                adapter.notifyDataSetChanged();
                onExpertsLoaded();
            }
            @Override
            public void onFailureOrLogicError(ModelError error) {

            }
        });
        return rootView;
    }

    private void onExpertsLoaded(){

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        ReservePagerAdapter.ReserveHolder holder = (ReservePagerAdapter.ReserveHolder) ((ReservePagerAdapter)mPager.getAdapter()).getCurrentView().getTag();
        if( holder != null) {
            Calendar calendar = Calendar.getInstance();
            holder.weekView.goToHour(calendar.get(Calendar.HOUR_OF_DAY));
            holder.weekView.getScrollListener().onFirstVisibleDayChanged(calendar, null);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    public class ReservePagerAdapter extends PagerAdapter
            implements PagerSlidingTabStrip.CustomTabProvider,
            WeekView.EventClickListener,
            WeekView.EventLongPressListener,
            WeekView.EmptyViewLongPressListener,
            WeekView.ScrollListener{
        private List<ModelDashboardExpert> experts;


        class ReserveHolder {
            TextView tvBeforeDate;
            TextView tvBeforeDay;

            TextView tvAfterDate;
            TextView tvAfterDay;

            ViewGroup viewIndicator;
            WeekView weekView;
            Button btnMonth;
            WeekViewDataDelegate weekDelegate;
        }

        ReserveFragment mContext;

        public void setExperts(List<ModelDashboardExpert> experts) {
            this.experts = experts;
        }

        public ReservePagerAdapter(ReserveFragment context){
            mContext = context;
            experts = new ArrayList<>();
        }
        @Override
        public int getCount() {
            return experts.size();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            View view = LayoutInflater.from(getActivity()).inflate(R.layout.view_reserve, null);
            // Get a reference for the week view in the layout.
            final ReserveHolder holder = new ReserveHolder();
            holder.weekView = (WeekView) view.findViewById(R.id.reserve_weekView);
            holder.tvBeforeDate = (TextView)view.findViewById(R.id.reserve_day_before_tvDate);
            holder.tvBeforeDay = (TextView)view.findViewById(R.id.reserve_day_before_tvDay);
            holder.tvAfterDate = (TextView)view.findViewById(R.id.reserve_day_after_tvDate);
            holder.tvAfterDay = (TextView)view.findViewById(R.id.reserve_day_after_tvDay);
            holder.viewIndicator = (ViewGroup)view.findViewById(R.id.reserve_day_indicator);
            holder.btnMonth = (Button)view.findViewById(R.id.reserve_btnMonth);

            holder.btnMonth.setText(new DateTime().toString("yyyy년 MM월 ▼"));
            ModelDashboardExpert expert = ((ReserveFragment.ReservePagerAdapter)mPager.getAdapter()).experts.get(mPager.getCurrentItem());

            holder.weekDelegate = new WeekViewDataDelegate(getActivity(), holder.weekView, experts.get(position));

            holder.weekView.setOnEventClickListener(this);
            holder.weekView.setMonthChangeListener(holder.weekDelegate);
            holder.weekView.setEventLongPressListener(this);
            holder.weekView.setEmptyViewLongPressListener(this);
            holder.weekView.setScrollListener(this);
            holder.weekView.setNumberOfVisibleDays(1);

            holder.weekView.setDateTimeInterpreter(holder.weekDelegate);
            holder.weekView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if( holder.weekView.getNumberOfVisibleDays() == 1){
                        switch(event.getAction()){
                            case MotionEvent.ACTION_DOWN:
                            case MotionEvent.ACTION_MOVE:
                                holder.viewIndicator.setVisibility(View.GONE);
                                break;
                            case MotionEvent.ACTION_UP:
                                holder.viewIndicator.setVisibility(View.VISIBLE);
                                break;
                        }
                    }else {
                        holder.viewIndicator.setVisibility(View.GONE);
                    }
                    return false;
                }
            });
            view.setTag(holder);
            ((ViewPager)container).addView(view, 0);

            Calendar calendar = Calendar.getInstance();
            holder.weekView.goToHour(calendar.get(Calendar.HOUR_OF_DAY));
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager)container).removeView((View)object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public View getCustomTabView(ViewGroup parent, int position) {
            ModelDashboardExpert expert = experts.get(position);
            TextView tab = (TextView)LayoutInflater.from(getActivity()).inflate(R.layout.view_reserve_header_experts, parent, false);
            Spannable word = new SpannableString(expert.getNickname());
            word.setSpan(new ForegroundColorSpan(Color.WHITE), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            ((TextView)tab).setText(word);

            Spannable wordTwo = new SpannableString("" + expert.getReservation_count());
            wordTwo.setSpan(new ForegroundColorSpan(Color.RED), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            ((TextView)tab).append(" ");
            ((TextView)tab).append(wordTwo);
            return tab;
        }

        @Override
        public void tabSelected(View tab) {
            String text = (String)((TextView) tab).getText().toString();
            Spannable word = new SpannableString(text.split(" ")[0]);
            word.setSpan(new ForegroundColorSpan(Color.WHITE), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            ((TextView)tab).setText(word);

            Spannable wordTwo = new SpannableString(text.split(" ")[1]);
            wordTwo.setSpan(new ForegroundColorSpan(Color.RED), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            ((TextView)tab).append(" ");
            ((TextView)tab).append(wordTwo);

            ReserveHolder holder = (ReserveHolder) ((ReservePagerAdapter)mPager.getAdapter()).getCurrentView().getTag();

            DateTime nowTime = new DateTime();
            holder.weekDelegate.loadSchedule(nowTime.getYear(), nowTime.getMonthOfYear());

            DateTime beforeMonth = nowTime.minusMonths(1);
            DateTime afterMonth = nowTime.plusMonths(1);

            holder.weekDelegate.loadSchedule(beforeMonth.getYear(), beforeMonth.getMonthOfYear());
            holder.weekDelegate.loadSchedule(afterMonth.getYear(), afterMonth.getMonthOfYear());

        }

        @Override
        public void tabUnselected(View tab) {
            ((TextView)tab).setTextColor(Color.parseColor("#949494"));
            String text = (String)((TextView) tab).getText().toString();
            ((TextView)tab).setText(text);
        }


        @Override
        public void onEventClick(WeekViewEvent event, RectF eventRect) {
            Intent i = new Intent(getActivity(), CardActivity.class);
            i.putExtra(Constants.HELLO_EXTRA_ID, event.getId());
            i.putExtra(Constants.HELLO_EXTRA_GUEST_ID, event.getReserve().getGuest_id());

            if( event.getReserve().getGuest_id() == 0){
                //TODO 팝업 띄우기

            }else {
                startActivity(i);
            }
        }

        @Override
        public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
            //Toast.makeText(this, "Long pressed event: " + event.getName(), Toast.LENGTH_SHORT).show();
        }

        private View mCurrentView;

        public View getCurrentView() {
            return mCurrentView;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            mCurrentView = (View)object;
        }


        @Override
        public void onEmptyViewLongPress(Calendar time) {
            //Toast.makeText(this, "Empty view long pressed: " + getEventTitle(time), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onFirstVisibleDayChanged(Calendar newFirstVisibleDay, Calendar oldFirstVisibleDay) {
            ReserveHolder holder = (ReserveHolder) mCurrentView.getTag();
            Calendar newDay = (Calendar) newFirstVisibleDay.clone();
            if (holder.weekView.getNumberOfVisibleDays() == 1) {
                holder.viewIndicator.setVisibility(View.VISIBLE);
                newDay.add(Calendar.DATE, -1);
                String[] beforeStrings = holder.weekView.getDateTimeInterpreter().interpretDate(newDay, true).split("\n");
                newDay.add(Calendar.DATE, +2);
                String[] afterStrings = holder.weekView.getDateTimeInterpreter().interpretDate(newDay, true).split("\n");

                holder.tvBeforeDate.setText(beforeStrings[0]);
                holder.tvBeforeDay.setText(beforeStrings[1]);

                holder.tvAfterDate.setText(afterStrings[0]);
                holder.tvAfterDay.setText(afterStrings[1]);
            } else {
                holder.viewIndicator.setVisibility(View.GONE);
            }

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy년 MM월 ▼");
            holder.btnMonth.setText(dateFormat.format(newFirstVisibleDay.getTime()));
        }
    }


}
