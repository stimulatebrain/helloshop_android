package com.agcd.helloshop.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.agcd.helloshop.CalendarActivity;
import com.agcd.helloshop.CardActivity;
import com.agcd.helloshop.R;
import com.agcd.helloshop.model.ModelDashboard;
import com.agcd.helloshop.model.ModelDashboardExpert;
import com.agcd.helloshop.model.ModelNotification;
import com.agcd.helloshop.model.ModelNotificationWrapper;
import com.agcd.helloshop.model.ModelReserve;
import com.agcd.helloshop.rest.APIHandler;
import com.agcd.helloshop.rest.ModelError;
import com.agcd.helloshop.rest.RestClient;
import com.agcd.helloshop.rest.ServerAPI;
import com.agcd.helloshop.util.Constants;
import com.agcd.helloshop.util.HelloshopUtility;
import com.agcd.helloshop.util.Utility;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import at.grabner.circleprogress.CircleProgressView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by jspark on 2017. 1. 20..
 */

public class DashboardFragment extends Fragment {
    @BindView(R.id.dashboard_lv) StickyListHeadersListView mListView;
    @BindView(R.id.dashboard_hide_wrapper) RelativeLayout mFloatingView;

    @BindView(R.id.dashboard_floating_countGroup) ViewGroup mFloatingCountGroup;
    @BindView(R.id.dashboard_floating_tvGroup) ViewGroup mFloatingTvGroup;

    @BindView(R.id.dashboard_floating_tvDate) TextView mFloatingTvDate;
    @BindView(R.id.dashboard_floating_tvToday) TextView mFloatingTvToday;
    @BindView(R.id.dashboard_hide_count) TextView mFloatingTvCount;
    @BindView(R.id.dashboard_expert_wrapper) ViewGroup mExpertWrapper;

    TextView mHeaderTvDate;
    TextView mHeaderTvToday;
    TextView mHeaderTvName;
    TextView mHeaderTvAllCount;
    TextView mHeaderTvCompleteCount;
    CircleProgressView mHeaderCircleView;


    private ReserveAdapter mAdapter;
    private View mHeaderView;
    private ViewPager mPager;
    private int mLastHeaderTop = -1;
    private int mLastScrollY = 0;

    private boolean mIsScrollingUp = false;
    private Activity mActivityContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, rootView);
        mActivityContext = getActivity();
        mHeaderView = getActivity().getLayoutInflater().inflate(R.layout.view_dashboard_header, (ViewGroup)mListView.getChildAt(0), false);
        mHeaderTvDate = (TextView) mHeaderView.findViewById(R.id.dashboard_tvDate);
        mHeaderTvToday = (TextView) mHeaderView.findViewById(R.id.dashboard_tvToday);
        mHeaderCircleView = (CircleProgressView)mHeaderView.findViewById(R.id.dashboard_circleView);
        mHeaderTvName = (TextView) mHeaderView.findViewById(R.id.dashboard_circle_tvName);
        mHeaderTvAllCount = (TextView) mHeaderView.findViewById(R.id.dashboard_circle_tvAllCount);
        mHeaderTvCompleteCount = (TextView) mHeaderView.findViewById(R.id.dashboard_circle_tvCompleteCount);


        mPager = (ViewPager) mHeaderView.findViewById(R.id.dashboard_viewPager);
        mListView.addHeaderView(mHeaderView, null, false);

        mFloatingView.bringToFront();

        mHeaderTvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), CalendarActivity.class);
                startActivity(i);
            }
        });
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                onScrollWithChangingPosition();
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // StickyHeader라 Header까지 포함되서 position이 옴 그래서 id로 변경.
                ModelNotification notification = mAdapter.reserveList.get((int) id);
                Intent i = new Intent(getActivity(), CardActivity.class);
                i.putExtra(Constants.HELLO_EXTRA_ID, (long)notification.getId());
                i.putExtra(Constants.HELLO_EXTRA_GUEST_ID, notification.getGuest_id());
                startActivity(i);
            }
        });


        setHeaderView(new DateTime());
        this.setReserveListview();
        this.setReservePager();

        onScrollWithChangingPosition();

        return rootView;
    }

    private void setHeaderView(DateTime time){
        mHeaderTvDate.setText(time.toString("yyyy년 MM월 dd일(E)", Locale.KOREA));
        mFloatingTvDate.setText(time.toString("yyyy년 MM월 dd일(E)", Locale.KOREA));

        if( time.toString("yyyy-MM-dd").equals(new DateTime().toString("yyyy-MM-dd")) ){
            mHeaderTvToday.setVisibility(View.VISIBLE);
            mFloatingTvToday.setVisibility(View.VISIBLE);
        }else {
            mHeaderTvToday.setVisibility(View.GONE);
            mFloatingTvToday.setVisibility(View.GONE);
        }
        RestClient.getApi().getDashboardReservationCount(
                            Constants.TEST_SHOP_ID,
                            time.toString("yyyy-MM-dd"),
                            time.toString("yyyy-MM-dd"),
                            ServerAPI.TEST_AUTH_TOKEN)
                .enqueue(new APIHandler<ModelDashboard>(getActivity()) {
            @Override
            public void onSuccess(ModelDashboard dataObject) {
                ModelDashboard result = dataObject;
                mHeaderCircleView.setMaxValue(result.getReservation_count());
                mHeaderCircleView.setValue(result.getFinish_count());
                mHeaderTvAllCount.setText(result.getReservation_count() + "");
                mHeaderTvCompleteCount.setText("완료 "+ result.getFinish_count());
                mHeaderTvName.setText(result.getName());
                mFloatingTvCount.setText(result.getReservation_count() + "");
            }

            @Override
            public void onFailureOrLogicError(ModelError error) {

            }
        });

        RestClient.getApi().getDashboardReservationExpertCount(Constants.TEST_SHOP_ID,
                time.toString("yyyy-MM-dd"), time.toString("yyyy-MM-dd"),
                ServerAPI.TEST_AUTH_TOKEN)
                .enqueue(new APIHandler<List<ModelDashboardExpert>>(mActivityContext) {

            @Override
            public void onSuccess(List<ModelDashboardExpert> dataObject) {
                ViewGroup viewExpert;
                for(ModelDashboardExpert model : dataObject){
                    viewExpert = (ViewGroup) mActivityContext.getLayoutInflater().inflate(R.layout.view_dashboard_experts, null);
                    mExpertWrapper.addView(viewExpert);
                    TextView tvCount = (TextView) ((ViewGroup)viewExpert.getChildAt(0)).getChildAt(0);
                    TextView tvName = (TextView) ((ViewGroup)viewExpert.getChildAt(0)).getChildAt(1);
                    tvCount.setText(model.getReservation_count() + "");
                    tvName.setText(model.getNickname());
                }
            }
            @Override
            public void onFailureOrLogicError(ModelError error) {

            }
        });


    }

    @OnClick(R.id.dashboard_floating_tvDate)
    public void onClickTextDate(View v){
        Intent i = new Intent(getActivity(), CalendarActivity.class);
        startActivity(i);
    }

    private void onScrollWithChangingPosition(){
        int headerTop = mHeaderView.getTop();
        if (headerTop > mLastHeaderTop){
            mIsScrollingUp = true;
        }else {
            mIsScrollingUp = false;
        }
        mLastHeaderTop = headerTop;

        // getHeight - 98dp 뺀 대 까지만 바꿈.
        int maxTop = Utility.dpToPx(320) -  Utility.dpToPx(98);
        if( -headerTop < maxTop){
            setFloatingViewFromScroll(-(float)headerTop / (float)maxTop);
        }else {
            setFloatingViewFromScroll(1.0f);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //mListView.setSelection(0);
        //onScrollWithChangingPosition();
    }


    private void setFloatingViewFromScroll(float percentage){
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mFloatingView.getLayoutParams();
        params.height = Utility.dpToPx(320) - (int)(Utility.dpToPx(222) * percentage);
        mFloatingView.setLayoutParams(params);

        // mFloatingCountGroup
        if( percentage > 0.8){
            mFloatingTvGroup.setAlpha((percentage - 0.8f) * 5.0f);
            mFloatingCountGroup.setAlpha((percentage - 0.8f) * 5.0f);
        }else {
            mFloatingTvGroup.setAlpha(0.0f);
            mFloatingCountGroup.setAlpha(0.0f);
        }

        if( percentage > 0.8){
            mFloatingView.setBackgroundColor(Color.BLACK);
        }else {
            mFloatingView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private int getScrollY(int firstVisibleItem){

        int scrollY = 0;
        for (int i =0; i < firstVisibleItem; i++){
            scrollY += Utility.dpToPx(86);

        }
        scrollY -= ((ViewGroup)mListView.getChildAt(0)).getChildAt(0).getTop();
        return scrollY;
    }

    private void setReservePager(){
        List<ModelReserve> reserves = new ArrayList<>();
//        reserves.add(new ModelReserve("1", "아토헤어 패키지 남성염색", "09:00~10:00", "ParkJi", "김효섭"));
//        reserves.add(new ModelReserve("2", "아토헤어 패키지 남성염색2", "09:00~11:00", "ParkJi", "김효섭"));
//        reserves.add(new ModelReserve("3", "아토헤어 패키지 남성염색3", "09:00~12:00", "ParkJi", "김효섭"));

        mPager.setAdapter(new PagerAdapter(getActivity().getLayoutInflater(), reserves));
    }

    private void setReserveListview(){
        mAdapter = new ReserveAdapter(getActivity());
        mListView.setAdapter(mAdapter);

        RestClient.getApi().loadNotificationsFromDashboard(
                Constants.TEST_SHOP_ID,
                ServerAPI.TEST_AUTH_TOKEN).enqueue(new APIHandler<List<ModelNotification>>(getActivity()) {

            @Override
            public void onSuccess(List<ModelNotification> dataObject) {
                mAdapter.reserveList = (ArrayList<ModelNotification>) dataObject;
                mAdapter.notifyDataSetChanged();
            }
            @Override
            public void onFailureOrLogicError(ModelError error) {

            }
        });
    }


    public class PagerAdapter extends android.support.v4.view.PagerAdapter {
        Context context;
        private List<ModelReserve> mReserves;

        LayoutInflater inflater;

        public PagerAdapter(LayoutInflater inflater, List<ModelReserve> reserves) {
            this.mReserves = reserves;
            this.inflater=inflater;
        }

        @Override
        public int getCount() {
            return mReserves.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view==object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = inflater.inflate(R.layout.cell_dashboard_reserve, null);
            //ViewPager에 만들어 낸 View 추가
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager)container).removeView((View) object);
        }
    }


    public class ReserveAdapter extends BaseAdapter implements StickyListHeadersAdapter {

        private ArrayList<ModelNotification> reserveList;
        private LayoutInflater inflater;
        private boolean isShowCheckBtn = false;

        public void setReserveList(ArrayList<ModelNotification> reserveList) {
            this.reserveList = reserveList;
        }

        public ReserveAdapter(Context context) {
            inflater = LayoutInflater.from(context);
            reserveList = new ArrayList<>();
        }

        @Override
        public int getCount() {
            return reserveList.size();
        }

        @Override
        public Object getItem(int position) {
            return reserveList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ReserveAdapter.ViewHolder holder;
            ModelNotification model = reserveList.get(position);
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.cell_dashboard_notification, parent, false);
                holder.tvDate = (TextView) convertView.findViewById(R.id.cell_notification_tvDate);
                holder.tvDateFrNow = (TextView) convertView.findViewById(R.id.cell_notification_tvDateFrNow);
                holder.tvCustomerName = (TextView) convertView.findViewById(R.id.cell_notification_tvCustomerName);
                holder.ivCustomerGrade = (ImageView) convertView.findViewById(R.id.cell_notification_ivCustomerGrade);
                holder.tvName = (TextView) convertView.findViewById(R.id.cell_notification_tvName);
                holder.tvStatus = (TextView) convertView.findViewById(R.id.cell_notification_tvStatus);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder)convertView.getTag();
            }
            holder.tvDate.setText(
                    DateTime.parse(model.getReservation_dt(), DateTimeFormat.forPattern("yyyy-MM-dd")).toString("MM월 dd일")
                            + " "
                            + model.getStart_time() + " ~ " +model.getEnd_time());
            holder.tvStatus.setText(HelloshopUtility.getReserveStatusDesc(model.getStatus()));
            if( model.getCreated_at() != null){
                DateTime beforeTime = DateTime.parse(model.getCreated_at(), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS"));
                Interval interval = new Interval(beforeTime, new DateTime());
                holder.tvDateFrNow.setText(Utility.getStandardIntervalText(interval.toDuration().getStandardSeconds()));

            }else {
                holder.tvDateFrNow.setVisibility(View.GONE);
            }
            if( model.getService_name() != null){
                holder.tvName.setText(model.getService_name());
            }else {
                holder.tvName.setText("");
            }
            if( model.getGuest_name() != null){
                holder.tvCustomerName.setText(model.getGuest_name());
                holder.ivCustomerGrade.setVisibility(View.VISIBLE);
            }else {
                holder.tvCustomerName.setText("");
                holder.ivCustomerGrade.setVisibility(View.GONE);
            }
            //holder.tvDateFrNow.setText();
            return convertView;
        }

        @Override
        public View getHeaderView(int position, View convertView, ViewGroup parent) {
            ReserveAdapter.HeaderViewHolder holder;
            if (convertView == null) {
                holder = new ReserveAdapter.HeaderViewHolder();
                convertView = inflater.inflate(R.layout.cell_dashboard_notification_header, parent, false);
                holder.tvTitle = (TextView) convertView.findViewById(R.id.notificationcell_tv_title);
                holder.tvCount = (TextView) convertView.findViewById(R.id.notificationcell_tv_count);
                convertView.setTag(holder);
            } else {
                holder = (HeaderViewHolder) convertView.getTag();
            }
            holder.tvCount.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
            holder.tvCount.setText("전체 " + reserveList.size() + "건");
            return convertView;
        }

        @Override
        public long getHeaderId(int position) {
            //return the first character of the country as ID because this is what headers are based upon
            return 0;
        }

        class HeaderViewHolder {
            TextView tvTitle;
            TextView tvCount;
        }

        class ViewHolder {
            TextView tvStatus;
            TextView tvDateFrNow;
            TextView tvName;
            ImageView ivCustomerGrade;
            TextView tvCustomerName;
            TextView tvDate;
        }

    }


}
