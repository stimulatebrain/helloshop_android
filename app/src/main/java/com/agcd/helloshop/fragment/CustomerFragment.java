package com.agcd.helloshop.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.agcd.helloshop.CustomerCardActivity;
import com.agcd.helloshop.CustomerCardCreateActivity;
import com.agcd.helloshop.CustomerMessageActivity;
import com.agcd.helloshop.MainTabActivity;
import com.agcd.helloshop.R;
import com.agcd.helloshop.adapter.CustomerListAdapter;
import com.agcd.helloshop.model.ModelDashboard;
import com.agcd.helloshop.model.ModelGuest;
import com.agcd.helloshop.popup.PopupBottomDialog;
import com.agcd.helloshop.popup.SelectedCustomerBar;
import com.agcd.helloshop.rest.APIHandler;
import com.agcd.helloshop.rest.ModelError;
import com.agcd.helloshop.rest.RestClient;
import com.agcd.helloshop.rest.ServerAPI;
import com.agcd.helloshop.util.Constants;
import com.agcd.helloshop.util.LocalData;
import com.agcd.helloshop.util.OnPopupStringResultListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by jspark on 2017. 1. 20..
 */

public class CustomerFragment extends Fragment {

    Context mContext;

    @BindView(R.id.frgCustomer_tvBtnSelect) TextView mBtnSelect;
    @BindView(R.id.customer_btn_filter) View mBtnFilter;
    @BindView(R.id.customer_list) StickyListHeadersListView mListView;
//    @BindView(R.id.customer_view_selected) View mBottomSelectedView;
//    @BindView(R.id.customer_tv_selected_people) TextView mTvSelected;

    @BindView(R.id.frgCustomer_rlBarRegister)
    ViewGroup mRlBarRegister;

    @BindView(R.id.frgCustomer_selectedCustomerBar)
    SelectedCustomerBar mCustomerSelectedBar;

    private CustomerListAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_customer, container, false);
        ButterKnife.bind(this, rootView);

        mContext = getActivity();

        //mCustomerSelectedBar.setOnClickAction();

        mAdapter = new CustomerListAdapter(mContext);
//        ArrayList<ModelGuest> list = new ArrayList<>();
//        list.add(new ModelGuest("김효섭", "VIP", "010-8204-4551"));
//        list.add(new ModelGuest("김효섭", "VIP", "010-8204-4551"));
//        list.add(new ModelGuest("김효섭", "VIP", "010-8204-4551"));
//        list.add(new ModelGuest("김효섭", "VIP", "010-8204-4551"));
//        list.add(new ModelGuest("김효섭", "VIP", "010-8204-4551"));
//        list.add(new ModelGuest("김효섭", "VIP", "010-8204-4551"));
//        list.add(new ModelGuest("김효섭", "VIP", "010-8204-4551"));
//        list.add(new ModelGuest("박종성", "Normal", "010-8204-4551"));
//        list.add(new ModelGuest("박종성", "Normal", "010-8204-4551"));
//        list.add(new ModelGuest("박종성", "Normal", "010-8204-4551"));
//        mAdapter.setCustomerList(list);

        loadCustomerList();

        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mAdapter.isSelectMode()) {
                    mAdapter.clickCheck(position);
                } else {
                    Intent i = new Intent(getActivity(), CustomerCardActivity.class);
                    startActivity(i);
                }
            }
        });

        mBtnSelect.setPaintFlags(mBtnSelect.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        return rootView;
    }

    @OnClick(R.id.frgCustomer_tvBtnSelect)
    public void onClickSelect(View view) {
        if (mAdapter.isSelectMode()==true) {
            mAdapter.setSelectModeOff();

            mBtnSelect.setText("선택");
            //mBottomSelectedView.setVisibility(View.GONE);
//            mCustomerSelectedBar.setVisibility(View.GONE);
        } else {
            mAdapter.setSelectModeOn(new CustomerListAdapter.OnChangeSelectedListener() {
                @Override
                public void onChange(List<ModelGuest> selectedList) {
                    if (selectedList.size()>0) {
                        mCustomerSelectedBar.setVisibility(View.VISIBLE);
                        mCustomerSelectedBar.setSelectedInfoTitle(selectedList);
                    } else {
                        mCustomerSelectedBar.setVisibility(View.GONE);
                    }
                }
            });

            mBtnSelect.setText("취소");
            //mBottomSelectedView.setVisibility(View.VISIBLE);
//            mCustomerSelectedBar.setVisibility(View.VISIBLE);
        }
        ((MainTabActivity)getActivity()).setToggleBottomView();
    }

    @OnClick(R.id.customer_btn_filter)
    public void onClickFilter(View view){
        ArrayList<String> list = new ArrayList<>();
        list.add("전체");
        list.add("VIP");
        list.add("Normal");
        PopupBottomDialog dialog = new PopupBottomDialog(getActivity(), list, new OnPopupStringResultListener() {
            @Override
            public void onComplete(String result) {

            }
        });
        dialog.show();
    }


    @OnClick(R.id.frgCustomer_rlBarRegister)
    public void onClickAdd(View view){
        Intent i = new Intent(getActivity(), CustomerCardCreateActivity.class);
        startActivity(i);
    }

//
//    @OnClick(R.id.customer_btn_forward)
//    public void onClickForward(View view){
//        ArrayList<String> list = new ArrayList<>();
//        list.add("쪽지 전송");
//        list.add("문자메시지 전송");
//        list.add("등급변경");
//        list.add("내보내기");
//        list.add("고객카드삭제");
//        PopupBottomDialog dialog = new PopupBottomDialog(getActivity(), list, new OnPopupStringResultListener() {
//            @Override
//            public void onComplete(String result) {
//                if( result.equals("쪽지 전송")){
//                    Intent i = new Intent(getActivity(), CustomerMessageActivity.class);
//                    startActivity(i);
//                }else if(result.equals("문자메시지 전송")){
//
//                }
//            }
//        });
//        dialog.show();
//    }


    public void loadCustomerList() {
        int shopId = LocalData.getShopId(mContext);
        String customerGradeParamValue = ModelGuest.Grade.ALL.getServerParamValue();

        RestClient.getApi().loadGuestList(shopId, customerGradeParamValue,
                LocalData.getTokenKey(mContext)).enqueue(new APIHandler<List<ModelGuest>>(mContext) {
            @Override
            public void onSuccess(List<ModelGuest> list) {
                mAdapter.setCustomerList(list);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailureOrLogicError(ModelError error) {}
        });
    }

}
