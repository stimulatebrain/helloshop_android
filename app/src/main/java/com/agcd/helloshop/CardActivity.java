package com.agcd.helloshop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.agcd.helloshop.adapter.CardReserveAdapter;
import com.agcd.helloshop.fragment.DashboardFragment;
import com.agcd.helloshop.model.ModelDashboardExpert;
import com.agcd.helloshop.model.ModelMemo;
import com.agcd.helloshop.model.ModelParent;
import com.agcd.helloshop.model.ModelReserve;
import com.agcd.helloshop.rest.APIHandler;
import com.agcd.helloshop.rest.ModelError;
import com.agcd.helloshop.rest.RestClient;
import com.agcd.helloshop.rest.ServerAPI;
import com.agcd.helloshop.util.Constants;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by jspark on 2017. 5. 25..
 */

public class CardActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {

    @BindView(R.id.card_customerLv) StickyListHeadersListView mListView;

    @BindView(R.id.card_tvDate) TextView tvDate;
    @BindView(R.id.card_tvName) TextView tvName;
    @BindView(R.id.card_tvTime) TextView tvTime;
    @BindView(R.id.card_tvServiceTime) TextView tvServiceTime;
    @BindView(R.id.card_tvStaffName) TextView tvStaffName;
    @BindView(R.id.card_tvPrice) TextView tvPrice;


    private TextView tvGuestName;
    private ImageView ivGuestGender;
    private TextView tvGuestGrade;
    private TextView tvGuestPhone;
    private ImageView ivGuestPhoto;

    private ModelReserve mReserve;

    private CardReserveAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);
        ButterKnife.bind(this);
        setReserveListview();

        long scheduleId = getIntent().getLongExtra(Constants.HELLO_EXTRA_ID, 0);
        int guestId = getIntent().getIntExtra(Constants.HELLO_EXTRA_GUEST_ID, 0);

        RestClient.getApi().getSchedule(
                Constants.TEST_SHOP_ID,
                (int) scheduleId,
                "staff,guest,service",
                ServerAPI.TEST_AUTH_TOKEN).enqueue
                (new APIHandler<ModelReserve>(this) {
                    @Override
                    public void onSuccess(ModelReserve dataObject) {
                        mReserve = dataObject;
                        tvTime.setText(mReserve.getStart_time() + " ~ " + mReserve.getEnd_time());
                        tvDate.setText(
                                DateTime.parse(mReserve.getReservation_dt(), DateTimeFormat.forPattern("yyyy-MM-dd"))
                                    .toString("MM월 dd일(E)", Locale.KOREA));
                        tvName.setText(mReserve.getService_name());
                        tvPrice.setText(mReserve.getService().getAmount() + "");
                        tvStaffName.setText(mReserve.getStaff().getNickname());


                        PeriodFormatter dhm = new PeriodFormatterBuilder()
                                .appendHours()
                                .appendSuffix("시간", "시간")
                                .appendSeparator(" ")
                                .appendMinutes()
                                .appendSuffix("분", "분")
                                .toFormatter();
                        tvServiceTime.setText(dhm.print(new Period(Minutes.minutes(mReserve.getService().getTime())).normalizedStandard()));

                        tvGuestName.setText(mReserve.getGuest().guest_name);
                        tvGuestPhone.setText(mReserve.getGuest().guest_mobile);
                        Picasso.with(CardActivity.this).load(mReserve.getGuest().getThumbnail()).into(ivGuestPhoto);
                    }
                    @Override
                    public void onFailureOrLogicError(ModelError error) {

                    }
                });

        RestClient.getApi().loadGuestReserveList(
                Constants.TEST_SHOP_ID,
                guestId,
                "service",
                ServerAPI.TEST_AUTH_TOKEN).enqueue(
                new APIHandler<List<ModelReserve>>(this) {
                    @Override
                    public void onSuccess(List<ModelReserve> dataObject) {
                        mAdapter.setReserveList((ArrayList<ModelReserve>) dataObject);
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailureOrLogicError(ModelError error) {

                    }
                }
        );


    }

    @OnClick(R.id.card_btnClose)
    public void closeCard(){
        this.finish();
    }

    private void setReserveListview(){
        View headerView = this.getLayoutInflater().inflate(R.layout.view_card_customer_header, (ViewGroup) mListView.getChildAt(0), false);
        headerView.findViewById(R.id.card_customerWrapper).setOnClickListener(this);

        tvGuestGrade = (TextView) headerView.findViewById(R.id.card_customer_tvGrade);
        tvGuestName = (TextView) headerView.findViewById(R.id.card_customer_tvName);
        tvGuestPhone = (TextView) headerView.findViewById(R.id.card_customer_tvPhone);
        ivGuestGender = (ImageView) headerView.findViewById(R.id.card_customer_ivGender);
        ivGuestPhoto = (ImageView) headerView.findViewById(R.id.card_customer_ivPhoto);


        mListView.addHeaderView(headerView);
        mAdapter = new CardReserveAdapter(this);

        ArrayList<ModelMemo> listMemo = new ArrayList<>();
        listMemo.add(new ModelMemo("1"));
        mAdapter.setMemoList(listMemo);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mAdapter.currentViewType == ModelParent.Type.TYPE_RESERVE){
            Intent i = new Intent(this, CustomerCardReserveActivity.class);
            startActivity(i);
        }else {
            Intent i = new Intent(this, CustomerCardMemoActivity.class);
            startActivity(i);
        }
    }

    @OnClick(R.id.card_reservell)
    public void clickCard(View view){
        Intent i = new Intent(this, CardModifyActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.card_btnModify)
    public void clickModify(View view){
        Intent i = new Intent(this, CardModifyActivity.class);
        startActivity(i);
    }


    // 카드 윗부분 메소드.
    @Override
    public void onClick(View v) {
//        Intent i = new Intent(this, CardModifyActivity.class);
//        startActivity(i);
    }
}
