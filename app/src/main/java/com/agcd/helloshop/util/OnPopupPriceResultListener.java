package com.agcd.helloshop.util;

import java.util.Date;

/**
 * Created by jspark on 2017. 5. 30..
 */

public interface OnPopupPriceResultListener {
    void onComplete(int newPrice);
}
