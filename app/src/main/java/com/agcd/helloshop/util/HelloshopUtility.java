package com.agcd.helloshop.util;

/**
 * Created by mac1 on 15. 6. 8..
 */
public class HelloshopUtility {

    public static String getReserveStatusDesc(String status){
        switch (status){
            case "00":
                return "시술완료";
            case "01":
                return "예약생성";
            case "02":
                return "예약요청";
            case "03":
                return "예약완료";
            case "04":
                return "예약변경";
            case "05":
                return "오프타임";
            case "06":
                return "임시예약";
            case "88":
                return "예약삭제";
            case "99":
                return "예약취소";
        }
        return "예약코드 에러";
    }


}
