package com.agcd.helloshop.util;

/**
 * Created by jspark on 2017. 5. 30..
 */

public interface OnPopupStringResultListener {
    void onComplete(String result);
}
