package com.agcd.helloshop.util;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.View;

/**
 * Created by jspark on 2017. 6. 2..
 */

public class AnimationUtility {

    public static void executeColorAnimFromDrawable(final View v, int beforeColor, int afterColor, int duration){
        ValueAnimator colorAnim = new ValueAnimator();
        colorAnim.setIntValues(beforeColor, afterColor);
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                GradientDrawable bgShape = (GradientDrawable)v.getBackground();
                bgShape.setColor((Integer)valueAnimator.getAnimatedValue());
            }
        });
        colorAnim.setDuration(duration);
        colorAnim.start();
    }

}
