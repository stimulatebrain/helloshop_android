package com.agcd.helloshop.util;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;


import com.agcd.helloshop.R;

import java.util.HashMap;

/**
 * 프래그먼트를 생성하고 관리하는 매니저 클래스이다.
 */
public class JFragmentManager {
    /**
     * 프래그먼트 해시맵
     */
    private HashMap<String, Fragment> fragmentMap;

    /**
     * 생성자
     * 프래그먼트를 생성한다.
     */
    public JFragmentManager() {
        // 프래그먼트를 저장할 해시맵을 생성한다.
        fragmentMap = new HashMap<>();
    }

    public JFragmentManager(HashMap<String, Fragment> fragmentMap){
        this.fragmentMap = fragmentMap;
    }

    /**
     * 프래그먼트를 이름을 사용해서 찾는다.
     *
     * @param fragmentName 프래그먼트 이름
     * @return 프래그먼트
     */
    public Fragment getFragment(String fragmentName) {
        return fragmentMap.get(fragmentName);
    }

    /**
     * 화면에 프래그먼트를 표시한다.
     *
     * @param activity     액티비티
     * @param fragmentName 표시할 프래그먼트
     * @return 표시된 프래그먼트
     */
    public Fragment changeFragment(Activity activity, String fragmentName) {
        Fragment fragment = getFragment(fragmentName);

        // 해당 프래그먼트가 없으면 리턴한다.
        if (fragment == null) {
            return null;
        }
        if (!fragment.isAdded()) {
            fragment.setArguments(null);
        }

        // 화면에 프래그먼트를 표시한다.
        android.app.FragmentManager fragmentManager = activity.getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.maintab_fragmentArea, fragment);
        fragmentTransaction.commit();

        return fragment;
    }

    /**
     * 화면에 프래그먼트를 표시한다.
     *
     * @param activity     액티비티
     * @param fragmentName 표시할 프래그먼트
     * @param bundle       넘길 번들 데이터
     * @return 표시된 프래그먼트
     */
    public Fragment changeFragmentWithBundle(Activity activity, String fragmentName, Bundle bundle) {
        Fragment fragment = getFragment(fragmentName);

        // 해당 프래그먼트가 없으면 리턴한다.
        if (fragment == null) {
            return null;
        }
        if( fragment.getArguments() != null ){
            fragment.getArguments().clear();
            fragment.getArguments().putAll(bundle);
        }else {
            fragment.setArguments(bundle);
        }

        // 화면에 프래그먼트를 표시한다.
        android.app.FragmentManager fragmentManager = activity.getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.maintab_fragmentArea, fragment);
        fragmentTransaction.commit();

        return fragment;
    }

    /**
     * 웹뷰 프래그먼트를 표시한다.
     *
     * @param activity 액티비티
     * @param url      표시할 URL
     */
//    public void changeWebviewFragment(Activity activity, String url) {
//        // URL을 표시한다.
//        WebviewFragment webviewFragment = (WebviewFragment) getFragment("webview");
//        webviewFragment.refreshWebview(url);
//
//        // 화면에 프래그먼트를 표시한다.
//        JFragmentManager fragmentManager = activity.getFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.fragmentArea, webviewFragment);
//        fragmentTransaction.commit();
//    }

    /**
     * 웹뷰 프래그먼트를 표시한다.
     *
     * @param activity 액티비티
     * @param message  메시지
     */
//    public void changeWebviewFragment(Activity activity, MessageModel message) {
//        // URL을 표시한다.
//        WebviewFragment webviewFragment = (WebviewFragment) getFragment("webview");
//        webviewFragment.refreshWebview(message);
//
//        // 화면에 프래그먼트를 표시한다.
//        JFragmentManager fragmentManager = activity.getFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.fragmentArea, webviewFragment);
//        fragmentTransaction.commit();
//    }

    /**
     * 현재 보이고 있는 프래그먼트를 반환한다.
     *
     * @return 프래그먼트 또는 null
     */
    public Fragment getCurrentFragment() {
        for (String key : fragmentMap.keySet()) {
            if (fragmentMap.get(key).isVisible()) {
                return fragmentMap.get(key);
            }
        }

        return null;
    }
}
