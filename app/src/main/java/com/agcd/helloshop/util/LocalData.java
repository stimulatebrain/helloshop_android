package com.agcd.helloshop.util;

import android.content.Context;

import com.agcd.helloshop.model.ModelShop;


public class LocalData {


//    public static String KEY_MY_PROFILE = "myProfile";
//    static ModelUser myProfile;
//    public static ModelUser getMyProfile(Context context) {
//        if (myProfile==null) {
//            myProfile = (ModelUser) UXSharedPreferences.get(context, KEY_MY_PROFILE);
//        }
//        //ModelUser user = (ModelUser) UXSharedPreferences.get(context, KEY_MY_PROFILE);
//        return myProfile;
//    }
//    public static void setMyProfile(Context context, ModelUser user) {
//        UXSharedPreferences.put(context, KEY_MY_PROFILE, user);
//        myProfile = user;
//    }



    static ModelShop _shopModel;

    static int _shopId = -1;
    static String KEY_SHOP_ID= "keyShopId";
    public static void setShopId(Context context, int shopId) {
        UXSharedPreferences.putInt(context, KEY_SHOP_ID, shopId);
        _shopId = shopId;
    }
    public static int getShopId(Context context) {
        // temp - 임시
        return 14;
//        if (_shopId==-1) {
//            _shopId = UXSharedPreferences.getInt(context, KEY_SHOP_ID, -1);
//        }
//        return _shopId;
    }

    public static void setShopModel(ModelShop shopModel){
        _shopModel = shopModel;
    }

    public static ModelShop getShopModel() {
        return _shopModel;
    }

    static String _tokenKey;
    static String KEY_TOKEN_KEY = "keyTokenKey";
    public static void setTokenKey(Context context, String tokenKey) {
        UXSharedPreferences.putString(context, KEY_TOKEN_KEY, tokenKey);
        _tokenKey = tokenKey;
    }
    public static String getTokenKey(Context context) {
        // temp - 임시
        return "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjBjZDk4MDYwMTFlYzAxMGJkYjA2MzFkYzQxMDk2YzZiOWEzNzBlMGFmYTRhN2NjNGQ2ZmRjNTVjMjg3OTI3ZDI3MGEyM2UzMjEyMGYyOTNiIn0.eyJhdWQiOiIxIiwianRpIjoiMGNkOTgwNjAxMWVjMDEwYmRiMDYzMWRjNDEwOTZjNmI5YTM3MGUwYWZhNGE3Y2M0ZDZmZGM1NWMyODc5MjdkMjcwYTIzZTMyMTIwZjI5M2IiLCJpYXQiOjE0OTU2OTMyMzMsIm5iZiI6MTQ5NTY5MzIzMywiZXhwIjoxODExMjI2MDMzLCJzdWIiOiI0NjEiLCJzY29wZXMiOltdfQ.l4e44tOWuzIKh7bHxcCngxw1VyK-Xki0_lSNcxPmUS_uc7A49Z8WpC9_Rc4mWYliotBjiyI5qi5LbDkGHIEw2q2NiYzMR0HKSvNdCtmYJA4ep3rgXqvFpWVN1_b48kCee8RODvgQTPQfWEWLGnaHSLC8ZtbTPzxXjTAYTzCmV4_QCNXcBYv8wkL2FxnWe5fyTIfVsu1RrVYm6AwdBmpazfBbuNb7DkN1qq4ct1cSSg_Skk38NdYKiM8ZB_ikeEbSXuQnY3w6MZvTGe0vEhXm2Ayunh-fKbmyJY0ytHL2MMOMfwVbVrzowlEauBbvsDtok5InmReIbaSRJAvEoTZj4_7oTlc_NTyLLwk2Gg8dxaEmKrjhY3iL3nEHHQQMQPkZFFfadgkOvde3cWx5GWiuvST2UTlzoIKok8TYWEm6xXUSdeEsKM2ST_HojC1QsN4dOhJunoOib5cuyJc4e2Cz_eYlZujLUT38A1hd0FjASNf92wiEt4pJ4P1ZkISADe6gKGhstZJnnlexJhhESsDO8mv3VfWA-unJeTOL49EdDSosEd5ENefJ83npBpNnJjUqPHTaUEdc6HqRErOCerpkHdF8mcV-h3kt9j9gMgGdB-IKORDb1AzoORIOHTG70wzq6-SlyrXPVqcSCDSXOxM0oi7s5IkP6FjJXSZqDBIUkiw";
//        if (_tokenKey==null) {
//            _tokenKey = UXSharedPreferences.getString(context, KEY_TOKEN_KEY);
//            if (_tokenKey==null) {
//                _tokenKey = "";
//            }
//        }
//        return _tokenKey;
    }
//
//    static String _deviceToken;
//    static String KEY_DEVICE_TOKEN = "keyDeviceToken";
//    public static void setDeviceToken(Context context, String deviceToken) {
//        UXSharedPreferences.putString(context, KEY_DEVICE_TOKEN, deviceToken);
//        _deviceToken = deviceToken;
//    }
//    public static String getDeviceToken(Context context) {
//        if (_deviceToken==null) {
//            _deviceToken = UXSharedPreferences.getString(context, KEY_DEVICE_TOKEN);
//            if (_deviceToken==null) {
//                _deviceToken= "";
//            }
//        }
//        return _deviceToken;
//    }


//    static String KEY_GCM_ENABLED = "keyGcmEnabled";
//    public static void setPushEnabled(Context context, boolean enabled) {
//        UXSharedPreferences.putBoolean(context, KEY_GCM_ENABLED, enabled);
//    }
//    public static boolean getPushEnabled(Context context) {
//        boolean gcmEnabled = UXSharedPreferences.getBoolean(context, KEY_GCM_ENABLED, true);
//        return gcmEnabled;
//    }


}
