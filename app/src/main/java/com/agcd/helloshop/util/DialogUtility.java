package com.agcd.helloshop.util;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;

import com.agcd.helloshop.popup.PopupToastDialog;

/**
 * Created by jspark on 2017. 6. 4..
 */

public class DialogUtility  {
    public static void showToast(Context context, SpannableString message){
//        SpannableString word = new SpannableString("오프타임 생성이 완료되었습니다.");
//        word.setSpan(new StyleSpan(Typeface.BOLD), 0, 7, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        PopupToastDialog dialog = new PopupToastDialog(context, message);
        dialog.show();
    }
}
