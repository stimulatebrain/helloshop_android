package com.agcd.helloshop.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;


public class UXSharedPreferences {

    private static String CLASS_SEPARATOR = "~!@#$%^&*()";
    private static String _convertToString(String jsonString, Class aClass) {
        String className = aClass.getName();
        return className+CLASS_SEPARATOR+jsonString;
    }
    private static String _convertToString(String jsonString, Class aClass, Class componentClass) {
        String className = aClass.getName() + "<" + componentClass.getName() + ">";;
        return className+CLASS_SEPARATOR+jsonString;
    }
    private static String _getJsonFromString(String jsonString) {
        if (jsonString!=null && jsonString.contains(CLASS_SEPARATOR)) {
            int index = jsonString.indexOf(CLASS_SEPARATOR);
            int startIndex = index+CLASS_SEPARATOR.length();
            jsonString = jsonString.substring( startIndex );
            return jsonString;
        }
        return null;
    }
    private static Class _getComponentClassFromString(String savedString) {
        if (savedString!=null && savedString.contains(CLASS_SEPARATOR)) {
            int index = savedString.indexOf(CLASS_SEPARATOR);
            String className = savedString.substring(0, index);
            if (className!=null && className.contains("<")) {
                int start = className.indexOf("<")+1;
                int end = className.indexOf(">");
                String componentClassName = className.substring(start, end);
                Class<?> cls = null;
                try {
                    cls = Class.forName(componentClassName);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                return cls;
            }
        }
        return null;
    }
    private static Class _getClassFromString(String savedString) {
        if (savedString!=null && savedString.contains(CLASS_SEPARATOR)) {
            int index = savedString.indexOf(CLASS_SEPARATOR);
            String className = savedString.substring(0, index);
            if (className.contains("<")) {
                index = className.indexOf("<");
                className = className.substring(0, index);
            }
            Class<?> cls = null;
            try {
                // className should be fully-qualified - i.e. com.mycompany.MyClass
                cls = Class.forName(className);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return cls;
        }
        return null;
    }



    private static String PREFS_NAME = "sharedPreference";
    public static void put(Context context, String key, Object value) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();

        if (value==null) {
            editor.remove(key);
            editor.commit();
            return;
        }

        String convertedString = "";
        Gson gson = new Gson();
        String json = gson.toJson(value);
        if (value instanceof List)
        {
            if (((List) value).size()>0) {
                Class componentClass = ((List) value).get(0).getClass();
                convertedString = _convertToString(json, List.class, componentClass);
            } else {
                convertedString = _convertToString(json, value.getClass());
            }
        }
        else
        {
             convertedString = _convertToString(json, value.getClass());
        }
        editor.putString(key, convertedString);
        editor.commit();
    }

    public static <T> T get(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);

        String savedString = settings.getString(key, null);
        if (savedString==null) {
            return null;
        }

        Class aClass = _getClassFromString(savedString);
        if (aClass==null) {
            return null;
        }


        Gson gson = new Gson();
        String json = _getJsonFromString(savedString);
        if (json == null) {
            return null;
        } else {
            if (aClass.getName().equals(List.class.getName())) {
                Class componentClass = _getComponentClassFromString(savedString);
                return (T) gson.fromJson(json, new CustomParameterizedType(componentClass));
            } else {
                return (T) gson.fromJson(json, aClass);
            }
        }
    }


    public static class CustomParameterizedType<T> implements ParameterizedType {
        private Class<?> innerClass;

        public CustomParameterizedType(Class<T> innerClass){
            this.innerClass = innerClass;
        }

        @Override
        public Type[] getActualTypeArguments(){
            return new Type[] { innerClass };
        }

        @Override
        public Type getRawType(){
            return List.class;
        }

        @Override
        public Type getOwnerType(){
            return null;
        }
    }


    /// Type 을 정하는게 효율이 좋음
    //
    public static void putString(Context context, String key, String value) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();

        if (value==null) {
            editor.remove(key);
            editor.commit();
            return;
        }

        editor.putString(key, value);
        editor.commit();
    }
    public static String getString(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        String savedString = settings.getString(key, "");
//        if (savedString==null) {
//            return null;
//        }
        return savedString;
    }

    public static void putInt(Context context, String key, int value) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();

        editor.putInt(key, value);
        editor.commit();
    }
    public static int getInt(Context context, String key, int defValue) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        int savedIntValue = settings.getInt(key, defValue);
        return savedIntValue;
    }

    public static void putBoolean(Context context, String key, boolean value) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();

        editor.putBoolean(key, value);
        editor.commit();
    }
    public static boolean getBoolean(Context context, String key, boolean defValue) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        boolean savedBooleanValue = settings.getBoolean(key, defValue);
        return savedBooleanValue;
    }
}
