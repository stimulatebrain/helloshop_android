package com.agcd.helloshop.util;

import java.util.Date;

/**
 * Created by jspark on 2017. 5. 30..
 */

public interface OnPopupDateResultListener {
    void onComplete(Date date);
}
