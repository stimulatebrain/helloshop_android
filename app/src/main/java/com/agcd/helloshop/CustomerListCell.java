package com.agcd.helloshop;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.agcd.helloshop.adapter.CustomerListAdapter;
import com.agcd.helloshop.model.ModelGuest;
import com.agcd.helloshop.model.ModelMemo;
import com.agcd.helloshop.popup.PopupBottomDialog;
import com.agcd.helloshop.util.OnPopupStringResultListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomerListCell {// extends RelativeLayout {

    Context mContext;

//    ViewHolder mViewHolder = new ViewHolder();

//    public class ViewHolder {
        ImageView mIvThumb;
    ImageView mIvGrade;
        TextView mTvName;
        TextView mTvPhone;
        ImageButton mBtnCheck;
        ImageButton mBtnCall;
//    }


//    static public ViewHolder getViewHolder() {
//        CustomerListCell cell = new CustomerListCell();
//        return cell.mViewHolder;
//    }

    public void setData(ModelGuest guest) {
        Picasso.with(mContext).load(guest.thumbnail).into(mIvThumb);
        mTvName.setText(guest.guest_name);
        mTvPhone.setText(guest.guest_mobile);
    }
    public void setData(ModelMemo memo) {
        //Picasso.with(mContext).load(guest.thumbnail).into(mIvThumb);
        //mTvName.setText(guest.guest_name);
        //mTvPhone.setText(memo.memo);
    }


    private static View configure(Context context, LayoutInflater inflater, View convertView, ViewGroup parent) {
        CustomerListCell cell;
        if (convertView == null) {
            cell = new CustomerListCell();

            convertView = inflater.inflate(R.layout.cell_customer, parent, false);
            cell.mIvThumb = (ImageView) convertView.findViewById(R.id.customercell_iv_profile);
            cell.mIvGrade = (ImageView) convertView.findViewById(R.id.customercell_iv_grade);
            cell.mTvName = (TextView) convertView.findViewById(R.id.customercell_tv_name);
            cell.mTvPhone = (TextView) convertView.findViewById(R.id.customercell_tv_phone);
            cell.mBtnCheck = (ImageButton) convertView.findViewById(R.id.customercell_btn_check);
            cell.mBtnCall = (ImageButton) convertView.findViewById(R.id.customercell_btn_phone);
            convertView.setTag(cell);
        } else {
            cell = (CustomerListCell) convertView.getTag();
        }
        cell.mContext = context;
        return convertView;
    }

    public static View configure(Context context, LayoutInflater inflater, View convertView, ViewGroup parent, ModelGuest guest) {
        convertView = configure(context, inflater, convertView, parent);
        CustomerListCell cell = (CustomerListCell) convertView.getTag();
        cell.setData(guest);
        return convertView;
    }
    public static View configure(Context context, LayoutInflater inflater, View convertView, ViewGroup parent, ModelMemo guest) {
        convertView = configure(context, inflater, convertView, parent);
        CustomerListCell cell = (CustomerListCell) convertView.getTag();
        cell.setData(guest);
        return convertView;
    }
}
