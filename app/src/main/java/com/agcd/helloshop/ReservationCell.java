package com.agcd.helloshop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.agcd.helloshop.model.ModelGuest;
import com.agcd.helloshop.model.ModelReserve;
import com.squareup.picasso.Picasso;

public class ReservationCell {// extends RelativeLayout {

    Context mContext;

    ImageView mIvLeftLine;

    ImageView mIvTopSectionDot;
    TextView mTvTopSectionTitle;
    TextView mTvTopSectionDate;

    TextView mTvTitle;
    TextView mTvReservationDateTime;
    TextView mTvExpert;
    TextView mTvCustomerGradeBadge;
    TextView mTvCustomerName;

    TextView mTvMemo;




    public void setData(ModelReserve reservation) {
//        mTvTopSectionTitle.setText( reservation.status );
//        mTvTopSectionDate.setText( reservation.reservation_dt );
//
//        mTvTitle.setText( reservation.service_name );
//        mTvReservationDateTime.setText( reservation.reservation_dt );
//        mTvExpert.setText( reservation.staff.getStaff_name() );
//        mTvCustomerGradeBadge.setText( reservation.guest.getGrade() );
//        mTvCustomerName.setText( reservation.guest_name );
//
//        mTvMemo.setText( reservation.guest_memo );
    }

    public static View configure(Context context,
                                 LayoutInflater inflater, View convertView, ViewGroup parent,
                                 ModelReserve reservation) {
        ReservationCell cell;
        if (convertView == null) {
            cell = new ReservationCell();

            convertView = inflater.inflate(R.layout.cell_card_reserve, parent, false);
//            cell.mTvTopSectionTitle = (TextView) convertView.findViewById(R.id.cell_);
//            cell.mTvTopSectionDate = (TextView) convertView.findViewById(R.id.cell_);
//            cell.mTvTitle = (TextView) convertView.findViewById(R.id.cell_);
//            cell.mTvReservationDateTime = (TextView) convertView.findViewById(R.id.cell_);
//            cell.mTvExpert = (TextView) convertView.findViewById(R.id.cell_);
//            cell.mTvCustomerGradeBadge = (TextView) convertView.findViewById(R.id.cell_);
//            cell.mTvCustomerName = (TextView) convertView.findViewById(R.id.cell_);
//            cell.mTvMemo = (TextView) convertView.findViewById(R.id.cell_);
            convertView.setTag(cell);
        } else {
            cell = (ReservationCell) convertView.getTag();
        }
        cell.mContext = context;
        cell.setData(reservation);
        return convertView;
    }
}
