package com.agcd.helloshop;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agcd.helloshop.model.ModelCreate;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by jspark on 2017. 7. 9..
 */

public class CustomerMessageActivity extends Activity {
    @OnClick(R.id.customer_message_btnClose)
    public void onClickClose(){
        finish();
    }

    @BindView(R.id.customer_message_tvMoreUser)
    TextView tvBtnMore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_message);
        ButterKnife.bind(this);

        tvBtnMore.getPaint().setUnderlineText(true);
    }

}
