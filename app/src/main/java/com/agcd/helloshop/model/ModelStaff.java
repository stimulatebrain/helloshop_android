package com.agcd.helloshop.model;

import java.io.Serializable;

/**
 * Created by jspark on 2017. 7. 24..
 */

public class ModelStaff extends ModelParent implements Serializable{
    int id;
    int shop_id;
    int user_id;
    String position;
    String nickname;
    String auth;
    String work_start_dt;
    String work_end_dt;
    boolean is_retire;
    String bigo;
    String staff_mobile;
    String staff_name;
    String staff_sex;
    String staff_profile;
    boolean is_activated;
    int priority;
    int created_user_id;
    int updated_user_id;
    String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getWork_start_dt() {
        return work_start_dt;
    }

    public void setWork_start_dt(String work_start_dt) {
        this.work_start_dt = work_start_dt;
    }

    public String getWork_end_dt() {
        return work_end_dt;
    }

    public void setWork_end_dt(String work_end_dt) {
        this.work_end_dt = work_end_dt;
    }

    public boolean is_retire() {
        return is_retire;
    }

    public void setIs_retire(boolean is_retire) {
        this.is_retire = is_retire;
    }

    public String getBigo() {
        return bigo;
    }

    public void setBigo(String bigo) {
        this.bigo = bigo;
    }

    public String getStaff_mobile() {
        return staff_mobile;
    }

    public void setStaff_mobile(String staff_mobile) {
        this.staff_mobile = staff_mobile;
    }

    public String getStaff_name() {
        return staff_name;
    }

    public void setStaff_name(String staff_name) {
        this.staff_name = staff_name;
    }

    public String getStaff_sex() {
        return staff_sex;
    }

    public void setStaff_sex(String staff_sex) {
        this.staff_sex = staff_sex;
    }

    public String getStaff_profile() {
        return staff_profile;
    }

    public void setStaff_profile(String staff_profile) {
        this.staff_profile = staff_profile;
    }

    public boolean is_activated() {
        return is_activated;
    }

    public void setIs_activated(boolean is_activated) {
        this.is_activated = is_activated;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getCreated_user_id() {
        return created_user_id;
    }

    public void setCreated_user_id(int created_user_id) {
        this.created_user_id = created_user_id;
    }

    public int getUpdated_user_id() {
        return updated_user_id;
    }

    public void setUpdated_user_id(int updated_user_id) {
        this.updated_user_id = updated_user_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
