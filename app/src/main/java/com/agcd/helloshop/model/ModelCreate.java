package com.agcd.helloshop.model;

/**
 * Created by jspark on 2017. 5. 30..
 */

public class ModelCreate extends ModelParent {
    public String name;
    public int headerId;

    public ModelCreate(String name, int headerId) {
        this.name = name;
        this.headerId = headerId;
    }
}
