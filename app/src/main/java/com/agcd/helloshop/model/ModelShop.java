package com.agcd.helloshop.model;

/**
 * Created by jspark on 2017. 8. 3..
 */

public class ModelShop extends ModelParent {
    int id;
    int user_id;

   // String type;
    String representative;
    String tel;
    String mobile;
    String email;
    String code;
    String name;
    String addr;
    String addr_detail;
    String addr_zip;
    String descriptions;
    String picture;
    String rest_period;
    boolean is_leave;
    String leave_dt;
    String leave_memo;
    boolean is_use;
    String admin_memo;

    int created_user_id;
    String reservation_closed_dt;
    String open_time;
    String close_time;


}
