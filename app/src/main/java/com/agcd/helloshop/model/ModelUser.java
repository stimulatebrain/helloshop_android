package com.agcd.helloshop.model;

/**
 * Created by jspark on 2017. 1. 19..
 */

public class ModelUser extends ModelParent {

    public ModelUser() {
    }

    public ModelUser(String name, String grade, String phone) {
        this.name = name;
        this.grade = grade;
        this.phone = phone;
    }

    public int id;
    private String name;
    private String grade;
    private String phone;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
