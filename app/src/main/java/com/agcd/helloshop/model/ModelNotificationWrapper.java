package com.agcd.helloshop.model;

import java.util.List;

/**
 * Created by jspark on 2017. 7. 10..
 */

public class ModelNotificationWrapper extends ModelParent {
    String id;
    int notifiable_id;
    String notificable_type;

    ModelNotification data;

    String read_at;
    String created_at;
    String updated_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNotifiable_id() {
        return notifiable_id;
    }

    public void setNotifiable_id(int notifiable_id) {
        this.notifiable_id = notifiable_id;
    }

    public String getNotificable_type() {
        return notificable_type;
    }

    public void setNotificable_type(String notificable_type) {
        this.notificable_type = notificable_type;
    }

    public ModelNotification getData() {
        return data;
    }

    public void setData(ModelNotification data) {
        this.data = data;
    }

    public String getRead_at() {
        return read_at;
    }

    public void setRead_at(String read_at) {
        this.read_at = read_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
