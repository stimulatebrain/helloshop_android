package com.agcd.helloshop.model;

import java.io.Serializable;

/**
 * Created by jspark on 2017. 7. 25..
 */

public class ModelService extends ModelParent  implements Serializable {
     int id;
     int shop_id;

     String code;
     String name;
     int amount;
     int time;

     String descriptions;
     int service_order;
     int sex;
     boolean reserve_confirm_yn;
     int repeat_number;
     boolean is_deposit;
     boolean is_vip;
     boolean is_use;

    String color;
    String color_code;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor_code() {
        return color_code;
    }

    public void setColor_code(String color_code) {
        this.color_code = color_code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public int getService_order() {
        return service_order;
    }

    public void setService_order(int service_order) {
        this.service_order = service_order;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public boolean isReserve_confirm_yn() {
        return reserve_confirm_yn;
    }

    public void setReserve_confirm_yn(boolean reserve_confirm_yn) {
        this.reserve_confirm_yn = reserve_confirm_yn;
    }

    public int getRepeat_number() {
        return repeat_number;
    }

    public void setRepeat_number(int repeat_number) {
        this.repeat_number = repeat_number;
    }

    public boolean is_deposit() {
        return is_deposit;
    }

    public void setIs_deposit(boolean is_deposit) {
        this.is_deposit = is_deposit;
    }

    public boolean is_vip() {
        return is_vip;
    }

    public void setIs_vip(boolean is_vip) {
        this.is_vip = is_vip;
    }

    public boolean is_use() {
        return is_use;
    }

    public void setIs_use(boolean is_use) {
        this.is_use = is_use;
    }
}
