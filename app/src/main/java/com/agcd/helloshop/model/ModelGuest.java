package com.agcd.helloshop.model;

import java.io.Serializable;

/**
 * Created by jspark on 2017. 1. 19..
 */

public class ModelGuest extends ModelUser implements Serializable {

    public enum Grade {
        ALL("all"),
        VVIP("vvip"),
        VIP("vip"),
        NEW("new"),
        NORMAL("normal"),
        BAD("bad");

        private String value;
        private Grade(String value){
            this.value = value;
        }
        public String getValue(){
            return value;
        }

        public String getServerParamValue() {
            if (this==ALL) {
                return "";
            }
            String value = getValue().toUpperCase();
            return value;
        }
    };

    //var guest_sex: Gender

    //int id;
    public int shop_id;
    public int user_id;
    public Grade guest_class;
    public String guest_name;
    public String guest_mobile;
    public int guest_sex;
    int residual_count;
    float residual_money;
    public String thumbnail;
    public String memo;
    boolean is_activated;
    int created_user_id;
    int updated_user_id;
    boolean is_delete;
    public boolean is_temporary = false;

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

//    public String getGuest_class() {
//
//        return guest_class;
//    }
//
//    public void setGuest_class(String guest_class) {
//        this.guest_class = guest_class;
//    }

//    public String getGuest_name() {
//        return guest_name;
//    }
//    public void setGuest_name(String guest_name) {
//        this.guest_name = guest_name;
//    }
//    public String getGuest_mobile() {
//        return guest_mobile;
//    }
//    public void setGuest_mobile(String guest_mobile) {
//        this.guest_mobile = guest_mobile;
//    }
//
//    public int getGuest_sex() {
//        return guest_sex;
//    }

    public void setGuest_sex(int guest_sex) {
        this.guest_sex = guest_sex;
    }

    public int getResidual_count() {
        return residual_count;
    }

    public void setResidual_count(int residual_count) {
        this.residual_count = residual_count;
    }

    public float getResidual_money() {
        return residual_money;
    }

    public void setResidual_money(float residual_money) {
        this.residual_money = residual_money;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }



    private boolean isChecked = false;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public ModelGuest() {
    }

    public ModelGuest(String name, String grade, String phone) {
        super(name, grade, phone);
    }


    public ModelGuest(int headerId, int id){
        this.id = id;
        this.headerId = headerId;
        this.type = Type.TYPE_CUSTOMER;
    }
}
