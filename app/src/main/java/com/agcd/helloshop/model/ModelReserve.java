package com.agcd.helloshop.model;

import com.alamkanak.weekview.WeekViewEvent;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Created by jspark on 2017. 5. 19..
 */

public class ModelReserve extends ModelParent {
    public int id;
    public String reservation_dt;
    public String start_time;
    public String end_time;
    public String service_code;
    public String service_time;
    public String service_name;
    public String service_color;
    public int guest_id;
    public String guest_name;
    public String guest_mobile;
    public String guest_class;
    public String guest_memo;
    public int staff_id;
    public String staff_memo;
    public String status;
    public int schedules_id;
    public int shop_id;
    public String shop_service_id;
    public String open_time;
    public String close_time;
    public String bigo;
    public String rest_period;
    public String reservation_closed_dt;

    public boolean is_delete;
    public int created_user_id;
    public int updated_user_id;


    public ModelStaff staff;
    public ModelAttachable attachable;
    public ModelGuest guest;
    public ModelService service;


    public ModelGuest getGuest() {
        return guest;
    }

    public void setGuest(ModelGuest guest) {
        this.guest = guest;
    }

    public ModelService getService() {
        return service;
    }

    public void setService(ModelService service) {
        this.service = service;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShop_service_id() {
        return shop_service_id;
    }

    public void setShop_service_id(String shop_service_id) {
        this.shop_service_id = shop_service_id;
    }

    public boolean is_delete() {
        return is_delete;
    }

    public void setIs_delete(boolean is_delete) {
        this.is_delete = is_delete;
    }

    public int getCreated_user_id() {
        return created_user_id;
    }

    public void setCreated_user_id(int created_user_id) {
        this.created_user_id = created_user_id;
    }

    public int getUpdated_user_id() {
        return updated_user_id;
    }

    public void setUpdated_user_id(int updated_user_id) {
        this.updated_user_id = updated_user_id;
    }

    public ModelStaff getStaff() {
        return staff;
    }

    public void setStaff(ModelStaff staff) {
        this.staff = staff;
    }

    public ModelAttachable getAttachable() {
        return attachable;
    }

    public void setAttachable(ModelAttachable attachable) {
        this.attachable = attachable;
    }

    public WeekViewEvent toWeekViewEvent(){
        DateTime startTime = DateTime.parse(reservation_dt + " " +start_time, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm"));
        DateTime endTime = DateTime.parse(reservation_dt + " " +end_time, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm"));
        return new WeekViewEvent(schedules_id, service_name, this, startTime.toGregorianCalendar(), endTime.toGregorianCalendar(),
                WeekViewEvent.statusForCode(status));
    }

    public WeekViewEvent toPossibleTimeEvent(){
        DateTime startTime = DateTime.parse(reservation_dt + " " +start_time, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm"));
        DateTime endTime = DateTime.parse(reservation_dt + " " +end_time, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm"));
        return new WeekViewEvent(0, "예약 가능", this, startTime.toGregorianCalendar(), endTime.toGregorianCalendar(), WeekViewEvent.Status.POSSIBLE);
    }


    public String getGuest_memo() {
        return guest_memo;
    }

    public void setGuest_memo(String guest_memo) {
        this.guest_memo = guest_memo;
    }

    public ModelReserve() {
        this.type = Type.TYPE_RESERVE;
    }

    public String getReservation_dt() {
        return reservation_dt;
    }

    public void setReservation_dt(String reservation_dt) {
        this.reservation_dt = reservation_dt;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getService_code() {
        return service_code;
    }

    public void setService_code(String service_code) {
        this.service_code = service_code;
    }

    public String getService_time() {
        return service_time;
    }

    public void setService_time(String service_time) {
        this.service_time = service_time;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getService_color() {
        return service_color;
    }

    public void setService_color(String service_color) {
        this.service_color = service_color;
    }

    public int getGuest_id() {
        return guest_id;
    }

    public void setGuest_id(int guest_id) {
        this.guest_id = guest_id;
    }

    public String getGuest_name() {
        return guest_name;
    }

    public void setGuest_name(String guest_name) {
        this.guest_name = guest_name;
    }

    public String getGuest_mobile() {
        return guest_mobile;
    }

    public void setGuest_mobile(String guest_mobile) {
        this.guest_mobile = guest_mobile;
    }

    public String getGuest_class() {
        return guest_class;
    }

    public void setGuest_class(String guest_class) {
        this.guest_class = guest_class;
    }

    public int getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(int staff_id) {
        this.staff_id = staff_id;
    }

    public String getStaff_memo() {
        return staff_memo;
    }

    public void setStaff_memo(String staff_memo) {
        this.staff_memo = staff_memo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getSchedules_id() {
        return schedules_id;
    }

    public void setSchedules_id(int schedules_id) {
        this.schedules_id = schedules_id;
    }

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public String getOpen_time() {
        return open_time;
    }

    public void setOpen_time(String open_time) {
        this.open_time = open_time;
    }

    public String getClose_time() {
        return close_time;
    }

    public void setClose_time(String close_time) {
        this.close_time = close_time;
    }

    public String getBigo() {
        return bigo;
    }

    public void setBigo(String bigo) {
        this.bigo = bigo;
    }

    public String getRest_period() {
        return rest_period;
    }

    public void setRest_period(String rest_period) {
        this.rest_period = rest_period;
    }

    public String getReservation_closed_dt() {
        return reservation_closed_dt;
    }

    public void setReservation_closed_dt(String reservation_closed_dt) {
        this.reservation_closed_dt = reservation_closed_dt;
    }
}
