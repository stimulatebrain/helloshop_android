package com.agcd.helloshop.model;

/**
 * Created by jspark on 2017. 5. 30..
 */

public class ModelCustomerCreate extends ModelParent {
    public String name;
    public CustomerCellType type;

    public ModelCustomerCreate(String name, CustomerCellType type) {
        this.name = name;
        this.type = type;
    }

    public enum CustomerCellType {
        TYPE_DEFAULT(1), TYPE_INFORMATION(2), TYPE_CONTACT(3);

        private int value;
        private CustomerCellType(int value){
            this.value = value;
        }
        public int getValue(){
            return value;
        }
    };
}
