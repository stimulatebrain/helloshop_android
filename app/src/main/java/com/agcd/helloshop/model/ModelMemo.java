package com.agcd.helloshop.model;

/**
 * Created by jspark on 2017. 7. 7..
 */

public class ModelMemo extends ModelParent {
    String id;

    public ModelMemo(String id) {
        this.id = id;
        this.type = Type.TYPE_MEMO;
    }
}
