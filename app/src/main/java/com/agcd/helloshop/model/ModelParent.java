package com.agcd.helloshop.model;

/**
 * Created by jspark on 2017. 6. 6..
 */

public class ModelParent {
    public ModelParent() {
    }

    public enum Type {
        TYPE_CUSTOMER(1), TYPE_RESERVE(2), TYPE_CELL(3), TYPE_MEMO(4);

        private int value;
        private Type(int value){
            this.value = value;
        }
        public int getValue(){
            return value;
        }
    };

    public Type type;
    public int headerId;
    // 헤더가 있을 경우 해당 헤더에 해당하는 섹션 데이터들의 갯수
    public int sectionCount;

    public ModelParent(int headerId, Type type) {
        this.headerId = headerId;
        this.type = type;
    }
}
