package com.agcd.helloshop.model;

/**
 * Created by jspark on 2017. 7. 9..
 */

public class ModelDashboard {
    int shop_id;
    String name;
    int finish_count;
    int reservation_count;

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFinish_count() {
        return finish_count;
    }

    public void setFinish_count(int finish_count) {
        this.finish_count = finish_count;
    }

    public int getReservation_count() {
        return reservation_count;
    }

    public void setReservation_count(int reservation_count) {
        this.reservation_count = reservation_count;
    }
}
