package com.agcd.helloshop.model;

/**
 * Created by jspark on 2017. 1. 19..
 */

public class ModelNotification extends ModelParent {

    int id;
    int shop_id;
    
    String reservation_dt;
    String start_time;
    String end_time;
    String service_time;
    int staff_id;
    int user_id;
    int guest_id;
    String guest_name;
    String guest_class;
    String guest_mobile;
    int shop_service_id;
    String service_code;
    String status;
    String bigo;
    String guest_memo;
    String staff_memo;
    boolean is_delete;
    int created_user_id;
    int updated_user_id;
    String created_at;
    String updated_at;
    String service_name;
    String staff_name;
    String service_color;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public String getReservation_dt() {
        return reservation_dt;
    }

    public void setReservation_dt(String reservation_dt) {
        this.reservation_dt = reservation_dt;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getService_time() {
        return service_time;
    }

    public void setService_time(String service_time) {
        this.service_time = service_time;
    }

    public int getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(int staff_id) {
        this.staff_id = staff_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getGuest_id() {
        return guest_id;
    }

    public void setGuest_id(int guest_id) {
        this.guest_id = guest_id;
    }

    public String getGuest_name() {
        return guest_name;
    }

    public void setGuest_name(String guest_name) {
        this.guest_name = guest_name;
    }

    public String getGuest_class() {
        return guest_class;
    }

    public void setGuest_class(String guest_class) {
        this.guest_class = guest_class;
    }

    public String getGuest_mobile() {
        return guest_mobile;
    }

    public void setGuest_mobile(String guest_mobile) {
        this.guest_mobile = guest_mobile;
    }

    public int getShop_service_id() {
        return shop_service_id;
    }

    public void setShop_service_id(int shop_service_id) {
        this.shop_service_id = shop_service_id;
    }

    public String getService_code() {
        return service_code;
    }

    public void setService_code(String service_code) {
        this.service_code = service_code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBigo() {
        return bigo;
    }

    public void setBigo(String bigo) {
        this.bigo = bigo;
    }

    public String getGuest_memo() {
        return guest_memo;
    }

    public void setGuest_memo(String guest_memo) {
        this.guest_memo = guest_memo;
    }

    public String getStaff_memo() {
        return staff_memo;
    }

    public void setStaff_memo(String staff_memo) {
        this.staff_memo = staff_memo;
    }

    public boolean is_delete() {
        return is_delete;
    }

    public void setIs_delete(boolean is_delete) {
        this.is_delete = is_delete;
    }

    public int getCreated_user_id() {
        return created_user_id;
    }

    public void setCreated_user_id(int created_user_id) {
        this.created_user_id = created_user_id;
    }

    public int getUpdated_user_id() {
        return updated_user_id;
    }

    public void setUpdated_user_id(int updated_user_id) {
        this.updated_user_id = updated_user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getStaff_name() {
        return staff_name;
    }

    public void setStaff_name(String staff_name) {
        this.staff_name = staff_name;
    }

    public String getService_color() {
        return service_color;
    }

    public void setService_color(String service_color) {
        this.service_color = service_color;
    }
}
