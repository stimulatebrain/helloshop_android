package com.agcd.helloshop;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agcd.helloshop.model.ModelNotification;
import com.agcd.helloshop.util.CustomViewPager;
import com.astuetz.PagerSlidingTabStrip;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by jspark on 2017. 5. 29..
 */

public class NotificationActivity extends Activity implements ViewPager.OnPageChangeListener {
    @BindView(R.id.notification_pager) CustomViewPager mPager;
    @BindView(R.id.notification_tabs) PagerSlidingTabStrip mTabs;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);

        NotificationPagerAdapter adapter = new NotificationPagerAdapter(this);
        mPager.setAdapter(adapter);
        mPager.setPagingDisabled();
        mPager.addOnPageChangeListener(this);
        mTabs.setViewPager(mPager);
    }

    @OnClick(R.id.notification_btnClose)
    public void clickClose(View view){
        finish();
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public class NotificationPagerAdapter extends PagerAdapter
            implements PagerSlidingTabStrip.CustomTabProvider{
        private final String[] TITLES = {
                "ALL",
                "메모",
                "신규",
                "취소",
                "변경",
                "요청",
        };

        Context mContext;
        public NotificationPagerAdapter(Context context){
            mContext = context;
        }
        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(NotificationActivity.this).inflate(R.layout.view_notification, null);
            StickyListHeadersListView lv = (StickyListHeadersListView) view.findViewById(R.id.notification_lv);
            final SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.notification_refresh);
            refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    refreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            refreshLayout.setRefreshing(false);
                        }
                    }, 1000);
                }
            });

            // Get a reference for the week view in the layout.
            NotificationListAdapter adapter = new NotificationListAdapter(NotificationActivity.this);
            ArrayList<ModelNotification> lists = new ArrayList<>();
            lists.add(new ModelNotification());
            lists.add(new ModelNotification());
            lists.add(new ModelNotification());
            lists.add(new ModelNotification());
            lists.add(new ModelNotification());
            lists.add(new ModelNotification());
            adapter.setNotifications(lists);
            lv.setAdapter(adapter);
            ((ViewPager)container).addView(view, 0);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager)container).removeView((View)object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public View getCustomTabView(ViewGroup parent, int position) {
            TextView tab = (TextView)LayoutInflater.from(NotificationActivity.this).inflate(R.layout.view_reserve_header_experts, parent, false);

            Spannable word = new SpannableString(TITLES[position]);
            word.setSpan(new ForegroundColorSpan(Color.parseColor("#f4f4f4")), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            ((TextView)tab).setText(word);

            if( position != 0){
                Spannable wordTwo = new SpannableString("" + position);
                wordTwo.setSpan(new ForegroundColorSpan(Color.parseColor("#e60b25")), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ((TextView)tab).append(" ");
                ((TextView)tab).append(wordTwo);
            }
            return tab;
        }

        @Override
        public void tabSelected(View tab) {
            String text = (String)((TextView) tab).getText().toString();
            String[] textArr = text.split(" ");
            ((TextView)tab).setTextColor(Color.parseColor("#f4f4f4"));

            if( textArr.length > 1){
                Spannable wordTwo = new SpannableString(text);

                wordTwo.setSpan(new ForegroundColorSpan(Color.parseColor("#f4f4f4")),
                        0,
                        textArr[0].length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                wordTwo.setSpan(new ForegroundColorSpan(Color.parseColor("#e60b25")),
                        textArr[0].length(),
                        wordTwo.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ((TextView)tab).setText(wordTwo);
            }

        }

        @Override
        public void tabUnselected(View tab) {
            ((TextView)tab).setTextColor(Color.parseColor("#949494"));
            String text = (String)((TextView) tab).getText().toString();
            ((TextView)tab).setText(text);
        }
    }



    public class NotificationListAdapter extends BaseSwipeAdapter implements StickyListHeadersAdapter {

        private ArrayList<ModelNotification> notifications;
        private LayoutInflater inflater;


        public NotificationListAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        public void setNotifications(ArrayList<ModelNotification> notifications) {
            this.notifications = notifications;
        }


        @Override
        public int getCount() {
            return notifications.size();
        }

        @Override
        public Object getItem(int position) {
            return notifications.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getSwipeLayoutResourceId(int position) {
            return R.id.notificationCellMain_sl;
        }

        @Override
        public View generateView(int position, ViewGroup parent) {
            ViewHolder holder = new ViewHolder();
            View convertView = inflater.inflate(R.layout.cell_notification, parent, false);
            holder.swipeLayout = (SwipeLayout) convertView.findViewById(R.id.notificationCellMain_sl);
            convertView.setTag(holder);
            return convertView;
        }

        @Override
        public void fillValues(int position, View convertView) {
            ViewHolder holder = (ViewHolder)convertView.getTag();
            holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        }

        @Override
        public View getHeaderView(int position, View convertView, ViewGroup parent) {
            HeaderViewHolder holder;
            if (convertView == null) {
                holder = new HeaderViewHolder();
                convertView = inflater.inflate(R.layout.cell_notification_header, parent, false);
                convertView.setTag(holder);
            } else {
                holder = (HeaderViewHolder) convertView.getTag();
            }

            return convertView;
        }

        @Override
        public long getHeaderId(int position) {
            //return the first character of the country as ID because this is what headers are based upon
            return 0;
        }

        class HeaderViewHolder {

        }

        class ViewHolder {
            SwipeLayout swipeLayout;
        }

    }
}
