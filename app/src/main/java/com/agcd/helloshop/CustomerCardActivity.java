package com.agcd.helloshop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agcd.helloshop.adapter.CardReserveAdapter;
import com.agcd.helloshop.model.ModelMemo;
import com.agcd.helloshop.model.ModelParent;
import com.agcd.helloshop.model.ModelReserve;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by jspark on 2017. 5. 25..
 */

public class CustomerCardActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {

    @BindView(R.id.customerCard_lv) StickyListHeadersListView mListView;

    private CardReserveAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_customer);
        ButterKnife.bind(this);

        setReserveListview();
    }

    @OnClick(R.id.customerCard_btnClose)
    public void closeCard(){
        this.finish();
    }

    private void setReserveListview(){
        View headerView = this.getLayoutInflater().inflate(R.layout.view_card_customer_header, (ViewGroup) mListView.getChildAt(0), false);
        headerView.findViewById(R.id.card_customerWrapper).setOnClickListener(this);

        mListView.addHeaderView(headerView);

        mAdapter = new CardReserveAdapter(this);
        ArrayList<ModelReserve> list = new ArrayList<>();
//        list.add(new ModelReserve("1", "아토헤어 패키지 남성염색", "09:00~10:00", "ParkJi", "김효섭"));
//        list.add(new ModelReserve("1", "아토헤어 패키지 남성염색", "09:00~10:00", "ParkJi", "김효섭"));
//        list.add(new ModelReserve("1", "아토헤어 패키지 남성염색", "09:00~10:00", "ParkJi", "김효섭"));
//        list.add(new ModelReserve("1", "아토헤어 패키지 남성염색", "09:00~10:00", "ParkJi", "김효섭"));
//        list.add(new ModelReserve("1", "아토헤어 패키지 남성염색", "09:00~10:00", "ParkJi", "김효섭"));
//        list.add(new ModelReserve("1", "아토헤어 패키지 남성염색", "09:00~10:00", "ParkJi", "김효섭"));
//        list.add(new ModelReserve("1", "아토헤어 패키지 남성염색", "09:00~10:00", "ParkJi", "김효섭"));

        mAdapter.setReserveList(list);

        ArrayList<ModelMemo> listMemo = new ArrayList<>();
        listMemo.add(new ModelMemo("1"));
        mAdapter.setMemoList(listMemo);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);

    }


    // 카드 윗부분 메소드.
    @Override
    public void onClick(View v) {
//        Intent i = new Intent(this, CardModifyActivity.class);
//        startActivity(i);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mAdapter.currentViewType == ModelParent.Type.TYPE_RESERVE){
            Intent i = new Intent(this, CustomerCardReserveActivity.class);
            startActivity(i);
        }else {
            Intent i = new Intent(this, CustomerCardMemoActivity.class);
            startActivity(i);
        }
    }
}
