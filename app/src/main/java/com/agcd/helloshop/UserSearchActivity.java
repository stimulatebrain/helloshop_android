package com.agcd.helloshop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.agcd.helloshop.model.ModelGuest;
import com.agcd.helloshop.model.ModelShop;
import com.agcd.helloshop.rest.APIHandler;
import com.agcd.helloshop.rest.ModelError;
import com.agcd.helloshop.rest.RestClient;
import com.agcd.helloshop.rest.ServerAPI;
import com.agcd.helloshop.util.Constants;
import com.agcd.helloshop.util.LocalData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnTextChanged;

/**
 * Created by jspark on 2017. 6. 2..
 */

public class UserSearchActivity extends Activity {

    @BindView(R.id.userSearch_et) EditText mEtSearch;
    @BindView(R.id.userSearch_lv) ListView lvSearch;
    @BindView(R.id.userSearch_tvTitle) TextView tvTitle;

    public final static int SEARCH_TYPE_NAME = 0;
    public final static int SEARCH_TYPE_PHONE = 1;

    private SearchAdapter mAdapter;
    private int mSearchType = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_search);
        ButterKnife.bind(this);

        mSearchType = getIntent().getIntExtra("type", 0);

        if( mSearchType == SEARCH_TYPE_NAME){
            tvTitle.setText("이름 입력");
            mEtSearch.setInputType(InputType.TYPE_CLASS_TEXT);
        }else {
            tvTitle.setText("전화번호 입력");
            mEtSearch.setInputType(InputType.TYPE_CLASS_PHONE);
        }

        mEtSearch.post(new Runnable() {
            public void run() {
                mEtSearch.requestFocusFromTouch();
                InputMethodManager lManager = (InputMethodManager)UserSearchActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                lManager.showSoftInput(mEtSearch, 0);
            }
        });

        mEtSearch.setImeOptions(EditorInfo.IME_ACTION_DONE);

        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_DONE:
                        Intent i = new Intent();
                        if(mSearchType == SEARCH_TYPE_NAME){
                            i.putExtra("guestName", mEtSearch.getText().toString());
                            setResult(CardCreateActivity.RESULT_CODE_GUEST_NAME, i);
                        }else {
                            i.putExtra("guestPhone", mEtSearch.getText().toString());
                            setResult(CardCreateActivity.RESULT_CODE_GUEST_PHONE, i);
                        }
                        finish();
                        return true;
                }
                return true;
            }
        });


        ArrayList<ModelGuest> list = new ArrayList<>();
        mAdapter = new SearchAdapter(this, list);
        lvSearch.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void loadGuests(String keyword, boolean isName){
        if( isName ){
            RestClient.getApi().loadGuestListByName(
                    Constants.TEST_SHOP_ID,
                    keyword,
                    ServerAPI.TEST_AUTH_TOKEN).enqueue
                    (new APIHandler<List<ModelGuest>>(this) {
                        @Override
                        public void onSuccess(List<ModelGuest> dataObject) {
                            mAdapter.contentsList = (ArrayList<ModelGuest>) dataObject;
                            mAdapter.notifyDataSetChanged();
                        }
                        @Override
                        public void onFailureOrLogicError(ModelError error) {

                        }
                    });
        }else {
            RestClient.getApi().loadGuestListByPhone(
                    Constants.TEST_SHOP_ID,
                    keyword,
                    ServerAPI.TEST_AUTH_TOKEN).enqueue
                    (new APIHandler<List<ModelGuest>>(this) {
                        @Override
                        public void onSuccess(List<ModelGuest> dataObject) {
                            mAdapter.contentsList = (ArrayList<ModelGuest>) dataObject;
                            mAdapter.notifyDataSetChanged();
                        }
                        @Override
                        public void onFailureOrLogicError(ModelError error) {

                        }
                    });
        }
    }

    @OnTextChanged(R.id.userSearch_et)
    public void onSearchTextChanged(CharSequence keyword, int start, int count, int after){
        Log.d("TAG", keyword.toString());
        if( mSearchType == SEARCH_TYPE_NAME){
            loadGuests(keyword.toString(), true);
        }else {
            loadGuests(keyword.toString(), false);
        }
    }

    @OnItemClick(R.id.userSearch_lv)
    public void onListItemClick(AdapterView<?> parent, View view, int position, long id){
        ModelGuest guest = mAdapter.getItem(position);
        Intent i = new Intent();
        i.putExtra("guestModel", guest);
        setResult(CardCreateActivity.RESULT_CODE_GUEST, i);
        finish();
    }

    @OnClick(R.id.userSearch_btnBack)
    public void clickBackButton(){
        finish();
    }

    public class SearchAdapter extends BaseAdapter {

        public ArrayList<ModelGuest> contentsList;
        private LayoutInflater inflater;

        public SearchAdapter(Context context, ArrayList<ModelGuest> contents) {
            inflater = LayoutInflater.from(context);
            contentsList = contents;
        }

        @Override
        public int getCount() {
            return contentsList.size();
        }

        @Override
        public ModelGuest getItem(int position) {
            return contentsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            SearchAdapter.ViewHolder holder;
            if (convertView == null) {
                holder = new SearchAdapter.ViewHolder();
                convertView = inflater.inflate(R.layout.cell_user_search, parent, false);
                holder.tvFirst = (TextView) convertView.findViewById(R.id.cell_userSearch_tvFirst);
                holder.tvSecond = (TextView) convertView.findViewById(R.id.cell_userSearch_tvSecond);
                holder.ivThumbnail = (ImageView) convertView.findViewById(R.id.cell_userSearch_tvImage);

                convertView.setTag(holder);
            } else {
                holder = (SearchAdapter.ViewHolder) convertView.getTag();
            }

            if( mSearchType == SEARCH_TYPE_NAME){
                holder.tvFirst.setText(contentsList.get(position).guest_name);
                holder.tvSecond.setText(contentsList.get(position).guest_mobile);
            }else {
                holder.tvFirst.setText(contentsList.get(position).guest_mobile);
                holder.tvSecond.setText(contentsList.get(position).guest_name);
            }

            Picasso.with(UserSearchActivity.this).load(contentsList.get(position).getThumbnail()).into(holder.ivThumbnail);
            return convertView;
        }

        class ViewHolder {
            TextView tvFirst;
            TextView tvSecond;
            ImageView ivThumbnail;

        }

    }

}
