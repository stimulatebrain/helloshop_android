package com.agcd.helloshop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.agcd.helloshop.model.ModelCreate;
import com.agcd.helloshop.model.ModelDashboardExpert;
import com.agcd.helloshop.model.ModelGuest;
import com.agcd.helloshop.model.ModelService;
import com.agcd.helloshop.model.ModelStaff;
import com.agcd.helloshop.popup.PopupBottomDialog;
import com.agcd.helloshop.rest.APIHandler;
import com.agcd.helloshop.rest.ModelError;
import com.agcd.helloshop.rest.RestClient;
import com.agcd.helloshop.rest.ServerAPI;
import com.agcd.helloshop.util.AnimationUtility;
import com.agcd.helloshop.util.LocalData;
import com.agcd.helloshop.util.OnPopupStringResultListener;
import com.agcd.helloshop.util.Utility;
import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewDataDelegate;
import com.alamkanak.weekview.WeekViewEvent;
import com.alamkanak.weekview.WeekViewReserveDataDelegate;
import com.github.ybq.android.spinkit.SpinKitView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.hoang8f.android.segmented.SegmentedGroup;
import se.emilsjolander.stickylistheaders.ExpandableStickyListHeadersListView;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by jspark on 2017. 5. 30..
 */

public class CardCreateActivity extends Activity implements AdapterView.OnItemClickListener{
    @BindView(R.id.card_create_lv) ExpandableStickyListHeadersListView mListview;
    @BindView(R.id.card_create_llOfftime) LinearLayout mLlOffTime;
    @BindView(R.id.card_create_llStepper) LinearLayout mLlStepper;


    @BindView(R.id.card_create_ivStepper1)
    ImageView mIvStepper1;

    @BindView(R.id.card_create_tvStepper1) TextView mTvStepper1;
    @BindView(R.id.card_create_tvStepper2) TextView mTvStepper2;
    @BindView(R.id.card_create_tvStepper3) TextView mTvStepper3;

    @BindView(R.id.card_create_rlStepper1) RelativeLayout mRlStepper1;
    @BindView(R.id.card_create_rlStepper2) RelativeLayout mRlStepper2;
    @BindView(R.id.card_create_rlStepper3) RelativeLayout mRlStepper3;


    @BindView(R.id.card_create_tvStepperDesc1) TextView mTvStepperDesc1;
    @BindView(R.id.card_create_viewStepperLine1) View mViewStpperLine1;

    @BindView(R.id.card_create_tvStepperDesc2) TextView mTvStepperDesc2;
    @BindView(R.id.card_create_viewStepperLine2) View mViewStpperLine2;
    @BindView(R.id.card_create_tvStepperDesc3) TextView mTvStepperDesc3;


    @BindView(R.id.card_create_llPrev) LinearLayout mLlPrev;
    @BindView(R.id.card_create_llNext) LinearLayout mLlNext;
    @BindView(R.id.card_create_llComplete) LinearLayout mLlComplete;
    @BindView(R.id.card_create_spinKit) SpinKitView mSpinKit;

    @BindView(R.id.card_create_btnNext) TextView mTvBtnNext;
    @BindView(R.id.card_create_tvGender) TextView mTvGender;

    @BindView(R.id.card_create_tvExpertName) TextView mTvExpertName;
    @BindView(R.id.card_create_tvServiceName) TextView mTvServiceName;
    @BindView(R.id.card_create_tvServiceTime) TextView mTvServiceTime;


    @BindView(R.id.card_create_rlBottomMenu) RelativeLayout mRlBottomMenu;
    @BindView(R.id.card_create_flBottomCardDesc) FrameLayout mFlBottomCardDesc;
    @BindView(R.id.card_create_flBottomCardMenu) FrameLayout mFlBottomCardMenu;

    @BindView(R.id.card_create_llStep2)
    LinearLayout mLlStep2;

    @BindView(R.id.card_create_reserveCalendar) RelativeLayout mRlStep3;

    public enum STATE_STEP {STEP_1, STEP_2, STEP_3};
    private STATE_STEP mCurrentStep;

    private ModelGuest mCurrentGuest = null;
    private ModelService mCurrentService = null;
    private ModelStaff mCurrentStaff = null;

    private String mCurrentName = null;
    private String mCurrentPhone = null;

    private int mCurrentMode = MODE_RESERVE;
    private CardCreateAdapter mAdapter;

    public final static int MODE_RESERVE = 0x00001;
    public final static int MODE_OFFTIME = 0x00002;


    public final static int RESULT_CODE_GUEST = 0x0000;
    public final static int RESULT_CODE_GUEST_NAME = 0x0003;
    public final static int RESULT_CODE_GUEST_PHONE = 0x0004;

    public final static int RESULT_CODE_SERVICE = 0x0001;
    public final static int RESULT_CODE_STAFF = 0x0002;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_create);
        ButterKnife.bind(this);

        GradientDrawable bgShape = (GradientDrawable)mRlStepper1.getBackground();
        bgShape.setColor(Color.parseColor("#04a604"));

        bgShape = (GradientDrawable)mRlStepper2.getBackground();
        bgShape.setColor(Color.parseColor("#333232"));

        bgShape = (GradientDrawable)mRlStepper3.getBackground();
        bgShape.setColor(Color.parseColor("#333232"));


        showStep1();

        ArrayList<ModelCreate> lists = new ArrayList<>();
        lists.add(new ModelCreate("이름입력", 0));
        lists.add(new ModelCreate("전화번호입력", 0));
        lists.add(new ModelCreate("기타 정보", 1));
        lists.add(new ModelCreate("기타 연락처", 2));
        lists.add(new ModelCreate("기타 연락처2", 2));

        mAdapter = new CardCreateAdapter(this, lists);
        mListview.setAdapter(mAdapter);
        mListview.collapse(1);
        mListview.collapse(2);

        mAdapter.notifyDataSetChanged();

        mListview.setOnHeaderClickListener(new StickyListHeadersListView.OnHeaderClickListener() {
            @Override
            public void onHeaderClick(StickyListHeadersListView l, View header, int itemPosition, long headerId, boolean currentlySticky) {
                if(mListview.isHeaderCollapsed(headerId)){
                    ((ViewGroup)header).getChildAt(1).setSelected(false);
                    mListview.expand(headerId);
                }else {
                    ((ViewGroup)header).getChildAt(1).setSelected(true);
                    mListview.collapse(headerId);
                }
            }
        });

        mListview.setOnItemClickListener(this);
        setWeekViewSetup();
    }

    @BindView(R.id.card_create_segmentedGroup) SegmentedGroup mSgCreate;
    @OnClick({R.id.card_create_btnReserve, R.id.card_create_btnOffTime})
    public void onCheckedChanged(RadioButton button) {
        switch (button.getId()){
            case R.id.card_create_btnReserve:
                mCurrentMode = MODE_RESERVE;
                mListview.setVisibility(View.VISIBLE);
                mLlOffTime.setVisibility(View.GONE);
                mLlStepper.setVisibility(View.VISIBLE);
                mTvBtnNext.setText(mCurrentGuest != null ? "다음" : "건너뛰기");

                break;
            case R.id.card_create_btnOffTime:
                mCurrentMode = MODE_OFFTIME;
                mListview.setVisibility(View.GONE);
                mLlOffTime.setVisibility(View.VISIBLE);
                mLlStepper.setVisibility(View.GONE);
                mTvBtnNext.setText("다음");
                break;
        }
    }

    private void setWeekViewSetup(){
        final WeekView weekView = (WeekView) mRlStep3.findViewById(R.id.reserve_weekView);
        final View viewIndicator = mRlStep3.findViewById(R.id.reserve_day_indicator);
        final Button btnMonth = (Button) mRlStep3.findViewById(R.id.reserve_btnMonth);

        final TextView tvBeforeDate = (TextView) mRlStep3.findViewById(R.id.reserve_day_before_tvDate);
        final TextView tvBeforeDay = (TextView) mRlStep3.findViewById(R.id.reserve_day_before_tvDay);
        final TextView tvAfterDate = (TextView) mRlStep3.findViewById(R.id.reserve_day_after_tvDate);
        final TextView tvAfterDay = (TextView) mRlStep3.findViewById(R.id.reserve_day_after_tvDay);

        ModelDashboardExpert expert = new ModelDashboardExpert();
        expert.setStaff_id(14);
        WeekViewReserveDataDelegate dataDelegate = new WeekViewReserveDataDelegate(this, weekView, expert);
        weekView.setOnEventClickListener(new WeekView.EventClickListener() {
            @Override
            public void onEventClick(WeekViewEvent event, RectF eventRect) {

            }
        });

        weekView.setNumberOfVisibleDays(6);
        weekView.setDateTimeInterpreter(dataDelegate);
        weekView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if( weekView.getNumberOfVisibleDays() == 1){
                    switch(event.getAction()){
                        case MotionEvent.ACTION_DOWN:
                        case MotionEvent.ACTION_MOVE:
                            viewIndicator.setVisibility(View.GONE);
                            break;
                        case MotionEvent.ACTION_UP:
                            viewIndicator.setVisibility(View.VISIBLE);
                            break;
                    }
                }else {
                    viewIndicator.setVisibility(View.GONE);
                }
                return false;
            }
        });

        weekView.setMonthChangeListener(dataDelegate);

        weekView.setScrollListener(new WeekView.ScrollListener() {
            @Override
            public void onFirstVisibleDayChanged(Calendar newFirstVisibleDay, Calendar oldFirstVisibleDay) {

                Calendar newDay = (Calendar) newFirstVisibleDay.clone();
                if (weekView.getNumberOfVisibleDays() == 1) {
                    viewIndicator.setVisibility(View.VISIBLE);
                    newDay.add(Calendar.DATE, -1);
                    String[] beforeStrings = weekView.getDateTimeInterpreter().interpretDate(newDay, true).split("\n");
                    newDay.add(Calendar.DATE, +2);
                    String[] afterStrings = weekView.getDateTimeInterpreter().interpretDate(newDay, true).split("\n");

                    tvBeforeDate.setText(beforeStrings[0]);
                    tvBeforeDay.setText(beforeStrings[1]);

                    tvAfterDate.setText(afterStrings[0]);
                    tvAfterDay.setText(afterStrings[1]);
                } else {
                    viewIndicator.setVisibility(View.GONE);
                }

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy년 MM월 ▼");
                btnMonth.setText(dateFormat.format(weekView.getFirstVisibleDay().getTime()));
            }
        });

        Calendar calendar = Calendar.getInstance();
        weekView.goToHour(calendar.get(Calendar.HOUR_OF_DAY));
    }


//    @OnClick(R.id.card_create_rlStepper1)
//    public void onClickStepper1(View v){
//        if( mCurrentStep == STATE_STEP.STEP_2){
//            hideButton2(true);
//            showButton1();
//        }else if( mCurrentStep == STATE_STEP.STEP_3){
//            hideButton3(true);
//            showButton1();
//        }
//        showStep1();
//    }
//    @OnClick(R.id.card_create_rlStepper2)
//    public void onClickStepper2(View v){
//        if( mCurrentStep == STATE_STEP.STEP_1) {
//            hideButton1(mCurrentGuest != null ? false : true);
//            showButton2(true);
//        }else if( mCurrentStep == STATE_STEP.STEP_3) {
//            hideButton3(false);
//            showButton2(false);
//        }
//        showStep2();
//    }
//
//    @OnClick(R.id.card_create_rlStepper3)
//    public void onClickStepper3(View v){
//        if( mCurrentStep == STATE_STEP.STEP_1) {
//            hideButton1(mCurrentGuest != null ? false : true);
//            showButton3(true);
//        }else if( mCurrentStep == STATE_STEP.STEP_2){
//            hideButton2(false);
//            showButton3(false);
//        }
//        showStep3();
//    }


    private void showStep1(){
        mSgCreate.setVisibility(View.VISIBLE);
        mListview.setVisibility(View.VISIBLE);
        mLlStep2.setVisibility(View.GONE);
        mRlStep3.setVisibility(View.GONE);
        mCurrentStep = STATE_STEP.STEP_1;

        mTvBtnNext.setText(mCurrentGuest != null ? "다음" : "건너뛰기");

        mLlPrev.setVisibility(View.GONE);
        mLlNext.setVisibility(View.VISIBLE);
        mLlComplete.setVisibility(View.GONE);

        mLlNext.setAlpha(1.0f);
    }



    private void showStep2(){
        mSgCreate.setVisibility(View.GONE);
        mListview.setVisibility(View.GONE);
        mLlStep2.setVisibility(View.VISIBLE);
        mRlStep3.setVisibility(View.GONE);
        mCurrentStep = STATE_STEP.STEP_2;

        mTvBtnNext.setText("다음");
        mLlPrev.setVisibility(View.VISIBLE);
        mLlNext.setVisibility(View.VISIBLE);
        mLlComplete.setVisibility(View.GONE);
        mLlNext.setAlpha(0.4f);

    }


    private void showStep3(){
        mListview.setVisibility(View.GONE);
        mLlStep2.setVisibility(View.GONE);
        mRlStep3.setVisibility(View.VISIBLE);
        mCurrentStep = STATE_STEP.STEP_3;
    }

    private void hideButton1(boolean isSkipping){
        if( isSkipping ){
            mTvStepper1.setVisibility(View.GONE);
            mIvStepper1.setVisibility(View.VISIBLE);
            mIvStepper1.setBackgroundResource(R.drawable.ic_stepper_writing);
        }else {
            mTvStepper1.setVisibility(View.VISIBLE);
            mIvStepper1.setVisibility(View.GONE);
            mTvStepper1.setTextColor(Color.parseColor("#ffffff"));
            AnimationUtility.executeColorAnimFromDrawable(mRlStepper1, Color.parseColor("#333232"), Color.parseColor("#04a604"), 300);

            //mTvStepper1.setTextColor(Color.parseColor("#999999"));
            //AnimationUtility.executeColorAnimFromDrawable(mRlStepper1, Color.parseColor("#04a604"), Color.parseColor("#333232"), 300);
        }

        mIvStepper1.setAlpha(0.0f);
        final int beforeTvWidth = mTvStepperDesc1.getWidth();
        final int beforeViewWidth = mViewStpperLine1.getWidth();

        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                mIvStepper1.setAlpha(interpolatedTime);
                mTvStepperDesc1.getLayoutParams().width = (int)(beforeTvWidth *( 1- interpolatedTime));
                mTvStepperDesc1.requestLayout();

                mViewStpperLine1.getLayoutParams().width = (int)(beforeViewWidth * (1 - interpolatedTime));
                mViewStpperLine1.requestLayout();
            }

        };
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                mTvStepperDesc1.setVisibility(View.GONE);
                mViewStpperLine1.setVisibility(View.GONE);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        anim.setDuration(300);
        anim.setInterpolator(new FastOutLinearInInterpolator());
        mIvStepper1.startAnimation(anim);
    }

    private void showButton1(){
        mTvStepperDesc1.setVisibility(View.VISIBLE);
        mViewStpperLine1.setVisibility(View.VISIBLE);
        mTvStepper1.setVisibility(View.VISIBLE);

        mIvStepper1.setVisibility(View.GONE);
        mTvStepper1.setTextColor(Color.parseColor("#ffffff"));

        AnimationUtility.executeColorAnimFromDrawable(mRlStepper1, Color.parseColor("#333232"), Color.parseColor("#04a604"), 300);

        final int beforeTvWidth = (int)mTvStepperDesc1.getPaint().measureText(mTvStepperDesc1.getText().toString());
        final int beforeViewWidth = Utility.dpToPx(110);

        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                mTvStepperDesc1.getLayoutParams().width = (int)(beforeTvWidth *( interpolatedTime));
                mTvStepperDesc1.requestLayout();

                mViewStpperLine1.getLayoutParams().width = (int)(beforeViewWidth * (interpolatedTime));
                mViewStpperLine1.requestLayout();
            }

        };
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {}
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });

        anim.setDuration(300);
        anim.setInterpolator(new FastOutLinearInInterpolator());
        mRlStepper1.startAnimation(anim);
    }

    private void hideButton2(final boolean willHideLine) {
        mTvStepper2.setTextColor(Color.parseColor("#999999"));
        AnimationUtility.executeColorAnimFromDrawable(mRlStepper2, Color.parseColor("#04a604"), Color.parseColor("#333232"), 300);

        final int beforeTvWidth = mTvStepperDesc2.getWidth();
        final int beforeViewWidth = mViewStpperLine2.getWidth();

        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                mTvStepperDesc2.getLayoutParams().width = (int)(beforeTvWidth *( 1- interpolatedTime));
                mViewStpperLine2.requestLayout();
                if( willHideLine){
                    mViewStpperLine2.getLayoutParams().width = (int)(beforeViewWidth * (1 - interpolatedTime));
                    mViewStpperLine2.requestLayout();
                }
            }

        };
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                mTvStepperDesc2.setVisibility(View.GONE);
                if( willHideLine){
                    mViewStpperLine2.setVisibility(View.GONE);
                }
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });

        anim.setDuration(300);
        anim.setInterpolator(new FastOutLinearInInterpolator());
        mRlStepper2.startAnimation(anim);
    }

    private void showButton2(final boolean willShowAnimLine){
        mTvStepperDesc2.setVisibility(View.VISIBLE);
        mViewStpperLine2.setVisibility(View.VISIBLE);
        mTvStepper2.setTextColor(Color.parseColor("#ffffff"));

        AnimationUtility.executeColorAnimFromDrawable(mRlStepper2, Color.parseColor("#333232"), Color.parseColor("#04a604"), 300);

        final int beforeTvWidth = (int)mTvStepperDesc2.getPaint().measureText(mTvStepperDesc2.getText().toString());
        final int beforeViewWidth = Utility.dpToPx(110);

        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                mTvStepperDesc2.getLayoutParams().width = (int)(beforeTvWidth *( interpolatedTime));
                mTvStepperDesc2.requestLayout();

                if( willShowAnimLine){
                    mViewStpperLine2.getLayoutParams().width = (int)(beforeViewWidth * (interpolatedTime));
                    mViewStpperLine2.requestLayout();
                }
            }

        };

        anim.setDuration(300);
        anim.setInterpolator(new FastOutLinearInInterpolator());
        mRlStepper2.startAnimation(anim);
    }

    private void hideButton3(final boolean willHideLine){
        mTvStepper3.setTextColor(Color.parseColor("#999999"));
        AnimationUtility.executeColorAnimFromDrawable(mRlStepper3, Color.parseColor("#04a604"), Color.parseColor("#333232"), 300);

        final int beforeTvWidth = mTvStepperDesc3.getWidth();
        final int beforeViewWidth = mViewStpperLine2.getWidth();

        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                mTvStepperDesc3.getLayoutParams().width = (int)(beforeTvWidth *( 1- interpolatedTime));
                mTvStepperDesc3.requestLayout();

                if( willHideLine ){
                    mViewStpperLine2.getLayoutParams().width = (int)(beforeViewWidth * (1 - interpolatedTime));
                    mViewStpperLine2.requestLayout();
                }
            }

        };
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                if( willHideLine ) {
                    mViewStpperLine2.setVisibility(View.GONE);
                }
                mTvStepperDesc3.setVisibility(View.GONE);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });

        anim.setDuration(300);
        anim.setInterpolator(new FastOutLinearInInterpolator());
        mRlStepper3.startAnimation(anim);

    }
    private void showButton3(final boolean willShowAnimLine){
        mTvStepperDesc3.setVisibility(View.VISIBLE);
        mViewStpperLine2.setVisibility(View.VISIBLE);
        mTvStepper3.setTextColor(Color.parseColor("#ffffff"));

        AnimationUtility.executeColorAnimFromDrawable(mRlStepper3, Color.parseColor("#333232"), Color.parseColor("#04a604"), 300);

        final int beforeTvWidth = (int)mTvStepperDesc3.getPaint().measureText(mTvStepperDesc3.getText().toString());
        final int beforeViewWidth = Utility.dpToPx(110);

        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                mTvStepperDesc3.getLayoutParams().width = (int)(beforeTvWidth *( interpolatedTime));
                mTvStepperDesc3.requestLayout();

                if( willShowAnimLine){
                    mViewStpperLine2.getLayoutParams().width = (int)(beforeViewWidth * (interpolatedTime));
                    mViewStpperLine2.requestLayout();
                }
            }

        };

        anim.setDuration(300);
        anim.setInterpolator(new FastOutLinearInInterpolator());
        mRlStepper3.startAnimation(anim);
    }


    @OnClick(R.id.card_create_btnClose)
    public void onCloseClick(View v){
        finish();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if( position == 0){
            Intent i = new Intent(this, UserSearchActivity.class);
            i.putExtra("type", UserSearchActivity.SEARCH_TYPE_NAME);
            startActivityForResult(i, 0);
        }else if( position == 1){
            Intent i = new Intent(this, UserSearchActivity.class);
            i.putExtra("type", UserSearchActivity.SEARCH_TYPE_PHONE);
            startActivityForResult(i, 0);
        }else {

        }
    }

    @OnClick(R.id.card_create_llNext)
    public void clickNextButton(View v){
        if(mCurrentStep == STATE_STEP.STEP_1){
            hideButton1(this.mCurrentGuest != null ? false : true);
            showButton2(true);

            showStep2();
        }else if(mCurrentStep == STATE_STEP.STEP_2){
            // 업로드 하고 스텝3로 보내기
            if(mLlNext.getAlpha() == 0.4) return;
            if(mCurrentGuest == null){
                showReserveView();
            }else if(mCurrentGuest.is_temporary){
                mSpinKit.setVisibility(View.VISIBLE);
                mLlNext.setVisibility(View.GONE);
                // 게스트 업로드 후 진행
                RestClient.getApi()
                        .uploadGuest(LocalData.getShopId(this),
                                     "NEW",
                                     mCurrentGuest.guest_name,
                                     mCurrentGuest.guest_mobile,
                                     LocalData.getTokenKey(this)).enqueue(new APIHandler<ModelGuest>(this) {
                    @Override
                    public void onSuccess(ModelGuest dataObject) {
                        mCurrentGuest = dataObject;
                        mSpinKit.setVisibility(View.GONE);
                        mLlNext.setVisibility(View.VISIBLE);
                        showReserveView();
                    }
                    @Override
                    public void onFailureOrLogicError(ModelError error) {

                    }
                });
            }else {
                // 게스트 업로드 하지 않고 진행
                showReserveView();
            }
        }
    }

    private void showReserveView(){
        mRlBottomMenu.setVisibility(View.GONE);
        mFlBottomCardDesc.setVisibility(View.VISIBLE);
        hideButton2(false);
        showButton3(false);
        showStep3();
    }

    @OnClick(R.id.card_create_llPrev)
    public void clickPrevButton(View v){
        if( mCurrentStep == STATE_STEP.STEP_2){
            hideButton2(true);
            showButton1();
            showStep1();
        }
    }

    private int mCurrentSex;

    @OnClick(R.id.card_create_btnSelectGender)
    public void onClickSelectGender(View v){
        ArrayList<String> tests = new ArrayList<>();
        tests.add("남성");
        tests.add("여성");

        PopupBottomDialog dialog = new PopupBottomDialog(this, tests, new OnPopupStringResultListener() {
            @Override
            public void onComplete(String result) {
                mTvGender.setText(result);
                mTvGender.setTextColor(Color.parseColor("#313131"));
                mCurrentSex = result.equals("남성") ? 1 : 2;
            }
        });
        dialog.show();
    }

    @OnClick(R.id.card_create_btnSelectService)
    public void onClickSelectService(View v){
        Intent i = new Intent(this,CardModifyListActivity.class);
        i.putExtra("type", CardModifyListActivity.TYPE_SERVICE);
        i.putExtra("sex", mCurrentSex);
        startActivityForResult(i, 0);
    }

    @OnClick(R.id.card_create_btnSelectExpert)
    public void onClickSelectStaff(View v){
        Intent i = new Intent(this,CardModifyListActivity.class);
        i.putExtra("type", CardModifyListActivity.TYPE_STAFF);
        i.putExtra("sex", mCurrentSex);
        startActivityForResult(i, 0);
    }



    private void setGuestModelView(ModelGuest model){
        this.mCurrentGuest = model;
        mAdapter.createLists.get(0).name = mCurrentGuest.guest_name;
        mAdapter.createLists.get(1).name = mCurrentGuest.guest_mobile;
        mAdapter.notifyDataSetChanged();

        if( mCurrentGuest.guest_sex == 1 || mCurrentGuest.guest_sex == 2){
            mTvGender.setText(mCurrentGuest.guest_sex == 1 ? "남성" : "여성");
            mTvGender.setTextColor(Color.parseColor("#313131"));
            mCurrentSex = mCurrentGuest.guest_sex;
        }else {
            mCurrentSex = 0;
        }
        mTvBtnNext.setText("다음");
    }

    private void setServiceModelView(ModelService model) {
        this.mCurrentService = model;
        mTvServiceName.setText(model.getName());
        mTvServiceTime.setText(model.getTime() + "분");
        mTvServiceName.setTextColor(Color.parseColor(model.getColor_code()));

        if( this.mCurrentStaff != null && this.mCurrentService != null){
            mLlNext.setAlpha(1.0f);

        }
    }

    private void setStaffModelView(ModelStaff model) {
        this.mCurrentStaff = model;

        mTvExpertName.setText(model.getNickname());
        mTvExpertName.setTextColor(Color.parseColor("#313131"));

        if( this.mCurrentStaff != null && this.mCurrentService != null){
            mLlNext.setAlpha(1.0f);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data == null) return;

        if( resultCode == RESULT_CODE_GUEST){
            ModelGuest model = (ModelGuest) data.getSerializableExtra("guestModel");
            if( model != null){
                setGuestModelView(model);
            }
        }else if(resultCode == RESULT_CODE_GUEST_NAME){
            String name = data.getStringExtra("guestName");
            this.mCurrentName = name;
            mAdapter.createLists.get(0).name = name;
            mAdapter.notifyDataSetChanged();

            if( mCurrentName != null && mCurrentPhone != null){
                ModelGuest guest = new ModelGuest();
                guest.guest_name = mCurrentName;
                guest.guest_mobile = mCurrentPhone;
                guest.is_temporary = true;
                setGuestModelView(guest);
            }
        }else if(resultCode == RESULT_CODE_GUEST_PHONE) {
            String phone = data.getStringExtra("guestPhone");
            this.mCurrentPhone = phone;
            mAdapter.createLists.get(1).name = phone;
            mAdapter.notifyDataSetChanged();

            if( mCurrentName != null && mCurrentPhone != null){
                ModelGuest guest = new ModelGuest();
                guest.guest_name = mCurrentName;
                guest.guest_mobile = mCurrentPhone;
                guest.is_temporary = true;
                setGuestModelView(guest);
            }
        }else if( resultCode == RESULT_CODE_SERVICE){
            ModelService model = (ModelService) data.getSerializableExtra("service");
            if( model != null){
                setServiceModelView(model);
            }
        }else if( resultCode == RESULT_CODE_STAFF){
            ModelStaff model = (ModelStaff) data.getSerializableExtra("staff");
            if( model != null){
                setStaffModelView(model);
            }
        }

    }

    public class CardCreateAdapter extends BaseAdapter implements StickyListHeadersAdapter {
        private LayoutInflater inflater;
        public ArrayList<ModelCreate> createLists;

        public CardCreateAdapter(Context context, ArrayList<ModelCreate> createLists) {
            inflater = LayoutInflater.from(context);
            this.createLists = createLists;
        }

        @Override
        public int getCount() {
            return createLists.size();
        }

        @Override
        public Object getItem(int position) {
            return createLists.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.cell_card_create, parent, false);
                holder.tvName = (TextView) convertView.findViewById(R.id.cell_cardCreate_tvName);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder)convertView.getTag();
            }

            holder.tvName.setText(createLists.get(position).name);

            return convertView;
        }

        @Override
        public View getHeaderView(int position, View convertView, ViewGroup parent) {
            HeaderViewHolder holder;
            if (convertView == null) {
                holder = new HeaderViewHolder();
                convertView = inflater.inflate(R.layout.cell_card_create_header, parent, false);
                holder.tvName = (TextView) convertView.findViewById(R.id.cell_cardCreateHeader_tvName);
                convertView.setTag(holder);
            } else {
                holder = (HeaderViewHolder) convertView.getTag();
            }
            ModelCreate model = createLists.get(position);
            switch (model.headerId){
                case 0:
                    convertView.setBackgroundResource(R.drawable.card_create_header_background);
                    holder.tvName.setText("기본 정보");
                    break;
                case 1:
                    convertView.setBackgroundColor(Color.parseColor("#e6e6e6"));
                    holder.tvName.setText("기타 정보");
                    break;
                case 2:
                    convertView.setBackgroundColor(Color.parseColor("#e6e6e6"));
                    holder.tvName.setText("기타 연락처");
                    break;
            }
            if( mListview.isHeaderCollapsed(model.headerId)){
                ((ViewGroup)convertView).getChildAt(1).setSelected(true);
            }else {
                ((ViewGroup)convertView).getChildAt(1).setSelected(false);
            }
            return convertView;
        }

        @Override
        public long getHeaderId(int position) {
            //return the first character of the country as ID because this is what headers are based upon
            return createLists.get(position).headerId;
        }

        class HeaderViewHolder {
            TextView tvName;
        }

        class ViewHolder {
            TextView tvName;
        }

    }
}
