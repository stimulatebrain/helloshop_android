package com.alamkanak.weekview;

import android.graphics.Color;

import com.agcd.helloshop.model.ModelReserve;
import com.agcd.helloshop.model.ModelUser;

import java.util.Calendar;

/**
 * Created by Raquib-ul-Alam Kanak on 7/21/2014.
 * Website: http://april-shower.com
 */
public class WeekViewEvent {
    private long mId;
    private Calendar mStartTime;
    private Calendar mEndTime;
    private String mName;
    private ModelReserve mReserve;
    private Status mStatus;

    public static Status statusForCode(String code) {
        for (Status typе : Status.values()) {
            if (typе.getValue().equals(code)) {
                return typе;
            }
        }
        return null;
    }

    public enum Status {
        DONE("00"),
        CREATE("01"),
        REQUEST("02"),
        ACCEPTED("03"),
        OFFTIME("05"),
        CHANGED("04"),
        TEMPORARY("06"),
        DELETED("88"),
        CANCELLED("99"),
        POSSIBLE("98");

        Status(String value){
            this.value = value;
        }

        private String value;
        public String getValue(){
            return value;
        }
        public String getDescription(){
            switch(this){
                case DONE:
                    return "시술완료";
                case CREATE:
                    return "예약생성";
                case REQUEST:
                    return "예약요청";
                case ACCEPTED:
                    return "예약완료";
                case CHANGED:
                    return "변경";
                case OFFTIME:
                    return "오프타임";
                case TEMPORARY:
                    return "임시예약";
                case DELETED:
                    return "삭제";
                case CANCELLED:
                    return "취소";
                case POSSIBLE:
                    return "예약가능";
                default:
                    return "예약생성";
            }
        }
    };

    public Status getStatus() {
        return mStatus;
    }

    public void setStatus(Status mStatus) {
        this.mStatus = mStatus;
    }

    private int mColor;

    public WeekViewEvent(){

    }

    /**
     * Initializes the event for week view.
     * @param id The id of the event.
     * @param name Name of the event.
     * @param startYear Year when the event starts.
     * @param startMonth Month when the event starts.
     * @param startDay Day when the event starts.
     * @param startHour Hour (in 24-hour format) when the event starts.
     * @param startMinute Minute when the event starts.
     * @param endYear Year when the event ends.
     * @param endMonth Month when the event ends.
     * @param endDay Day when the event ends.
     * @param endHour Hour (in 24-hour format) when the event ends.
     * @param endMinute Minute when the event ends.
     */
    public WeekViewEvent(long id, String name, int startYear, int startMonth, int startDay, int startHour, int startMinute, int endYear, int endMonth, int endDay, int endHour, int endMinute) {
        this.mId = id;

        this.mStartTime = Calendar.getInstance();
        this.mStartTime.set(Calendar.YEAR, startYear);
        this.mStartTime.set(Calendar.MONTH, startMonth-1);
        this.mStartTime.set(Calendar.DAY_OF_MONTH, startDay);
        this.mStartTime.set(Calendar.HOUR_OF_DAY, startHour);
        this.mStartTime.set(Calendar.MINUTE, startMinute);

        this.mEndTime = Calendar.getInstance();
        this.mEndTime.set(Calendar.YEAR, endYear);
        this.mEndTime.set(Calendar.MONTH, endMonth-1);
        this.mEndTime.set(Calendar.DAY_OF_MONTH, endDay);
        this.mEndTime.set(Calendar.HOUR_OF_DAY, endHour);
        this.mEndTime.set(Calendar.MINUTE, endMinute);

        this.mName = name;
    }

    /**
     * Initializes the event for week view.
     * @param id The id of the event.
     * @param eventName Name of the event.
     * @param startTime The time when the event starts.
     * @param endTime The time when the event ends.
     */
    public WeekViewEvent(long id,
                         String eventName,
                         ModelReserve reserve,
                         Calendar startTime,
                         Calendar endTime,
                         Status status) {
        this.mId = id;
        this.mReserve = reserve;
        this.mName = eventName;
        this.mStartTime = startTime;
        this.mEndTime = endTime;
        this.mStatus = status;
        this.mColor = Color.parseColor("#7435B3");
    }


    public Calendar getStartTime() {
        return mStartTime;
    }

    public void setStartTime(Calendar startTime) {
        this.mStartTime = startTime;
    }

    public Calendar getEndTime() {
        return mEndTime;
    }

    public void setEndTime(Calendar endTime) {
        this.mEndTime = endTime;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }


    public int getColor() {
        return mColor;
    }

    public void setColor(int color) {
        this.mColor = color;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        this.mId = id;
    }

    public ModelReserve getReserve() {
        return mReserve;
    }

    public void setReserve(ModelReserve mReserve) {
        this.mReserve = mReserve;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WeekViewEvent that = (WeekViewEvent) o;

        return mId == that.mId;

    }

    @Override
    public int hashCode() {
        return (int) (mId ^ (mId >>> 32));
    }
}
