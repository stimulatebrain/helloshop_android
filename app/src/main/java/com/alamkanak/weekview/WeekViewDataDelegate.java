package com.alamkanak.weekview;

import android.content.Context;
import android.util.Log;

import com.agcd.helloshop.fragment.ReserveFragment;
import com.agcd.helloshop.model.ModelDashboardExpert;
import com.agcd.helloshop.model.ModelReserve;
import com.agcd.helloshop.model.ModelStaff;
import com.agcd.helloshop.rest.APIHandler;
import com.agcd.helloshop.rest.ModelError;
import com.agcd.helloshop.rest.RestClient;
import com.agcd.helloshop.rest.ServerAPI;
import com.agcd.helloshop.util.Constants;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by jspark on 2017. 8. 29..
 */

public class WeekViewDataDelegate implements MonthLoader.MonthChangeListener, DateTimeInterpreter{
    HashMap<String, ArrayList<WeekViewEvent>> mEventsMap = new HashMap<>();
    Context context;
    WeekView weekView;
    ModelDashboardExpert expert;

    public WeekViewDataDelegate(Context context, WeekView weekView, ModelDashboardExpert expert){
        this.context = context;
        this.weekView = weekView;
        this.expert = expert;
    }


    @Override
    public String interpretDate(Calendar date, boolean isDayView) {
        SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE", Locale.getDefault());
        String weekday = weekdayNameFormat.format(date.getTime());
        SimpleDateFormat format = new SimpleDateFormat("d", Locale.getDefault());

        if (isDayView) {
            return format.format(date.getTime())+"\n("+ weekday.toUpperCase() + ")" ;
        }else {
            return weekday.toUpperCase() +"\n"+ format.format(date.getTime());
        }
    }

    @Override
    public String interpretTime(int hour) {
        return hour == 12 ? "pm\n12" :
                (hour > 11 ? "pm\n" + (hour - 12)  : (hour == 0 ? "am \n12" : "am\n" + hour));
    }

    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth, int relativeIndex) {
        loadSchedule(newYear, newMonth);
        // Return only the events that matches newYear and newMonth.
        List<WeekViewEvent> matchedEvents = new ArrayList<WeekViewEvent>();
        String eventKey =  newYear + ":" + newMonth;
        for (WeekViewEvent event : mEventsMap.get(eventKey)) {
            if (eventMatches(event, newYear, newMonth-1)) {
                if( !(event.getStatus() == WeekViewEvent.Status.CANCELLED ||
                        event.getStatus() == WeekViewEvent.Status.TEMPORARY ||
                        event.getStatus() == WeekViewEvent.Status.DELETED)){
                    matchedEvents.add(event);
                }
            }
        }
        return matchedEvents;
    }


    public void loadSchedule(final int year, final int month){
        Log.d("TAG", "ON MONTH CHANGE : " + year + ":" + month);
        final String eventKey = year + ":" + month;

        if(!mEventsMap.containsKey(eventKey)){
            final WeekView currentWeekView = this.weekView;
            DateTime startTime = DateTime.now().withDate(year, month, 1);

            mEventsMap.put(eventKey, new ArrayList<WeekViewEvent>());
            RestClient.getApi().loadScheduleListInfo(
                    Constants.TEST_SHOP_ID,
                    startTime.toString("yyyy-MM-dd"),
                    startTime.dayOfMonth().withMaximumValue().toString("yyyy-MM-dd"),
                    expert.getStaff_id(),
                    ServerAPI.TEST_AUTH_TOKEN).enqueue
                    (new APIHandler<List<ModelReserve>>(this.context) {
                        @Override
                        public void onSuccess(List<ModelReserve> dataObject) {
                            Log.d("TAG", "ON LOAD SCHEDULE LIST : " + year + ":" + month + " , size : " + dataObject.size());

                            for (ModelReserve event : dataObject) {
                                mEventsMap.get(eventKey).add(event.toWeekViewEvent());
                            }
                            currentWeekView.notifyDatasetChanged();
                        }
                        @Override
                        public void onFailureOrLogicError(ModelError error) {
                            mEventsMap.remove(eventKey);
                        }
                    });
        }
    }

    public boolean eventMatches(WeekViewEvent event, int year, int month) {
        return (event.getStartTime().get(Calendar.YEAR) == year
                && event.getStartTime().get(Calendar.MONTH) == month)
                ||
                (event.getEndTime().get(Calendar.YEAR) == year
                        && event.getEndTime().get(Calendar.MONTH) == month);
    }
}
