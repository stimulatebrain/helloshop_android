package com.alamkanak.weekview;

import android.content.Context;
import android.util.Log;

import com.agcd.helloshop.model.ModelDashboardExpert;
import com.agcd.helloshop.model.ModelReserve;
import com.agcd.helloshop.rest.APIHandler;
import com.agcd.helloshop.rest.ModelError;
import com.agcd.helloshop.rest.RestClient;
import com.agcd.helloshop.rest.ServerAPI;
import com.agcd.helloshop.util.Constants;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jspark on 2017. 8. 29..
 */

public class WeekViewReserveDataDelegate extends WeekViewDataDelegate {
    HashMap<String, ArrayList<WeekViewEvent>> mPossibleMap = new HashMap<>();

    public WeekViewReserveDataDelegate(Context context, WeekView weekView, ModelDashboardExpert expert) {
        super(context, weekView, expert);
    }

    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth, int relativeIndex) {
        List<WeekViewEvent> list = (List<WeekViewEvent>) super.onMonthChange(newYear, newMonth, relativeIndex);
        String eventKey =  newYear + ":" + newMonth;
        for (WeekViewEvent event : mPossibleMap.get(eventKey)) {
            if (eventMatches(event, newYear, newMonth-1)) {
                list.add(event);
            }
        }
        return list;
    }

    @Override
    public void loadSchedule(final int year, final int month) {
        super.loadSchedule(year, month);
        final String eventKey = year + ":" + month;
        if(!mPossibleMap.containsKey(eventKey)){
            final WeekView currentWeekView = this.weekView;
            DateTime startTime = DateTime.now().withDate(year, month, 1);

            mPossibleMap.put(eventKey, new ArrayList<WeekViewEvent>());
            RestClient.getApi().loadPossibleTimes(
                    Constants.TEST_SHOP_ID,
                    startTime.toString("yyyy-MM-dd"),
                    startTime.dayOfMonth().withMaximumValue().toString("yyyy-MM-dd"),
                    expert.getStaff_id(),
                    ServerAPI.TEST_AUTH_TOKEN).enqueue
                    (new APIHandler<List<ModelReserve>>(this.context) {
                        @Override
                        public void onSuccess(List<ModelReserve> dataObject) {

                            for (ModelReserve event : dataObject) {
                                mPossibleMap.get(eventKey).add(event.toPossibleTimeEvent());
                            }
                            currentWeekView.notifyDatasetChanged();
                        }
                        @Override
                        public void onFailureOrLogicError(ModelError error) {
                            mPossibleMap.remove(eventKey);
                        }
                    });
        }

    }
}
