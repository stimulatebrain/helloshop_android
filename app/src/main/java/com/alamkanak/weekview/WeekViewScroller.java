package com.alamkanak.weekview;

import android.content.Context;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.view.animation.AnimationUtils;
import android.widget.OverScroller;
import android.widget.Scroller;

/**
 * Created by jspark on 2016. 12. 28..
 */

public class WeekViewScroller {
    private Scroller mScroller;
    private OverScroller mOverScroller;
    private WeekView weekView;

    public WeekViewScroller(Context context, WeekView weekView) {
        this.weekView = weekView;
        mOverScroller = new OverScroller(context, new FastOutLinearInInterpolator());
        mScroller = new Scroller(context, new FastOutLinearInInterpolator());
    }

    public final boolean isFinished() {
        return (weekView.getCurrentScrollDirection() == WeekView.Direction.VERTICAL) ?
                mOverScroller.isFinished() : mScroller.isFinished();
    }

    public void forceFinished(boolean finished) {
        if( weekView.getCurrentScrollDirection() == WeekView.Direction.VERTICAL){
            mOverScroller.forceFinished(finished);
        }else {
            mScroller.forceFinished(finished);
        }
    }

    public void fling(int startX, int startY, int velocityX, int velocityY,
                      int minX, int maxX, int minY, int maxY, boolean forceOverScroll) {
        if (forceOverScroll) {
            mOverScroller.fling(startX, startY, velocityX, velocityY, minX, maxX, minY, maxY);
            return;
        }

        mScroller.fling(startX, startY, velocityX, velocityY, minX, maxX, minY, maxY);
    }

    public float getCurrVelocity() {
        return (weekView.getCurrentScrollDirection() == WeekView.Direction.VERTICAL) ?
                        mOverScroller.getCurrVelocity() : mScroller.getCurrVelocity();
    }

    public void startScroll(int startX, int startY, int dx, int dy, int duration) {
        mScroller.startScroll(startX, startY, dx, dy, duration);
    }

    public boolean computeScrollOffset() {
        return (weekView.getCurrentScrollDirection() == WeekView.Direction.VERTICAL) ?
                mOverScroller.computeScrollOffset() : mScroller.computeScrollOffset();
    }

    public final int getCurrX() {
        return
        (weekView.getCurrentScrollDirection() == WeekView.Direction.VERTICAL) ?
                mOverScroller.getCurrX() : mScroller.getCurrX();
    }

    public final int getCurrY() {
        return (weekView.getCurrentScrollDirection() == WeekView.Direction.VERTICAL) ?
                mOverScroller.getCurrY() : mScroller.getCurrY();
    }

}